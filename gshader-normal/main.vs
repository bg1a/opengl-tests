#version 330 core
layout (location = 0) in vec3 vpos;
layout (location = 1) in vec3 col;

out VS_OUT{
    mat4 proj_view;
    vec4 coord;
}vs_out;

out vec3 fcol;

void main(){
    float zn = 1.0;
    float zf = 100.0;
    const float alpha = 3.14 / 4.0;

    mat4 p = mat4(1.0, 0.0,             0.0,        0.0,
                  0.0, 1.0,             0.0,        0.0,
                  0.0, 0.0, -(zn + zf)/(zn - zf),   1.0,
                  0.0, 0.0,  (2*zn*zf)/(zn - zf),   0.0);

    mat4 t = mat4(1.0,  0.0,  0.0, 0.0,
                  0.0,  1.0,  0.0, 0.0,
                  0.0,  0.0,  1.0, 0.0,
                  0.0,  0.0,  4.0, 1.0);

    mat4 v = mat4(normalize(vec4(1.0,  0.0,  0.0,  0.0)),
                  normalize(vec4(0.0,  1.0,  0.0,  0.0)),
                  normalize(vec4(0.0,  0.0, -1.0,  0.0)),
                            vec4(0.0,  0.0,  0.0,  1.0));

    mat4 rx = mat4(1.0,  0.0,         0.0,        0.0,
                   0.0,  cos(alpha),  sin(alpha), 0.0,
                   0.0, -sin(alpha),  cos(alpha), 0.0,
                   0.0,  0.0,         0.0,        1.0);
    mat4 rz = mat4( cos(alpha),  sin(alpha), 0.0, 0.0,
                   -sin(alpha),  cos(alpha), 0.0, 0.0,
                    0.0,         0.0,        1.0, 0.0,
                    0.0,         0.0,        0.0, 1.0);
    gl_Position = p*inverse(t*v)*rz*rx*vec4(vpos, 1.0);

    vs_out.proj_view = p*inverse(t*v);
    vs_out.coord = rz*rx*vec4(vpos, 1.0);
    fcol = col;
}

