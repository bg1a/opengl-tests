#version 330 core
layout (triangles) in;
layout (line_strip, max_vertices = 2) out;

in VS_OUT{
    mat4 proj_view;
    vec4 coord;
}gs_in[];

vec3 get_mass_center(vec3 a, vec3 b, vec3 c)
{
    return ((a + b + c) / 3.0);
}

vec3 get_normal(vec3 a, vec3 b, vec3 c)
{
    vec3 v1 = b - a;
    vec3 v2 = c - b;

    return normalize(cross(v1, v2));
}

void main(){
    vec3 n = get_normal(vec3(gs_in[0].coord),
                        vec3(gs_in[1].coord),
                        vec3(gs_in[2].coord));
    vec3 m = get_mass_center(vec3(gs_in[0].coord),
                             vec3(gs_in[1].coord),
                             vec3(gs_in[2].coord));
    gl_Position = gs_in[0].proj_view * vec4(m, 1.0);
    EmitVertex();

    gl_Position = gs_in[0].proj_view * vec4(m + n, 1.0);
    EmitVertex();
    EndPrimitive();
}

