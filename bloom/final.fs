// vim: set ft=glsl:
#version 330 core

in VS_OUT{
    vec3 coord;
    vec2 tcoord;
}vs_out;

out vec4 FragColor;
uniform sampler2D tex;
uniform sampler2D blur;

void main(){
    vec3 color = texture(tex, vs_out.tcoord).rgb;
    vec3 blur_color = texture(blur, vs_out.tcoord).rgb;
    float exposure = 1.0;
    float gamma = 2.2;

    color += blur_color;
    color = vec3(1.0) - exp(-color / exposure);
    //color = pow(color, vec3(1.0/gamma));

    FragColor = vec4(color, 1.0);
}
