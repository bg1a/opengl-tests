#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include <GLES3/gl31.h>
#include <GL/gl.h>

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

int width = 800;
int height = 600;
GLfloat view_pos[] = {
    7.0, 0.0, 5.0,
};

struct LightSource{
    GLfloat pos[3];
    GLfloat color[3];
} light_source[] = {
    {
        {0.0, 0.5, 1.5},
        {5.0, 5.0, 5.0},
    },
    {
        {-4.0, 0.5, -3.0},
        {10.0, 0.0, 0.0},
    },
    {
        {3.0, 0.5, 1.0},
        {0.0, 0.0, 15.0},
    },
    {
        {-0.8, 2.4, -1.0},
        {0.0, 5.0, 0.0},
    }
};

glm::mat4 proj = glm::perspective(glm::radians(45.0f), (float)width/(float)height, 0.1f, 100.0f);
glm::mat4 view = glm::lookAt(glm::vec3(view_pos[0], view_pos[1], view_pos[2]), 
                             glm::vec3(0.0, 0.0, 0.0),
                             glm::vec3(0.0, 1.0, 0.0));

extern void create_window(int w, int h);
extern void window_loop(void);

GLfloat vertices[] = {
    //Vertex             Normals         Texture
   -1.0, -1.0, -1.0,  0.0,  0.0, -1.0,  0.0, 0.0,                                                             
    1.0, -1.0, -1.0,  0.0,  0.0, -1.0,  1.0, 0.0,                                                             
    1.0,  1.0, -1.0,  0.0,  0.0, -1.0,  1.0, 1.0,                                                             
    1.0,  1.0, -1.0,  0.0,  0.0, -1.0,  1.0, 1.0,                                                             
   -1.0,  1.0, -1.0,  0.0,  0.0, -1.0,  0.0, 1.0,                                                             
   -1.0, -1.0, -1.0,  0.0,  0.0, -1.0,  0.0, 0.0,                                                             
                                      
   -1.0, -1.0,  1.0,  0.0,  0.0,  1.0,  0.0, 0.0,                                                            
    1.0, -1.0,  1.0,  0.0,  0.0,  1.0,  1.0, 0.0,                                                            
    1.0,  1.0,  1.0,  0.0,  0.0,  1.0,  1.0, 1.0,                                                            
    1.0,  1.0,  1.0,  0.0,  0.0,  1.0,  1.0, 1.0,                                                            
   -1.0,  1.0,  1.0,  0.0,  0.0,  1.0,  0.0, 1.0,                                                            
   -1.0, -1.0,  1.0,  0.0,  0.0,  1.0,  0.0, 0.0,                                                            

   -1.0,  1.0,  1.0, -1.0,  0.0,  0.0,  1.0, 1.0,                                                   
   -1.0,  1.0, -1.0, -1.0,  0.0,  0.0,  0.0, 1.0,                                                   
   -1.0, -1.0, -1.0, -1.0,  0.0,  0.0,  0.0, 0.0,                                                   
   -1.0, -1.0, -1.0, -1.0,  0.0,  0.0,  0.0, 0.0,                                                   
   -1.0, -1.0,  1.0, -1.0,  0.0,  0.0,  1.0, 0.0,                                                   
   -1.0,  1.0,  1.0, -1.0,  0.0,  0.0,  1.0, 1.0,                                                   
                                                 
    1.0,  1.0,  1.0,  1.0,  0.0,  0.0,  0.0, 0.0,                                                   
    1.0,  1.0, -1.0,  1.0,  0.0,  0.0,  1.0, 0.0,                                                   
    1.0, -1.0, -1.0,  1.0,  0.0,  0.0,  1.0, 1.0,                                                   
    1.0, -1.0, -1.0,  1.0,  0.0,  0.0,  1.0, 1.0,                                                   
    1.0, -1.0,  1.0,  1.0,  0.0,  0.0,  0.0, 1.0,                                                   
    1.0,  1.0,  1.0,  1.0,  0.0,  0.0,  0.0, 0.0,                                                   

   -1.0, -1.0, -1.0,  0.0, -1.0,  0.0,  0.0, 0.0,                                                  
    1.0, -1.0, -1.0,  0.0, -1.0,  0.0,  1.0, 0.0,                                                  
    1.0, -1.0,  1.0,  0.0, -1.0,  0.0,  1.0, 1.0,                                                  
    1.0, -1.0,  1.0,  0.0, -1.0,  0.0,  1.0, 1.0,                                                  
   -1.0, -1.0,  1.0,  0.0, -1.0,  0.0,  0.0, 1.0,                                                  
   -1.0, -1.0, -1.0,  0.0, -1.0,  0.0,  0.0, 0.0,                                                  
                                                 
    1.0,  1.0,  1.0,  0.0,  1.0,  0.0,  1.0, 1.0,                                                  
    1.0,  1.0, -1.0,  0.0,  1.0,  0.0,  1.0, 0.0,                                                  
   -1.0,  1.0, -1.0,  0.0,  1.0,  0.0,  0.0, 0.0,                                                  
    1.0,  1.0,  1.0,  0.0,  1.0,  0.0,  1.0, 1.0,                                                  
   -1.0,  1.0, -1.0,  0.0,  1.0,  0.0,  0.0, 0.0,                                                  
   -1.0,  1.0,  1.0,  0.0,  1.0,  0.0,  0.0, 1.0,                                                  
};

GLfloat vertices_quad[] = {
    -1.0, -1.0,  0.0,   0.0, 0.0,
     1.0, -1.0,  0.0,   1.0, 0.0,
     1.0,  1.0,  0.0,   1.0, 1.0,

    -1.0, -1.0,  0.0,   0.0, 0.0,
     1.0,  1.0,  0.0,   1.0, 1.0,
    -1.0,  1.0,  0.0,   0.0, 1.0,
};

char *load_file(const char *filename)
{
    FILE *f = fopen(filename, "r");
    char *text = NULL;

    if(f){
        size_t size = 0;

        fseek(f, 0, SEEK_END);
        size = ftell(f);
        rewind(f);

        text = (char*)calloc(size + 1, sizeof(char));
        fread(text, sizeof(char), size, f);
        fclose(f); 
    }

    return text;
}

void load_shader(const char *vertex_source, const char *fragment_source,
        GLuint *vertex, GLuint *fragment, GLuint *program)
{
    const char * test_str = "#version ";
    char infoLog[512] = {0};
    char *shader_source = NULL;

    *vertex = glCreateShader(GL_VERTEX_SHADER);
    if(!strcmp(vertex_source, test_str)){
        glShaderSource(*vertex, 1, &vertex_source, NULL);
        glCompileShader(*vertex);
        glGetShaderInfoLog(*vertex, sizeof(infoLog), NULL, infoLog);
        printf("Vertex shader compile log: %s\n", infoLog);
    }else{
        shader_source = load_file(vertex_source);
        glShaderSource(*vertex, 1, &shader_source, NULL);
        glCompileShader(*vertex);
        glGetShaderInfoLog(*vertex, sizeof(infoLog), NULL, infoLog);
        printf("%s, compile log: %s\n", vertex_source, infoLog);
        free(shader_source);
    }

    *fragment = glCreateShader(GL_FRAGMENT_SHADER);
    if(!strcmp(fragment_source, test_str)){
        glShaderSource(*fragment, 1, &fragment_source, NULL);
        glCompileShader(*fragment);
        glGetShaderInfoLog(*fragment, sizeof(infoLog), NULL, infoLog);
        printf("Fragment shader compile log: %s\n", infoLog);
    }else{
        shader_source = load_file(fragment_source);
        glShaderSource(*fragment, 1, &shader_source, NULL);
        glCompileShader(*fragment);
        glGetShaderInfoLog(*fragment, sizeof(infoLog), NULL, infoLog);
        printf("%s, compile log: %s\n", fragment_source, infoLog);
        free(shader_source);
    }

    *program = glCreateProgram();
    glAttachShader(*program, *vertex);
    glAttachShader(*program, *fragment);
    glLinkProgram(*program);
}

enum{
    FIRST_BOX = 0,
    SECOND_BOX,
    THIRD_BOX,
    FOUR_BOX,
    FIVE_BOX,
    SIX_BOX,
    SEVEN_BOX,
    EIGHT_BOX,
    NINE_BOX,
    TEN_BOX,
    ELEVEN_BOX,
    LAST_BOX,
};

#define ARRAY_SZ(_a) \
    (sizeof(_a) / sizeof(_a[0]))

GLuint vao[LAST_BOX], buf[LAST_BOX];
GLuint box_vs, box_fs, box_ph, box_tex, floor_tex;
GLuint light_box_vs, light_box_fs, light_box_ph;

// Framebbufer
enum{
    COLOR_TEXTURE0 = 0,
    COLOR_TEXTURE1,
    COLOR_TEXTURE_LAST,
};
GLuint fbo, color_tex[COLOR_TEXTURE_LAST], rbo;
GLuint draw_buf[] = {GL_COLOR_ATTACHMENT0, GL_COLOR_ATTACHMENT1};

// Pingpong framebuffer
enum{
    PPONG0 = 0,
    PPONG1,
    PPONG_LAST,
};
GLuint ppong_fbo[PPONG_LAST], ppong_tex[PPONG_LAST];
GLuint blur_vao, blur_buf;
GLuint blur_vs, blur_fs, blur_ph;

// Final scene
GLuint final_vao, final_buf, final_vs, final_fs, final_ph;

void draw_opengl(char c)
{
    glm::mat4 model(1.0f); 

    glBindFramebuffer(GL_FRAMEBUFFER, fbo);
    {
        // Render boxes
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
        glUseProgram(box_ph);
        glActiveTexture(GL_TEXTURE0);
        glBindTexture(GL_TEXTURE_2D, box_tex);
        glUniform1i(glGetUniformLocation(box_ph, "tex"), 0);
        glUniformMatrix4fv(glGetUniformLocation(box_ph, "proj"), 1, GL_FALSE, glm::value_ptr(proj));
        glUniformMatrix4fv(glGetUniformLocation(box_ph, "view"), 1, GL_FALSE, glm::value_ptr(view));
        glUniform3fv(glGetUniformLocation(box_ph, "view_pos"), 1, view_pos);
        for(int i = 0; i < ARRAY_SZ(light_source); ++i){
            LightSource *ls = &light_source[i];
            char pos_name[30];
            char color_name[30];

            snprintf(pos_name, sizeof(pos_name), "light_source[%d].pos", i);
            snprintf(color_name, sizeof(color_name), "light_source[%d].color", i);

            glUniform3fv(glGetUniformLocation(box_ph, pos_name), 1, ls->pos);
            glUniform3fv(glGetUniformLocation(box_ph, color_name), 1, ls->color);
        }

        glBindVertexArray(vao[FIRST_BOX]);
        model = glm::mat4(1.0f);
        model = glm::translate(model, glm::vec3(0.0f, 1.5f, 0.0f));
        model = glm::scale(model, glm::vec3(0.5f));
        glUniformMatrix4fv(glGetUniformLocation(box_ph, "model"), 1, GL_FALSE, glm::value_ptr(model));
        glDrawArrays(GL_TRIANGLES, 0, 36);

        glBindVertexArray(vao[SECOND_BOX]);
        model = glm::mat4(1.0f);
        model = glm::translate(model, glm::vec3(2.0f, 0.0f, 1.0f));
        model = glm::scale(model, glm::vec3(0.5f));
        glUniformMatrix4fv(glGetUniformLocation(box_ph, "model"), 1, GL_FALSE, glm::value_ptr(model));
        glDrawArrays(GL_TRIANGLES, 0, 36);

        glBindVertexArray(vao[THIRD_BOX]);
        model = glm::mat4(1.0f);
        model = glm::translate(model, glm::vec3(-1.0f, -1.0f, 2.0f));
        model = glm::rotate(model, glm::radians(60.0f), glm::normalize(glm::vec3(1.0f, 0.0f, 1.0f)));
        glUniformMatrix4fv(glGetUniformLocation(box_ph, "model"), 1, GL_FALSE, glm::value_ptr(model));
        glDrawArrays(GL_TRIANGLES, 0, 36);

        glBindVertexArray(vao[FOUR_BOX]);
        model = glm::mat4(1.0f);
        model = glm::translate(model, glm::vec3(0.0f, 2.7f, 4.0f));
        model = glm::rotate(model, glm::radians(23.0f), glm::normalize(glm::vec3(1.0f, 0.0f, 1.0f)));
        model = glm::scale(model, glm::vec3(1.25f));
        glUniformMatrix4fv(glGetUniformLocation(box_ph, "model"), 1, GL_FALSE, glm::value_ptr(model));
        glDrawArrays(GL_TRIANGLES, 0, 36);

        glBindVertexArray(vao[FIVE_BOX]);
        model = glm::mat4(1.0f);
        model = glm::translate(model, glm::vec3(-2.0f, 1.0f, -3.0f));
        model = glm::rotate(model, glm::radians(124.0f), glm::normalize(glm::vec3(1.0f, 0.0f, 1.0f)));
        glUniformMatrix4fv(glGetUniformLocation(box_ph, "model"), 1, GL_FALSE, glm::value_ptr(model));
        glDrawArrays(GL_TRIANGLES, 0, 36);

        glBindVertexArray(vao[SIX_BOX]);
        model = glm::mat4(1.0f);
        model = glm::translate(model, glm::vec3(-3.0f, 0.0f, 0.0f));
        model = glm::scale(model, glm::vec3(0.5f));
        glUniformMatrix4fv(glGetUniformLocation(box_ph, "model"), 1, GL_FALSE, glm::value_ptr(model));
        glDrawArrays(GL_TRIANGLES, 0, 36);

        // Render floor box
        glBindTexture(GL_TEXTURE_2D, floor_tex);
        glActiveTexture(GL_TEXTURE0);
        glBindVertexArray(vao[SEVEN_BOX]);
        model = glm::mat4(1.0f);
        model = glm::translate(model, glm::vec3(0.0f, -1.0f, 0.0f));
        model = glm::scale(model, glm::vec3(12.5f, 0.5f, 12.5f));
        glUniformMatrix4fv(glGetUniformLocation(box_ph, "model"), 1, GL_FALSE, glm::value_ptr(model));
        glDrawArrays(GL_TRIANGLES, 0, 36);

        // Render light boxes
        glUseProgram(light_box_ph);
        glUniformMatrix4fv(glGetUniformLocation(light_box_ph, "proj"), 1, GL_FALSE, glm::value_ptr(proj));
        glUniformMatrix4fv(glGetUniformLocation(light_box_ph, "view"), 1, GL_FALSE, glm::value_ptr(view));
        for(int i = EIGHT_BOX; i < LAST_BOX; ++i){
            LightSource *ls = &light_source[i - EIGHT_BOX];

            glBindVertexArray(vao[i]);
            model = glm::mat4(1.0);
            model = glm::translate(model, glm::vec3(ls->pos[0], ls->pos[1], ls->pos[2]));
            model = glm::scale(model, glm::vec3(0.25f, 0.25f, 0.25f));
            glUniformMatrix4fv(glGetUniformLocation(light_box_ph, "model"), 1, GL_FALSE, glm::value_ptr(model));
            glUniform3fv(glGetUniformLocation(light_box_ph, "color"), 1, ls->color);
            glDrawArrays(GL_TRIANGLES, 0, 36);
        }
    }
    glBindFramebuffer(GL_FRAMEBUFFER, 0);

    // Apply blur
    glUseProgram(blur_ph);
    glActiveTexture(GL_TEXTURE0);
    glUniform1i(glGetUniformLocation(blur_ph, "tex"), 0);
    glBindVertexArray(blur_vao);

    for(GLuint is_horizontal = 1, tx = color_tex[COLOR_TEXTURE1], i = 0;
            i < 10; ++i){
        glUniform1i(glGetUniformLocation(blur_ph, "horizontal"), is_horizontal);
        glBindFramebuffer(GL_FRAMEBUFFER, ppong_fbo[i % PPONG_LAST]);
        glBindTexture(GL_TEXTURE_2D, tx);

        glDrawArrays(GL_TRIANGLES, 0, 6);

        tx = ppong_tex[i % PPONG_LAST];
        is_horizontal = !is_horizontal;
    }

    // Render final scene
    glBindFramebuffer(GL_FRAMEBUFFER, 0);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glUseProgram(final_ph);
    glUniform1i(glGetUniformLocation(final_ph, "tex"), 0);
    glUniform1i(glGetUniformLocation(final_ph, "blur"), 1);
    glBindVertexArray(final_vao);
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, color_tex[COLOR_TEXTURE0]);
    glActiveTexture(GL_TEXTURE1);
    glBindTexture(GL_TEXTURE_2D, ppong_tex[PPONG1]);

    glDrawArrays(GL_TRIANGLES, 0, 6);
}

int main(int argc, char **argv)
{
    char *wood_img = load_file("wood.rgb"); 
    char *container_img = load_file("container2.rgb");

    create_window(width, height);

    glGenVertexArrays(ARRAY_SZ(vao), vao);
    glGenBuffers(ARRAY_SZ(buf), buf);
    for(int i = 0; i < ARRAY_SZ(vao); ++i){
        glBindVertexArray(vao[i]);
        glBindBuffer(GL_ARRAY_BUFFER, buf[i]);
        glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);
        glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 8*sizeof(GLfloat), 0);
        glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 8*sizeof(GLfloat), (void*)(3*sizeof(GLfloat)));
        glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, 8*sizeof(GLfloat), (void*)(6*sizeof(GLfloat)));
        glEnableVertexAttribArray(0);
        glEnableVertexAttribArray(1);
        glEnableVertexAttribArray(2);
    }

    glGenTextures(1, &box_tex);
    glBindTexture(GL_TEXTURE_2D, box_tex);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_SRGB, 512, 512,
                                0, GL_RGB, GL_UNSIGNED_BYTE, container_img);

    glGenTextures(1, &floor_tex);
    glBindTexture(GL_TEXTURE_2D, floor_tex);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_SRGB, 1024, 1024,
                                0, GL_RGB, GL_UNSIGNED_BYTE, wood_img);

    load_shader("box.vs", "box.fs", &box_vs, &box_fs, &box_ph);
    load_shader("light_box.vs", "light_box.fs", &light_box_vs, &light_box_fs, &light_box_ph);

    // Framebuffers
    glGenFramebuffers(1, &fbo);
    glBindFramebuffer(GL_FRAMEBUFFER, fbo);
    glGenTextures(ARRAY_SZ(color_tex), color_tex);
    for(int i = COLOR_TEXTURE0; i < COLOR_TEXTURE_LAST; ++i){
        glBindTexture(GL_TEXTURE_2D, color_tex[i]);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA16F, width, height,
                                    0, GL_RGBA, GL_FLOAT, NULL);
        glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0 + i, GL_TEXTURE_2D,
                color_tex[i], 0);
    }
    glGenRenderbuffers(1, &rbo);
    glBindRenderbuffer(GL_RENDERBUFFER, rbo);
    glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT, width, height);
    glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, rbo);
    glDrawBuffers(ARRAY_SZ(draw_buf), draw_buf);
    glBindFramebuffer(GL_FRAMEBUFFER, 0);

    // Blur settings
    glGenVertexArrays(1, &blur_vao);
    glBindVertexArray(blur_vao);
    glGenBuffers(1, &blur_buf);
    glBindBuffer(GL_ARRAY_BUFFER, blur_buf);
    glBufferData(GL_ARRAY_BUFFER, sizeof(vertices_quad), vertices_quad, GL_STATIC_DRAW);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 5*sizeof(GLfloat), 0);
    glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 5*sizeof(GLfloat), (void*)(3*sizeof(GLfloat)));
    glEnableVertexAttribArray(0);
    glEnableVertexAttribArray(1);
    glGenFramebuffers(ARRAY_SZ(ppong_fbo), ppong_fbo);
    glGenTextures(ARRAY_SZ(ppong_tex), ppong_tex);
    for(int i = 0; i < PPONG_LAST; ++i){
        glBindFramebuffer(GL_FRAMEBUFFER, ppong_fbo[i]);
        glBindTexture(GL_TEXTURE_2D, ppong_tex[i]);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA16F, width, height,
                                    0, GL_RGBA, GL_FLOAT, NULL);
        glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D,
                ppong_tex[i], 0);
    }
    glBindFramebuffer(GL_FRAMEBUFFER, 0);
    load_shader("blur.vs", "blur.fs", &blur_vs, &blur_fs, &blur_ph);

    // Final scene settings
    glGenVertexArrays(1, &final_vao);
    glBindVertexArray(final_vao);
    glGenBuffers(1, &final_buf);
    glBindBuffer(GL_ARRAY_BUFFER, final_buf);
    glBufferData(GL_ARRAY_BUFFER, sizeof(vertices_quad), vertices_quad, GL_STATIC_DRAW);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 5*sizeof(GLfloat), 0);
    glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 5*sizeof(GLfloat), (void*)(3*sizeof(GLfloat)));
    glEnableVertexAttribArray(0);
    glEnableVertexAttribArray(1);
    load_shader("final.vs", "final.fs", &final_vs, &final_fs, &final_ph);

    glEnable(GL_DEPTH_TEST);

    window_loop();

    return 0;
}
