// vim: set ft=glsl:
#version 330 core

layout (location = 0) in vec3 coord;
layout (location = 1) in vec2 tcoord;

out vec2 tpos;

void main(){
    gl_Position = vec4(coord, 1.0);

    tpos = tcoord;
}

