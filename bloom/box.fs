// vim: set ft=glsl:
#version 330 core

layout (location = 0) out vec4 FragColor;
layout (location = 1) out vec4 BrightColor;

in VS_OUT{
    vec3 coord;
    vec3 norm;
    vec2 tcoord;
}vs_out;

struct LightSource{
    vec3 pos;
    vec3 color;
};

//out vec4 FragColor;

uniform sampler2D tex;
uniform LightSource light_source[4];
uniform vec3 view_pos;

void main(){
    vec3 color = texture(tex, vs_out.tcoord).rgb;
    vec3 N = normalize(vs_out.norm);
    vec3 V = normalize(view_pos - vs_out.coord);

    float Kamb = 0.1;
    vec3 Iamb = Kamb*vec3(1.0);

    float Kdiff = 1.0;
    vec3 Idiff_all = vec3(0.0);
    for(int i = 0; i < 4; ++i){
        vec3 I = normalize(vs_out.coord - light_source[i].pos);
        float D = length(vs_out.coord - light_source[i].pos);
        vec3 Idiff = Kdiff*max(dot(-I, N), 0.0)*light_source[i].color*color;

        Idiff_all += Idiff / (D*D);
    }

    vec3 color_final = Iamb + Idiff_all;
    float brightness = dot(color_final, vec3(0.2126, 0.7152, 0.0722));

    FragColor = vec4(color_final, 1.0);

    if(brightness > 1.0){
        BrightColor = vec4(color_final, 1.0);
    }else{
        BrightColor = vec4(0.0, 0.0, 0.0, 1.0);
    }
}
