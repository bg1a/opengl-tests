// vim: set ft=glsl:
#version 330 core

in vec2 tpos;

out vec4 FragColor;
uniform sampler2D tex;
uniform bool horizontal;

uniform float weight[5] = float[] (0.2270270270, 0.1945945946, 0.1216216216, 0.0540540541, 0.0162162162);

void main(){
    vec3 color = texture(tex, tpos).rgb;
    vec2 tex_off = 1.0 / textureSize(tex, 0);

    vec3 result = color * vec3(weight[0]);

    if(horizontal){
        for(int i = 1; i < 5; ++i){
            result += texture(tex, vec2(tpos.x + tex_off.x*i, tpos.y)).rgb * vec3(weight[i]);
            result += texture(tex, vec2(tpos.x - tex_off.x*i, tpos.y)).rgb * vec3(weight[i]);
        }
    }else{
        for(int i = 1; i < 5; ++i){
            result += texture(tex, vec2(tpos.x, tpos.y + tex_off.y*i)).rgb * vec3(weight[i]);
            result += texture(tex, vec2(tpos.x, tpos.y - tex_off.y*i)).rgb * vec3(weight[i]);
        }
    }

    FragColor = vec4(result, 1.0);
}

