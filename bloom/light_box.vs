// vim: set ft=glsl:
#version 330 core

layout (location = 0) in vec3 coord;
layout (location = 1) in vec3 norm;
layout (location = 2) in vec2 tcoord;

uniform mat4 model;
uniform mat4 view;
uniform mat4 proj;

void main(){
    gl_Position = proj*view*model*vec4(coord, 1.0);
}
