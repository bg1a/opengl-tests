#include <stdio.h>
#include <stdlib.h>

#define USE_GL 1

#include <EGL/egl.h>
#include <EGL/eglext.h>

Display *display;
int screen;
Window win = 0; 

EGLDisplay eglDisplay = EGL_NO_DISPLAY;
EGLSurface eglSurface = EGL_NO_SURFACE;
EGLContext eglContext = EGL_NO_CONTEXT;

extern void draw_opengl(void);

void
create_window(int width, int height)
{
    int xpos = 0;
    int ypos = 0;

#if USE_GL
    EGLint configAttrs[] = {
        EGL_RED_SIZE,        1,
        EGL_GREEN_SIZE,      1,
        EGL_BLUE_SIZE,       1,
        //EGL_DEPTH_SIZE,    16,
        EGL_SAMPLE_BUFFERS,  0,
        EGL_SAMPLES,         0,
	EGL_CONFORMANT,      EGL_OPENGL_BIT,
	//EGL_RENDERABLE_TYPE,      EGL_OPENGL_BIT,
	//EGL_CONTEXT_OPENGL_CORE_PROFILE_BIT_KHR, 1,
        EGL_NONE
    };
    EGLint contextAttrs[] = {
      //EGL_CONTEXT_MAJOR_VERSION_KHR, 4,
      //EGL_CONTEXT_MINOR_VERSION_KHR, 3,
      EGL_NONE
    };
#else
    EGLint configAttrs[] = {
        EGL_RED_SIZE,        1,
        EGL_GREEN_SIZE,      1,
        EGL_BLUE_SIZE,       1,
        EGL_DEPTH_SIZE,    16,
        EGL_SAMPLE_BUFFERS,  0,
        EGL_SAMPLES,         0,
	//EGL_CONFORMANT,      EGL_OPENGL_BIT,
	EGL_RENDERABLE_TYPE, EGL_OPENGL_ES2_BIT,  //3_BIT_KHR,
        EGL_NONE
    };
    EGLint contextAttrs[] = {
      EGL_CONTEXT_CLIENT_VERSION, 3,
      EGL_NONE
    };
#endif

    EGLint windowAttrs[] = { EGL_NONE};
    EGLConfig* configList = NULL;
    EGLint     configCount;

    display = XOpenDisplay(NULL);
    if (!display) 
      printf("Error opening X display.\n");

    screen = DefaultScreen(display);

    eglDisplay = eglGetDisplay(display);
    if (eglDisplay == EGL_NO_DISPLAY) 
      printf("EGL failed to obtain display\n");

    if (!eglInitialize(eglDisplay, 0, 0)) 
      printf("EGL failed to initialize\n");

    if (!eglChooseConfig(eglDisplay, configAttrs,
                         NULL, 0, &configCount) || !configCount) 
      printf("EGL failed to return any matching configurations\n");

    configList = (EGLConfig*)malloc(configCount * sizeof(EGLConfig));

    if (!eglChooseConfig(eglDisplay, configAttrs,
                         configList, configCount, &configCount) || !configCount) 
        printf("EGL failed to populate configuration list\n");

    win = XCreateSimpleWindow(display, RootWindow(display, screen),
                              xpos, ypos, width, height, 0,
                              BlackPixel(display, screen),
                              WhitePixel(display, screen));


    XStoreName( display, win, "New win");
    
    XSelectInput(display, win, ExposureMask | ButtonPressMask | KeyPressMask | StructureNotifyMask | ButtonReleaseMask |
                          KeyReleaseMask | EnterWindowMask | LeaveWindowMask |
                          PointerMotionMask | Button1MotionMask | Button2MotionMask | VisibilityChangeMask |
                          ColormapChangeMask);
    
    XMapWindow(display, win);

    eglSurface = eglCreateWindowSurface(eglDisplay, configList[0],
                                        (EGLNativeWindowType) win, windowAttrs);
    if (!eglSurface) 
      printf("EGL couldn't create window\n");

#ifdef USE_GL
    eglBindAPI(EGL_OPENGL_API);
#else
    eglBindAPI(EGL_OPENGL_ES_API);
#endif

    eglContext = eglCreateContext(eglDisplay, configList[0],
                                  NULL, contextAttrs);
    if (!eglContext) 
      printf("EGL couldn't create context\n");
    
    if (!eglMakeCurrent(eglDisplay, eglSurface, eglSurface, eglContext)) 
      printf("EGL couldn't make context/surface current\n");

}

void window_loop()
{

    XEvent event;		
    KeySym key;		
    char text[255];		

    while (true) 
    {		
        if (XPending(display)) 
        {	  
            XNextEvent(display, &event);

            if (event.type==Expose && event.xexpose.count==0) 
            {
                printf("Redraw requested!\n");
            }
            if (event.type==KeyPress&&
                    XLookupString(&event.xkey,text,255,&key,0)==1) 
            {
                if (text[0] == 27) break;

                printf("You pressed the %c key!\n",text[0]);
            }

            if (event.type==ButtonPress) 
            {
                printf("Mouse button %d press at (%d,%d)\n",
                        event.xbutton.button, event.xbutton.x,event.xbutton.y);
            }

            if (event.type==ButtonRelease) 
            {
                printf("Mouse button %d released at (%d,%d)\n",
                        event.xbutton.button, event.xbutton.x,event.xbutton.y);

            }

            if (event.type == MotionNotify)
            {
                printf("Mouse motion towards %d %d\n", 
                        event.xmotion.x, event.xmotion.y); 
            }
        }

        draw_opengl();
        eglSwapBuffers(eglDisplay, eglSurface);
    }

    if (eglDisplay != EGL_NO_DISPLAY) 
    {
        eglMakeCurrent(eglDisplay, EGL_NO_SURFACE, EGL_NO_SURFACE, EGL_NO_CONTEXT);

        if (eglContext != EGL_NO_CONTEXT)
            eglDestroyContext(eglDisplay, eglContext);

        if (eglSurface != EGL_NO_SURFACE)
            eglDestroySurface(eglDisplay, eglSurface);

        eglTerminate(eglDisplay);
    }

    if (win)
        XDestroyWindow(display, win);

    XCloseDisplay(display); 
}

