#include <stdlib.h>
#include <stdio.h>
#include <GLES3/gl31.h>
#include <GL/gl.h>

extern void create_window(int width, int height);
extern void window_loop();

//GLfloat vs[] = 
//{
//    -0.5,  0.5,
//     0.5,  0.5,
//     0.5, -0.5, 
//    -0.5, -0.5,    
//};
GLfloat vs[] = 
{
    0.0, 0.0,
};

const char *vsh = 
"#version 330 core\n"
"layout (location=0) in vec2 vpos;"
"out vec4 position;"
"void main(){"
" gl_Position = vec4(vpos, 0.0, 1.0);"
"}";
const char *fsh = 
"#version 330 core\n"
"out vec4 FragColor;"
"void main(){"
" FragColor = vec4(0.12, 0.0, 1.0, 1.0);"
"}";

//const char *gsh =
//"#version 330 core\n"
//"layout (points) in;"
//"layout (line_strip, max_vertices = 2) out;"
//"void main(){"
//" gl_Position = gl_in[0].gl_Position + vec4(-0.1, 0.0, 0.0, 0.0);"
//" EmitVertex();"
//" gl_Position = gl_in[0].gl_Position + vec4(0.1, 0.0, 0.0, 0.0);"
//" EmitVertex();"
//" EndPrimitive();"
//"}";
const char *gsh = 
"#version 330 core\n"
"layout (points) in;"
"layout (triangle_strip, max_vertices = 5) out;"
"void main(){"
" gl_Position = gl_in[0].gl_Position + vec4(-0.2, -0.2, 0.0, 0.0);"
" EmitVertex();"
" gl_Position = gl_in[0].gl_Position + vec4(0.2, -0.2, 0.0, 0.0);"
" EmitVertex();"
" gl_Position = gl_in[0].gl_Position + vec4(-0.2, 0.2, 0.0, 0.0);"
" EmitVertex();"
" gl_Position = gl_in[0].gl_Position + vec4(0.2, 0.2, 0.0, 0.0);"
" EmitVertex();"
" gl_Position = gl_in[0].gl_Position + vec4(0.0, 0.3, 0.0, 0.0);"
" EmitVertex();"
" EndPrimitive();"
"}";


GLuint vao, buf, vh, fh, gh, ph;

void draw_opengl(void)
{
    glClear(GL_COLOR_BUFFER_BIT);
    //glClearColor(0.02, 0.0, 0.02, 1.0);

    glDrawArrays(GL_POINTS, 0, 4);
}

int main(int argc, char **argv)
{
    GLchar infoLog[512] = {0};
    create_window(800, 600);

    glGenVertexArrays(1, &vao);
    glBindVertexArray(vao);
    glGenBuffers(1, &buf);
    glBindBuffer(GL_ARRAY_BUFFER, buf);
    glBufferData(GL_ARRAY_BUFFER, sizeof(vs), vs, GL_STATIC_DRAW);
    glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 2*sizeof(GLfloat), 0);
    glEnableVertexAttribArray(0);

    vh = glCreateShader(GL_VERTEX_SHADER);
    glShaderSource(vh, 1, &vsh, NULL);
    glCompileShader(vh);
    glGetShaderInfoLog(vh, 512, NULL, infoLog);
    printf("Shader log: %s\n", infoLog);

    fh = glCreateShader(GL_FRAGMENT_SHADER);
    glShaderSource(fh, 1, &fsh, NULL);
    glCompileShader(fh);
    glGetShaderInfoLog(fh, 512, NULL, infoLog);
    printf("Shader log: %s\n", infoLog);

    gh = glCreateShader(GL_GEOMETRY_SHADER);
    glShaderSource(gh, 1, &gsh, NULL);
    glCompileShader(gh);
    glGetShaderInfoLog(gh, 512, NULL, infoLog);
    printf("Shader log: %s\n", infoLog);

    ph = glCreateProgram();
    glAttachShader(ph, vh);
    glAttachShader(ph, fh);
    glAttachShader(ph, gh);
    glLinkProgram(ph);

    glGetShaderInfoLog(ph, 512, NULL, infoLog);
    printf("Shader log: %s\n", infoLog);

    glUseProgram(ph);

    window_loop();

    return 0;
}
