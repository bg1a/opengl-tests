#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "glad.h"
#define GLFW_INCLUDE_NONE
#include <GLFW/glfw3.h>

const int width = 800;
const int height = 600;

GLfloat vs[] = 
{
    -1.0, -1.0,
     1.0, -1.0,
    -1.0,  1.0,
     1.0,  1.0,
};

const char *vsh = 
"#version 430 core\n"
"layout (location = 0) in vec2 vpos;"
"void main(){"
" gl_Position = vec4(vpos, 0.0, 1.0);"
"}";
const char *fsh = 
"#version 430 core\n"
"out vec4 FragColor;"
"uniform sampler2D tex;"
"void main(){"
" FragColor = texture(tex, vec2(gl_FragCoord.xy) / textureSize(tex, 0));"
"}";
const char *csh = 
"#version 430 core \n"
"layout (local_size_x = 8, local_size_y = 8) in;"
"layout (binding = 0, rgba32f) uniform image2D img;"
"void main(){"
"  imageStore(img, ivec2(gl_GlobalInvocationID.xy), vec4(vec2(gl_LocalInvocationID.xy) / vec2(gl_WorkGroupSize.xy), 0.0, 1.0));"
"}";

GLuint vao, buf, vh, fh, ph, tex;
GLuint ch, cph;
GLFWwindow* window;
void draw_opengl(void)
{
    glUseProgram(cph);
    glBindImageTexture(0, tex, 0, GL_FALSE, 0, GL_WRITE_ONLY, GL_RGBA32F);
    glDispatchCompute(100, 75, 1);

    glClear(GL_COLOR_BUFFER_BIT);
    glBindVertexArray(vao);
    glUseProgram(ph);
    glBindTexture(GL_TEXTURE_2D, tex);
    glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
}

int main(int argc, char **argv)
{
    GLchar infoLog[512] = {0};

    glfwInit();
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
    if(!(window = glfwCreateWindow(width, height, "OpenGl memory", NULL, NULL))){
        printf("Failed to create a window!\n");
        return -1;
    }
    glfwMakeContextCurrent(window);

    gladLoadGL();
    glfwSwapInterval(1);

    glGenVertexArrays(1, &vao);
    glBindVertexArray(vao);
    glGenBuffers(1, &buf);
    glBindBuffer(GL_ARRAY_BUFFER, buf);
    glBufferData(GL_ARRAY_BUFFER, sizeof(vs), vs, GL_STATIC_DRAW);
    glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 2*sizeof(GLfloat), 0);
    glEnableVertexAttribArray(0);

    vh = glCreateShader(GL_VERTEX_SHADER);
    glShaderSource(vh, 1, &vsh, NULL);
    glCompileShader(vh);
    glGetShaderInfoLog(vh, 512, NULL, infoLog);
    printf("Vertex shader log: %s\n", infoLog);

    fh = glCreateShader(GL_FRAGMENT_SHADER);
    glShaderSource(fh, 1, &fsh, NULL);
    glCompileShader(fh);
    glGetShaderInfoLog(fh, 512, NULL, infoLog);
    printf("Fragment shader log: %s\n", infoLog);

    ph = glCreateProgram();
    glAttachShader(ph, vh);
    glAttachShader(ph, fh);
    glLinkProgram(ph);

    glGetShaderInfoLog(ph, 512, NULL, infoLog);
    printf("Program log: %s\n", infoLog);

    glUniform1i(glGetUniformLocation(ph, "tex"), 0);
    glActiveTexture(GL_TEXTURE0);

    ch = glCreateShader(GL_COMPUTE_SHADER);
    glShaderSource(ch, 1, &csh, NULL);
    glCompileShader(ch);
    glGetShaderInfoLog(ch, 512, NULL, infoLog);
    printf("Compute shader log: %s\n", infoLog);

    cph = glCreateProgram();
    glAttachShader(cph, ch);
    glLinkProgram(cph);

    glGetShaderInfoLog(cph, 512, NULL, infoLog);
    printf("Program log: %s\n", infoLog);

    glGenTextures(1, &tex);
    glBindTexture(GL_TEXTURE_2D, tex);
    glTexStorage2D(GL_TEXTURE_2D, 1, GL_RGBA32F, width, height);

    while(!glfwWindowShouldClose(window)){
        int width, height;
        glfwGetFramebufferSize(window, &width, &height);
        glViewport(0, 0, width, height);

        draw_opengl();
        glfwSwapBuffers(window);
        glfwPollEvents();
    }
    glfwDestroyWindow(window);
    glfwTerminate();

    return 0;
}
