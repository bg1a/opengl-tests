// vim: set ft=glsl:
#version 330 core

layout (location = 0) in vec3 in_coord;
layout (location = 1) in vec2 in_tex_coord;

out vec2 tex_coord;

void main(){
    gl_Position = vec4(in_coord, 1.0);

    tex_coord = in_tex_coord;
}
