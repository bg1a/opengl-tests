#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include <GLES3/gl32.h>
#include <GL/gl.h>

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

#include "../model-loader/loader.h"

#define ARRAY_SZ(_a) \
    (sizeof(_a) / sizeof(_a[0]))

const float camX = 10.0f;
const float camY = 0.0f;
const float camZ = 10.0f;
const int width = 800;
const int height = 600;
const float aspect = (float)width / (float)height;
GLfloat view_pos[] = {
    camX, camY, camZ
};

struct LightSource{
    glm::vec3 coord;
    glm::vec3 color;
}light_src[] = {
    {
        {0.0, 0.5, 4.5},
        {5.0, 5.0, 5.0},
    },
    {
        {-4.0, 0.5, -3.0},
        {10.0, 0.0, 0.0},
    },
    {
        {3.0, 0.5, 1.0},
        {0.0, 0.0, 15.0},
    },
    {
        {-0.8, 2.4, -1.0},
        {0.0, 5.0, 0.0},
    }
};

const std::vector<glm::vec3> positions = {
    glm::vec3(-3.0,  -0.5, -3.0),
    glm::vec3( 0.0,  -0.5, -3.0),
    glm::vec3( 3.0,  -0.5, -3.0),
    glm::vec3(-3.0,  -0.5,  0.0),
    glm::vec3( 0.0,  -0.5,  0.0),
    glm::vec3( 3.0,  -0.5,  0.0),
    glm::vec3(-3.0,  -0.5,  3.0),
    glm::vec3( 0.0,  -0.5,  3.0),
    glm::vec3( 3.0,  -0.5,  3.0),
};

GLfloat vertices_cube[] = {
    //Vertex           Texture
   -1.0, -1.0, -1.0,   0.0, 0.0,                                                             
    1.0, -1.0, -1.0,   1.0, 0.0,                                                             
    1.0,  1.0, -1.0,   1.0, 1.0,                                                             
    1.0,  1.0, -1.0,   1.0, 1.0,                                                             
   -1.0,  1.0, -1.0,   0.0, 1.0,                                                             
   -1.0, -1.0, -1.0,   0.0, 0.0,                                                             
                     
   -1.0, -1.0,  1.0,   0.0, 0.0,                                                            
    1.0, -1.0,  1.0,   1.0, 0.0,                                                            
    1.0,  1.0,  1.0,   1.0, 1.0,                                                            
    1.0,  1.0,  1.0,   1.0, 1.0,                                                            
   -1.0,  1.0,  1.0,   0.0, 1.0,                                                            
   -1.0, -1.0,  1.0,   0.0, 0.0,                                                            

   -1.0,  1.0,  1.0,   1.0, 1.0,                                                   
   -1.0,  1.0, -1.0,   0.0, 1.0,                                                   
   -1.0, -1.0, -1.0,   0.0, 0.0,                                                   
   -1.0, -1.0, -1.0,   0.0, 0.0,                                                   
   -1.0, -1.0,  1.0,   1.0, 0.0,                                                   
   -1.0,  1.0,  1.0,   1.0, 1.0,                                                   
                                
    1.0,  1.0,  1.0,   0.0, 0.0,                                                   
    1.0,  1.0, -1.0,   1.0, 0.0,                                                   
    1.0, -1.0, -1.0,   1.0, 1.0,                                                   
    1.0, -1.0, -1.0,   1.0, 1.0,                                                   
    1.0, -1.0,  1.0,   0.0, 1.0,                                                   
    1.0,  1.0,  1.0,   0.0, 0.0,                                                   

   -1.0, -1.0, -1.0,   0.0, 0.0,                                                  
    1.0, -1.0, -1.0,   1.0, 0.0,                                                  
    1.0, -1.0,  1.0,   1.0, 1.0,                                                  
    1.0, -1.0,  1.0,   1.0, 1.0,                                                  
   -1.0, -1.0,  1.0,   0.0, 1.0,                                                  
   -1.0, -1.0, -1.0,   0.0, 0.0,                                                  
                                
    1.0,  1.0,  1.0,   1.0, 1.0,                                                  
    1.0,  1.0, -1.0,   1.0, 0.0,                                                  
   -1.0,  1.0, -1.0,   0.0, 0.0,                                                  
    1.0,  1.0,  1.0,   1.0, 1.0,                                                  
   -1.0,  1.0, -1.0,   0.0, 0.0,                                                  
   -1.0,  1.0,  1.0,   0.0, 1.0,                                                  
};

GLfloat vertices_plane[] = {
    -1.0,   1.0,  0.0,      0.0, 1.0,
    -1.0,  -1.0,  0.0,      0.0, 0.0,
     1.0,   1.0,  0.0,      1.0, 1.0,
     1.0,  -1.0,  0.0,      1.0, 0.0,
};

extern void create_window(int width, int height);
extern void window_loop(void);

char *load_file(const char *filename)
{
    FILE *f = fopen(filename, "r");
    char *text = NULL;

    if(f){
        size_t size = 0;

        fseek(f, 0, SEEK_END);
        size = ftell(f);
        rewind(f);

        text = (char*)calloc(size + 1, sizeof(char));
        fread(text, sizeof(char), size, f);
        fclose(f); 
    }

    return text;
}

void load_shader(const char *vertex_source, const char *fragment_source,
        GLuint *vertex, GLuint *fragment, GLuint *program)
{
    const char * test_str = "#version ";
    char infoLog[512] = {0};
    char *shader_source = NULL;

    *vertex = glCreateShader(GL_VERTEX_SHADER);
    if(!strcmp(vertex_source, test_str)){
        glShaderSource(*vertex, 1, &vertex_source, NULL);
        glCompileShader(*vertex);
        glGetShaderInfoLog(*vertex, sizeof(infoLog), NULL, infoLog);
        printf("Vertex shader compile log: %s\n", infoLog);
    }else{
        shader_source = load_file(vertex_source);
        glShaderSource(*vertex, 1, &shader_source, NULL);
        glCompileShader(*vertex);
        glGetShaderInfoLog(*vertex, sizeof(infoLog), NULL, infoLog);
        printf("%s, compile log: %s\n", vertex_source, infoLog);
        free(shader_source);
    }

    *fragment = glCreateShader(GL_FRAGMENT_SHADER);
    if(!strcmp(fragment_source, test_str)){
        glShaderSource(*fragment, 1, &fragment_source, NULL);
        glCompileShader(*fragment);
        glGetShaderInfoLog(*fragment, sizeof(infoLog), NULL, infoLog);
        printf("Fragment shader compile log: %s\n", infoLog);
    }else{
        shader_source = load_file(fragment_source);
        glShaderSource(*fragment, 1, &shader_source, NULL);
        glCompileShader(*fragment);
        glGetShaderInfoLog(*fragment, sizeof(infoLog), NULL, infoLog);
        printf("%s, compile log: %s\n", fragment_source, infoLog);
        free(shader_source);
    }

    *program = glCreateProgram();
    glAttachShader(*program, *vertex);
    glAttachShader(*program, *fragment);
    glLinkProgram(*program);
}

std::vector<GLuint> vao, buf;
std::vector<size_t> vertices_num;
GLuint tex, tex_normal, tex_specular;

GLuint g_buf_vs, g_buf_fs, g_buf_ph;
GLuint g_fbo, g_rbo, g_coord_tex, g_col_tex, g_norm_tex;

GLuint plane_vao, plane_buf, plane_vs, plane_fs, plane_ph;
GLuint cube_vao, cube_buf, cube_vs, cube_fs, cube_ph;

void draw_opengl(char c)
{
    glm::mat4 view = glm::lookAt(glm::vec3(camX, camY, camZ),
                                 glm::vec3(0.0f, 0.0f, 0.0f),
                                 glm::vec3(0.0f, 1.0f, 0.0f));
    glm::mat4 proj = glm::perspective(glm::radians(45.0f), aspect, 0.1f, 100.0f);

    // Render scene objects
    glClearColor(0.0, 0.0, 0.0, 1.0);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);


    glBindFramebuffer(GL_FRAMEBUFFER, g_fbo);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    glUseProgram(g_buf_ph);

    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, tex);
    glUniform1i(glGetUniformLocation(g_buf_ph, "tex"), 0);

    glActiveTexture(GL_TEXTURE1);
    glBindTexture(GL_TEXTURE_2D, tex_normal);
    glUniform1i(glGetUniformLocation(g_buf_ph, "tex_normal"), 1);

    glActiveTexture(GL_TEXTURE2);
    glBindTexture(GL_TEXTURE_2D, tex_specular);
    glUniform1i(glGetUniformLocation(g_buf_ph, "tex_specular"), 2);

    for(const auto &pos : positions){
        glm::mat4 model(1.0);
        model = glm::translate(model, pos);
        model = glm::scale(model, glm::vec3(0.5f));

        glUniformMatrix4fv(glGetUniformLocation(g_buf_ph, "model"), 1, GL_FALSE, glm::value_ptr(model));
        glUniformMatrix4fv(glGetUniformLocation(g_buf_ph, "view"), 1, GL_FALSE, glm::value_ptr(view));
        glUniformMatrix4fv(glGetUniformLocation(g_buf_ph, "proj"), 1, GL_FALSE, glm::value_ptr(proj));

        for(int i = 0; i < vao.size(); ++i){
            glBindVertexArray(vao[i]);
            glDrawArrays(GL_TRIANGLES, 0, vertices_num[i]);
        }
    }
    glBindFramebuffer(GL_FRAMEBUFFER, 0);

    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    // Render final scene

    glUseProgram(plane_ph);

    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, g_col_tex);
    glUniform1i(glGetUniformLocation(plane_ph, "color_tex"), 0);

    glActiveTexture(GL_TEXTURE1);
    glBindTexture(GL_TEXTURE_2D, g_coord_tex);
    glUniform1i(glGetUniformLocation(plane_ph, "coord_tex"), 1);

    glActiveTexture(GL_TEXTURE2);
    glBindTexture(GL_TEXTURE_2D, g_norm_tex);
    glUniform1i(glGetUniformLocation(plane_ph, "normal_tex"), 2);

    for(int i = 0; i < ARRAY_SZ(light_src); ++i){
        char coord_buf[30] = {0};
        char color_buf[30] = {0};

        snprintf(coord_buf, sizeof(coord_buf), "ls[%d].coord", i);
        snprintf(color_buf, sizeof(color_buf), "ls[%d].color", i);

        glUniform3fv(glGetUniformLocation(plane_ph, coord_buf), 1, glm::value_ptr(light_src[i].coord));
        glUniform3fv(glGetUniformLocation(plane_ph, color_buf), 1, glm::value_ptr(light_src[i].color));
    }
    glUniform3fv(glGetUniformLocation(plane_ph, "view_pos"), 1, glm::value_ptr(glm::vec3(camX, camY, camZ)));

    glBindVertexArray(plane_vao);
    glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);

    // Copy depth component
    glBindFramebuffer(GL_READ_FRAMEBUFFER, g_fbo);
    glBindFramebuffer(GL_DRAW_FRAMEBUFFER, 0);
    glBlitFramebuffer(0, 0, width, height, 0, 0, width, height, GL_DEPTH_BUFFER_BIT, GL_NEAREST);
    glBindFramebuffer(GL_FRAMEBUFFER, 0);

    // Render lights
    glUseProgram(cube_ph);

    for(const auto &ls : light_src){
        glm::mat4 model(1.0);
        model = glm::translate(model, ls.coord);
        model = glm::scale(model, glm::vec3(0.1f));

        glUniformMatrix4fv(glGetUniformLocation(cube_ph, "model"), 1, GL_FALSE, glm::value_ptr(model));
        glUniformMatrix4fv(glGetUniformLocation(cube_ph, "view"), 1, GL_FALSE, glm::value_ptr(view));
        glUniformMatrix4fv(glGetUniformLocation(cube_ph, "proj"), 1, GL_FALSE, glm::value_ptr(proj));
    
        glUniform3fv(glGetUniformLocation(cube_ph, "color"), 1, glm::value_ptr(ls.color));
        glBindVertexArray(cube_vao);
        glDrawArrays(GL_TRIANGLES, 0, 36);
    }
}

int main(int argc, char **argv)
{
    char *img = load_file("diffuse.rgb");
    char *img_normal = load_file("normal.rgb");
    char *img_spec = load_file("specular.rgb");

    create_window(width, height);
    {
        std::vector<std::vector<float>> vao_vertices;
        std::vector<LoaderObject> objects;

        load_obj("../model-loader/backpack.obj", objects);
        for(const auto &obj : objects){
            vao_vertices.push_back(get_vao_data(obj));
        }

        vao.resize(vao_vertices.size());
        buf.resize(vao_vertices.size());
        glGenVertexArrays(vao.size(), vao.data());
        glGenBuffers(buf.size(), buf.data());
        for(int i = 0; i < vao.size(); ++i){
            glBindVertexArray(vao[i]);
            glBindBuffer(GL_ARRAY_BUFFER, buf[i]);
            glBufferData(GL_ARRAY_BUFFER, sizeof(vao_vertices[0]) * vao_vertices[i].size(),
                    vao_vertices[i].data(), GL_STATIC_DRAW);
            glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, VAO_LINE_SZ, (void*)VAO_VERTEX_OFFSET);
            glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, VAO_LINE_SZ, (void*)VAO_NORMAL_OFFSET);
            glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, VAO_LINE_SZ, (void*)VAO_TEXTURE_OFFSET);
            glVertexAttribPointer(3, 3, GL_FLOAT, GL_FALSE, VAO_LINE_SZ, (void*)VAO_TANGENT_OFFSET);
            glVertexAttribPointer(4, 3, GL_FLOAT, GL_FALSE, VAO_LINE_SZ, (void*)VAO_BITANGENT_OFFSET);
            glEnableVertexAttribArray(0);
            glEnableVertexAttribArray(1);
            glEnableVertexAttribArray(2);
            glEnableVertexAttribArray(3);
            glEnableVertexAttribArray(4);

            vertices_num.push_back(vao_vertices[i].size() / VAO_IDX_LAST);
        }

        objects.clear();
        vao_vertices.clear();
    }

    glGenTextures(1, &tex);
    glBindTexture(GL_TEXTURE_2D, tex);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, 4096, 4096,
                                0, GL_RGB, GL_UNSIGNED_BYTE, img);

    glGenTextures(1, &tex_normal);
    glBindTexture(GL_TEXTURE_2D, tex_normal);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, 4096, 4096,
                                0, GL_RGB, GL_UNSIGNED_BYTE, img_normal);

    glGenTextures(1, &tex_specular);
    glBindTexture(GL_TEXTURE_2D, tex_specular);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, 4096, 4096,
                                0, GL_RGB, GL_UNSIGNED_BYTE, img_spec);

    load_shader("g_buffer.vs", "g_buffer.fs", &g_buf_vs, &g_buf_fs, &g_buf_ph);

    glGenFramebuffers(1, &g_fbo);
    glBindFramebuffer(GL_FRAMEBUFFER, g_fbo);

    glGenTextures(1, &g_coord_tex);
    glBindTexture(GL_TEXTURE_2D, g_coord_tex);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA16F, width, height,
                                0, GL_RGBA, GL_FLOAT, NULL);
    glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D,
                            g_coord_tex, 0);

    glGenTextures(1, &g_col_tex);
    glBindTexture(GL_TEXTURE_2D, g_col_tex);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height,
                                0, GL_RGBA, GL_UNSIGNED_BYTE, NULL);
    glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT1, GL_TEXTURE_2D,
                            g_col_tex, 0);

    glGenTextures(1, &g_norm_tex);
    glBindTexture(GL_TEXTURE_2D, g_norm_tex);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA16F, width, height,
                                0, GL_RGBA, GL_FLOAT, NULL);
    glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT2, GL_TEXTURE_2D,
                            g_norm_tex, 0);

    GLuint draw_buffers[] = {GL_COLOR_ATTACHMENT0, GL_COLOR_ATTACHMENT1, GL_COLOR_ATTACHMENT2};
    glDrawBuffers(3, draw_buffers);

    glGenRenderbuffers(1, &g_rbo);
    glBindRenderbuffer(GL_RENDERBUFFER, g_rbo);
    glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT, width, height);
    glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, g_rbo);

    glBindFramebuffer(GL_FRAMEBUFFER, 0);

    glGenVertexArrays(1, &plane_vao);
    glBindVertexArray(plane_vao);
    glGenBuffers(1, &plane_buf);
    glBindBuffer(GL_ARRAY_BUFFER, plane_buf);
    glBufferData(GL_ARRAY_BUFFER, sizeof(vertices_plane), vertices_plane, GL_STATIC_DRAW);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 5*sizeof(float), 0);
    glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 5*sizeof(float), (void*)(3*sizeof(float)));
    glEnableVertexAttribArray(0);
    glEnableVertexAttribArray(1);

    load_shader("plane.vs", "plane.fs", &plane_vs, &plane_fs, &plane_ph);

    glGenVertexArrays(1, &cube_vao);
    glBindVertexArray(cube_vao);
    glGenBuffers(1, &cube_buf);
    glBindBuffer(GL_ARRAY_BUFFER, cube_buf);
    glBufferData(GL_ARRAY_BUFFER, sizeof(vertices_cube), vertices_cube, GL_STATIC_DRAW);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 5*sizeof(GLfloat), 0);
    glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 5*sizeof(GLfloat), (void*)(3*sizeof(GLfloat)));
    glEnableVertexAttribArray(0);
    glEnableVertexAttribArray(1);

    load_shader("cube.vs", "cube.fs", &cube_vs, &cube_fs, &cube_ph);

    glEnable(GL_DEPTH_TEST);

    free(img);
    free(img_normal);
    free(img_spec);

    window_loop();

    return 0;
}
