// vim: set ft=glsl:
#version 330 core

in vec2 tex_coord;

uniform sampler2D color_tex;
uniform sampler2D coord_tex;
uniform sampler2D normal_tex;

out vec4 FragColor;

struct LightSource{
    vec3 coord;
    vec3 color;
};

uniform LightSource ls[4];
uniform vec3 view_pos;

void main(){
    const float attenuation_linear = 0.8;
    const float attenuation_quad = 1.7;
    vec3 color = texture(color_tex, tex_coord).rgb;
    float spec = texture(color_tex, tex_coord).a;
    vec3 coord = texture(coord_tex, tex_coord).rgb;
    vec3 N = texture(normal_tex, tex_coord).rgb;
    
    vec3 Iamb = 0.1 * color;
    vec3 Itotal = Iamb;
    for(int i = 0; i < 4; ++i){
        vec3 I = normalize(coord - ls[i].coord);
        vec3 H = normalize(view_pos - I);
        float D = length(coord - ls[i].coord);

        vec3 Idiff = 0.7 * max(dot(-I, N), 0.0) * color * ls[i].color;
        vec3 Ispec = spec * pow(max(dot(H, N), 0.0), 16.0) * ls[i].color; 

        float A = 1.0 / (1.0 + attenuation_linear * D + attenuation_quad * D * D);

        Itotal += A * Idiff + A * Ispec;
    }

    FragColor = vec4(Itotal, 1.0);
}

