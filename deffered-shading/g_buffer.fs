// vim: set ft=glsl:
#version 330 core

layout (location = 0) out vec3 g_coord;
layout (location = 1) out vec4 g_color;
layout (location = 2) out vec3 g_normal;

in VS_OUT{
    vec3 coord;
    vec3 normal;
    vec2 tex_coord;
}vs_out;

//out vec4 FragColor;

uniform sampler2D albedo_tex;
uniform sampler2D spec_tex;

void main(){
    g_color.rgb = texture(albedo_tex, vs_out.tex_coord).rgb;
    g_color.a = texture(spec_tex, vs_out.tex_coord).r;
    g_normal = normalize(vs_out.normal);
    g_coord = vs_out.coord;
}
