#include <stdlib.h>
#include <stdio.h>
#include <sys/time.h>


#include <GLES3/gl31.h>

int read_pixels_flag = 0;

extern void window_loop(void);
extern void create_window(int w, int h);

GLfloat vs[] = {
    // Vertices          Texture    
    -1.0,  1.0,  0.0,    0.0, 0.0,
    -1.0, -1.0,  0.0,    1.0, 0.0,
     1.0, -1.0,  0.0,    1.0, 1.0,
    
     1.0, -1.0,  0.0,    1.0, 1.0,
     1.0,  1.0,  0.0,    0.0, 1.0,
    -1.0,  1.0,  0.0,    0.0, 0.0,
};

const char *vertex_shader = 
"#version 100\n"
"attribute vec3 vpos;"
"attribute vec2 tpos;"
"varying vec2 out_tpos;"
"uniform float xpos;"
"uniform float ypos;"
"uniform float zpos;"
"void main(void){"
"  mat4 t = mat4(1.0);"
"  mat4 s = mat4(1.0);"
"  s[0][0] = 0.5; s[1][1] = 0.5;"
"  t[3] = vec4(xpos, ypos, zpos, 1.0);"
"  gl_Position = s * t * vec4(vpos, 1.0);"
"  out_tpos = tpos;"
"}";

const char *frag_shader = 
"#version 100\n"
"precision mediump float;"
"uniform sampler2D tex;"
"varying vec2 out_tpos;"
"void main(void){"
"  gl_FragColor = texture2D(tex, out_tpos);"
"}";

GLuint vao, buf, tx, zpos, xpos, ypos;
GLuint vsh, fsh, p;

struct timeval tv_old;

void draw_opengl(void)
{
    struct timeval tv_new;
    gettimeofday(&tv_new, NULL);
    printf("Time elapsed: %lu us (%g ms)\n", tv_new.tv_usec - tv_old.tv_usec,
            (double)(tv_new.tv_usec - tv_old.tv_usec) / 1000.0);
    tv_old = tv_new;

    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glClearColor(0.05, 0.12, 0.35, 1.0);

    xpos = glGetUniformLocation(p, "xpos");
    glUniform1f(xpos, 0.5);
    ypos = glGetUniformLocation(p, "ypos");
    glUniform1f(ypos, 0.5);
    zpos = glGetUniformLocation(p, "zpos");
    glUniform1f(zpos, 0.5);

    glDrawArrays(GL_TRIANGLES, 0, 6);

    xpos = glGetUniformLocation(p, "xpos");
    glUniform1f(xpos, 0.0);
    ypos = glGetUniformLocation(p, "ypos");
    glUniform1f(ypos, 0.0);
    zpos = glGetUniformLocation(p, "zpos");
    glUniform1f(zpos, 0.0);

    glDrawArrays(GL_TRIANGLES, 0, 6);
    if(!read_pixels_flag){
        FILE *out_f;
#define PBUF_SZ 800 * 600 *4
        char pix_buf[PBUF_SZ] = {0};

        glFinish();
        glReadPixels(0, 0, 800, 600, GL_RGBA, GL_UNSIGNED_BYTE, pix_buf);
        out_f = fopen("pixels.data", "wb+");
        if(out_f){
            fwrite(pix_buf, sizeof(char), PBUF_SZ, out_f);
            fclose(out_f);
        }

        read_pixels_flag = 1;
    }
}

int main(int argc, char **argv)
{
    FILE* f = fopen("texture.data", "rb");
    unsigned char *tex_data = NULL;
    size_t tex_data_sz = 0;
    
    fseek(f, 0, SEEK_END);
    tex_data_sz = ftell(f);
    rewind(f);

    tex_data = (unsigned char *)malloc(tex_data_sz);
    fread(tex_data, sizeof(unsigned char), tex_data_sz, f);

    create_window(800, 600);

    glGenVertexArrays(1, &vao);
    glBindVertexArray(vao);
    glGenBuffers(1, &buf);
    glBindBuffer(GL_ARRAY_BUFFER, buf);
    glBufferData(GL_ARRAY_BUFFER, sizeof(vs), vs, GL_STATIC_DRAW);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 5*sizeof(GLfloat), 0);
    glEnableVertexAttribArray(0);
    glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 5*sizeof(GLfloat), (void *) (3*sizeof(GLfloat)));
    glEnableVertexAttribArray(1);

    vsh = glCreateShader(GL_VERTEX_SHADER);
    glShaderSource(vsh, 1, &vertex_shader, NULL);
    glCompileShader(vsh);
    fsh = glCreateShader(GL_FRAGMENT_SHADER);
    glShaderSource(fsh, 1, &frag_shader, NULL);
    glCompileShader(fsh);
    p = glCreateProgram();
    glAttachShader(p, vsh);
    glAttachShader(p, fsh);
    glLinkProgram(p);
    glUseProgram(p);

    glGenTextures(1, &tx);
    glBindTexture(GL_TEXTURE_2D, tx);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, 256, 256,
                                0, GL_RGBA, GL_UNSIGNED_BYTE, tex_data);

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

    glEnable(GL_DEPTH_TEST);
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

    gettimeofday(&tv_old, NULL);
    window_loop();

    free(tex_data);
    fclose(f);
    return 0;
}
