#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "glad.h"
#define GLFW_INCLUDE_NONE
#include <GLFW/glfw3.h>

const int width = 1024;
const int height = 1024;
const size_t channels = 4;
const size_t bytes_per_channel = 4;
const size_t img_byte_sz = width * height * channels * bytes_per_channel;

GLfloat vs[] = 
{
    -1.0, -1.0,
     1.0, -1.0,
    -1.0,  1.0,
     1.0,  1.0,
};

const char *vsh = 
"#version 430 core\n"
"layout (location = 0) in vec2 vpos;"
"out vec2 tp;"
"void main(){"
" gl_Position = vec4(vpos, 0.0, 1.0);"
"}";
const char *fsh = 
"#version 430 core\n"
"out vec4 FragColor;"
"uniform sampler2D tex;"
"void main(){"
" vec2 coord = vec2(gl_FragCoord.xy) / vec2(textureSize(tex, 0).xy);"
" FragColor = texture(tex, vec2(1.0 - coord.y, coord.x));"
"}";
const char *csh = 
"#version 430 core \n"
"layout (local_size_x = 1024) in;"
"layout (binding = 0, rgba32f) uniform image2D img_in;"
"layout (binding = 1, rgba32f) uniform image2D img_out;"
"shared vec4 line[1024];"
"void main(){"
"  ivec2 coord = ivec2(gl_GlobalInvocationID.xy);"
"  line[gl_LocalInvocationID.x] = imageLoad(img_in, coord);"
"  barrier();"
"  vec4 color = line[min(gl_LocalInvocationID.x + 1, 1023)] - "
"               line[max(gl_LocalInvocationID.x - 1, 0)];"
"  imageStore(img_out, coord.yx, color);"
"}";

GLuint vao, buf, vh, fh, ph, tex;
GLuint ch, cph, tex_out, tex_final;
GLFWwindow* window;
void draw_opengl(void)
{
    glUseProgram(cph);
    glBindImageTexture(0, tex, 0, GL_FALSE, 0, GL_READ_ONLY, GL_RGBA32F);
    glBindImageTexture(1, tex_out, 0, GL_FALSE, 0, GL_WRITE_ONLY, GL_RGBA32F);
    glDispatchCompute(1, 1024, 1);
    glMemoryBarrier(GL_SHADER_IMAGE_ACCESS_BARRIER_BIT);

    glBindImageTexture(0, tex_out, 0, GL_FALSE, 0, GL_READ_ONLY, GL_RGBA32F);
    glBindImageTexture(1, tex_final, 0, GL_FALSE, 0, GL_WRITE_ONLY, GL_RGBA32F);
    glDispatchCompute(1, 1024, 1);

    glClear(GL_COLOR_BUFFER_BIT);
    glBindVertexArray(vao);
    glUseProgram(ph);
    glBindTexture(GL_TEXTURE_2D, tex_out);
    glActiveTexture(GL_TEXTURE0);
    glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
}

int main(int argc, char **argv)
{
    GLchar infoLog[512] = {0};

    glfwInit();
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
    if(!(window = glfwCreateWindow(width, height, "OpenGl memory", NULL, NULL))){
        printf("Failed to create a window!\n");
        return -1;
    }
    glfwMakeContextCurrent(window);

    gladLoadGL();
    glfwSwapInterval(1);

    glGenVertexArrays(1, &vao);
    glBindVertexArray(vao);
    glGenBuffers(1, &buf);
    glBindBuffer(GL_ARRAY_BUFFER, buf);
    glBufferData(GL_ARRAY_BUFFER, sizeof(vs), vs, GL_STATIC_DRAW);
    glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 2*sizeof(GLfloat), 0);
    glEnableVertexAttribArray(0);

    vh = glCreateShader(GL_VERTEX_SHADER);
    glShaderSource(vh, 1, &vsh, NULL);
    glCompileShader(vh);
    glGetShaderInfoLog(vh, 512, NULL, infoLog);
    printf("Vertex shader log: %s\n", infoLog);

    fh = glCreateShader(GL_FRAGMENT_SHADER);
    glShaderSource(fh, 1, &fsh, NULL);
    glCompileShader(fh);
    glGetShaderInfoLog(fh, 512, NULL, infoLog);
    printf("Fragment shader log: %s\n", infoLog);

    ph = glCreateProgram();
    glAttachShader(ph, vh);
    glAttachShader(ph, fh);
    glLinkProgram(ph);

    glGetShaderInfoLog(ph, 512, NULL, infoLog);
    printf("Program log: %s\n", infoLog);

    glUniform1i(glGetUniformLocation(ph, "tex"), 0);

    ch = glCreateShader(GL_COMPUTE_SHADER);
    glShaderSource(ch, 1, &csh, NULL);
    glCompileShader(ch);
    glGetShaderInfoLog(ch, 512, NULL, infoLog);
    printf("Compute shader log: %s\n", infoLog);

    cph = glCreateProgram();
    glAttachShader(cph, ch);
    glLinkProgram(cph);

    glGetShaderInfoLog(cph, 512, NULL, infoLog);
    printf("Program log: %s\n", infoLog);

    FILE *f = fopen("test.rgba", "rb");
    unsigned char *img_data = (unsigned char*)malloc(img_byte_sz);
    fread(img_data, sizeof(char), img_byte_sz, f);
    fclose(f);

    glGenTextures(1, &tex);
    glBindTexture(GL_TEXTURE_2D, tex);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA32F, width, height,
                                0, GL_RGBA, GL_FLOAT, img_data);
    free(img_data);

    glGenTextures(1, &tex_out);
    glBindTexture(GL_TEXTURE_2D, tex_out);
    glTexStorage2D(GL_TEXTURE_2D, 1, GL_RGBA32F, width, height);

    glGenTextures(1, &tex_final);
    glBindTexture(GL_TEXTURE_2D, tex_final);
    glTexStorage2D(GL_TEXTURE_2D, 1, GL_RGBA32F, width, height);

    while(!glfwWindowShouldClose(window)){
        int width, height;
        glfwGetFramebufferSize(window, &width, &height);
        glViewport(0, 0, width, height);

        draw_opengl();
        glfwSwapBuffers(window);
        glfwPollEvents();
    }
    glfwDestroyWindow(window);
    glfwTerminate();

    return 0;
}
