#include <stdlib.h>
#include <stdio.h>

#include <GLES3/gl31.h>
#include <GL/gl.h>

extern void create_window(int width, int height);
extern void window_loop();

GLfloat vs_cube[] = 
{
    // Front
    -1.0,  1.0,  1.0,   1.0, 0.0, 0.0,
    -1.0, -1.0,  1.0,   1.0, 0.0, 0.0,
     1.0, -1.0,  1.0,   1.0, 0.0, 0.0,
     1.0, -1.0,  1.0,   0.0, 0.0, 1.0,
     1.0,  1.0,  1.0,   0.0, 1.0, 0.0,
    -1.0,  1.0,  1.0,   1.0, 0.0, 0.0, 
    // Back
     1.0,  1.0, -1.0,   0.0, 1.0, 0.0,
     1.0, -1.0, -1.0,   0.0, 1.0, 0.0,
    -1.0, -1.0, -1.0,   0.0, 1.0, 0.0,
    -1.0, -1.0, -1.0,   0.0, 0.0, 1.0,
    -1.0,  1.0, -1.0,   0.0, 1.0, 0.0,
     1.0,  1.0, -1.0,   1.0, 0.0, 0.0,
    // Left
    -1.0,  1.0, -1.0,   1.0, 0.0, 0.0,
    -1.0, -1.0, -1.0,   0.0, 1.0, 0.0,
    -1.0, -1.0,  1.0,   0.0, 0.0, 1.0,
    -1.0, -1.0,  1.0,   0.0, 0.0, 1.0,
    -1.0,  1.0,  1.0,   0.0, 1.0, 0.0,
    -1.0,  1.0, -1.0,   1.0, 0.0, 0.0,
    // Right
     1.0,  1.0,  1.0,   1.0, 0.0, 0.0,
     1.0, -1.0,  1.0,   0.0, 1.0, 0.0,
     1.0, -1.0, -1.0,   0.0, 0.0, 1.0,
     1.0, -1.0, -1.0,   0.0, 0.0, 1.0,
     1.0,  1.0, -1.0,   0.0, 1.0, 0.0,
     1.0,  1.0,  1.0,   1.0, 0.0, 0.0, 
    // Top
     1.0,  1.0,  1.0,   1.0, 0.0, 0.0,
     1.0,  1.0, -1.0,   0.0, 1.0, 0.0,
    -1.0,  1.0, -1.0,   0.0, 0.0, 1.0,
    -1.0,  1.0, -1.0,   0.0, 0.0, 1.0,
    -1.0,  1.0,  1.0,   0.0, 1.0, 0.0,
     1.0,  1.0,  1.0,   1.0, 0.0, 0.0,
    // Bottom
     1.0, -1.0,  1.0,   1.0, 0.0, 0.0,
    -1.0, -1.0,  1.0,   0.0, 1.0, 0.0,
    -1.0, -1.0, -1.0,   0.0, 0.0, 1.0,
    -1.0, -1.0, -1.0,   0.0, 0.0, 1.0,
     1.0, -1.0, -1.0,   0.0, 1.0, 0.0,
     1.0, -1.0,  1.0,   1.0, 0.0, 0.0,
};

const char *cube_vsh;
const char *cube_fsh;

char *read_file(const char *fname, const char *attr){
    char *content = NULL;
    if(fname){
        FILE *f;
        size_t fsize;

        f = fopen(fname, attr);
        fseek(f, 0, SEEK_END);
        fsize = ftell(f);
        rewind(f);
        content = (char*)calloc(fsize + 1, sizeof(char));
        fread(content, sizeof(char), fsize, f);
        fclose(f);
    }

    return content;
}

GLuint cube_vao, cube_buf, cube_vh, cube_fh, cube_ph;
GLuint vi;
void draw_opengl(void)
{
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    glDrawArrays(GL_TRIANGLES, 0, 36);
}


int main(int argc, char **argv)
{
    char infoLog[512] = {0};

    cube_vsh = read_file("main.vs", "r");
    cube_fsh = read_file("main.fs", "r");
    create_window(800, 600);

    glGenVertexArrays(1, &cube_vao);
    glBindVertexArray(cube_vao);

    glGenBuffers(1, &cube_buf);
    glBindBuffer(GL_ARRAY_BUFFER, cube_buf);
    glBufferData(GL_ARRAY_BUFFER, sizeof(vs_cube), vs_cube, GL_STATIC_DRAW);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 6*sizeof(GLfloat), 0);
    glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 6*sizeof(GLfloat), (void*)(3*sizeof(GLfloat)));
    glEnableVertexAttribArray(0);
    glEnableVertexAttribArray(1);

    cube_vh = glCreateShader(GL_VERTEX_SHADER);
    glShaderSource(cube_vh, 1, &cube_vsh, 0);
    glCompileShader(cube_vh);
    glGetShaderInfoLog(cube_vh, sizeof(infoLog), 0, infoLog);
    printf("Vertex shader log: %s\n", infoLog);

    cube_fh = glCreateShader(GL_FRAGMENT_SHADER);
    glShaderSource(cube_fh, 1, &cube_fsh, 0);
    glCompileShader(cube_fh);
    glGetShaderInfoLog(cube_fh, sizeof(infoLog), 0, infoLog);
    printf("Fragment shader log: %s\n", infoLog);

    cube_ph = glCreateProgram();
    glAttachShader(cube_ph, cube_vh);
    glAttachShader(cube_ph, cube_fh);
    glLinkProgram(cube_ph);
    glUseProgram(cube_ph);

    glEnable(GL_DEPTH_TEST);
    window_loop();

    return 0;
}
