#version 330 core
layout (location = 0) in vec3 vpos;
layout (location = 1) in vec3 col;

out vec3 fcol;

void main(){
    float zn = 1.0;
    float zf = 1000.0;

    mat4 p = mat4(1.0, 0.0,             0.0,        0.0,
                  0.0, 1.0,             0.0,        0.0,
                  0.0, 0.0, -(zn + zf)/(zn - zf),   1.0,
                  0.0, 0.0,  (2*zn*zf)/(zn - zf),   0.0);

    mat4 t = mat4(1.0,  0.0,  0.0, 0.0,
                  0.0,  1.0,  0.0, 0.0,
                  0.0,  0.0,  1.0, 0.0,
                  0.0,  0.0,  4.0, 1.0);

    mat4 v = mat4(normalize(vec4(1.0,  0.0,  0.0,  0.0)),
                  normalize(vec4(0.0,  1.0,  0.0,  0.0)),
                  normalize(vec4(0.0,  0.0, -1.0,  0.0)),
                            vec4(0.0,  0.0,  0.0,  1.0));

    gl_Position = p*inverse(t*v)*vec4(vpos, 1.0);

    fcol = col;
}

