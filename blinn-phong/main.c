#include <stdio.h>
#include <stdlib.h>
#include <GLES3/gl31.h>
#include <GL/gl.h>

bool is_bpressed = false;

GLfloat vxs[] = {
    // vertices        normal          texture 
    -10.0, -0.0,  10.0,  0.0, 1.0, 0.0,  0.0, 0.0,
     10.0, -0.0,  10.0,  0.0, 1.0, 0.0,  1.0, 0.0,
     10.0, -0.0, -10.0,  0.0, 1.0, 0.0,  1.0, 1.0,

    -10.0, -0.0,  10.0,  0.0, 1.0, 0.0,  0.0, 0.0,
     10.0, -0.0, -10.0,  0.0, 1.0, 0.0,  1.0, 1.0,
    -10.0, -0.0, -10.0,  0.0, 1.0, 0.0,  0.0, 1.0,
};

const char *vsh = 
"#version 330 core\n"
"layout (location = 0) in vec3 vpos;"
"layout (location = 1) in vec3 norm;"
"layout (location = 2) in vec2 tx_coord;"
"out vec2 tpos;"
"out vec3 coord;"
"out vec3 npos;"
"out vec3 viewer_pos;"
"void main(){"
" float zn = 0.1;"
" float zf = 100.0;"
" viewer_pos = vec3(0.0, 2.0, 2.0);"
" "
" mat4 p = mat4(1.0, 0.0,  0.0,                    0.0,"
"               0.0, 1.0,  0.0,                    0.0,"
"               0.0, 0.0, -(zn + zf)/(zn - zf),    1.0,"
"               0.0, 0.0,  2*zn*zf/(zn - zf),      0.0);"
" mat4 t = mat4(1.0,  0.0,  0.0, 0.0,"
"               0.0,  1.0,  0.0, 0.0,"
"               0.0,  0.0,  1.0, 0.0,"
"               vec4(viewer_pos, 1.0));"
" mat4 r = mat4(1.0,  0.0,    0.0,   0.0,"
"               0.0,  0.707, -0.707, 0.0,"
"               0.0,  0.707,  0.707, 0.0,"
"               0.0,  0.0,    0.0,   1.0);"
" mat4 v = mat4(vec4(1.0,  0.0,  0.0,  0.0),"
"               vec4(0.0,  1.0,  0.0,  0.0),"
"               vec4(0.0,  0.0, -1.0,  0.0),"
"               vec4(0.0,  0.0,  0.0,  1.0));"
""
" gl_Position = p*inverse(r*t*v)*vec4(vpos, 1.0);"
" tpos = tx_coord;"
" coord = vpos;"
" npos = norm;"
"}";

const char *fsh = 
"#version 330 core\n"
"in vec3 coord;"
"in vec3 npos;"
"in vec2 tpos;"
"in vec3 viewer_pos;"
"out vec4 FragColor;"
"uniform sampler2D tex;"
"uniform bool use_blinn_phong;"
"void main(){"
" vec3 lpos = vec3(0.0, 1.0, -1.0);"
" vec3 L = normalize(lpos - coord);"
" "
" float Kamb = 0.0;"
" float Iamb = Kamb;"
" "
" float Kdiff = 0.0;"
" float Idiff = Kdiff * max(dot(L, npos), 0.0);"
" "
" float Kspec = 1.0;"
" float Ispec = 1.0;"
" if(!use_blinn_phong){"
"  vec3 R = reflect(-L, npos);"
"  vec3 V = normalize(viewer_pos - coord);" 
"  float cosVR = max(dot(V, R), 0.0);"
"  Ispec = Kspec * pow(cosVR, 1.0);"
" }else{"
"  vec3 V = normalize(viewer_pos - coord);"
"  vec3 H = normalize(V + L); "
"  float cosNH = max(dot(npos, H), 0.0);"
"  Ispec = Kspec * pow(cosNH, 1.0);"
" }"
" FragColor = vec4((Iamb + Idiff + Ispec) * texture(tex, tpos).rgb, 1.0);"
"}";

char *load_file(const char *fname)
{
    char *fbuf = NULL;

    if(fname){
        FILE *f = fopen(fname, "rb");
        if(f){
            size_t fsize;

            fseek(f, 0, SEEK_END);
            fsize = ftell(f);
            rewind(f);

            fbuf = (char *)malloc(fsize);
            fread(fbuf, sizeof(char), fsize, f);
        
            fclose(f);
        }
    }else{
        printf("Bad filename!\n");
    }

    return fbuf;
}

extern void create_window(int width, int height);
extern void window_loop();

GLuint vao, buf, vh, fh, ph, tx;

void draw_opengl(char c)
{
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    if('b' == c){
        if(is_bpressed){
            glUniform1i(glGetUniformLocation(ph, "use_blinn_phong"), GL_FALSE);
            is_bpressed = false;
        }else{
            glUniform1i(glGetUniformLocation(ph, "use_blinn_phong"), GL_TRUE);
            is_bpressed = true;
        }
    }
    glDrawArrays(GL_TRIANGLES, 0, 6);
}

int main(int argc, char **argv)
{
    char *img;
    GLchar infoLog[512] = {0};

    create_window(800, 600);
    img = load_file("wood.rgb");
    
    glGenVertexArrays(1, &vao);
    glBindVertexArray(vao);
    glGenBuffers(1, &buf);
    glBindBuffer(GL_ARRAY_BUFFER, buf);
    glBufferData(GL_ARRAY_BUFFER, sizeof(vxs), vxs, GL_STATIC_DRAW);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 8*sizeof(GLfloat), 0);
    glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 8*sizeof(GLfloat), (void*)(3*sizeof(GLfloat)));
    glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, 8*sizeof(GLfloat), (void*)(6*sizeof(GLfloat)));
    glEnableVertexAttribArray(0);
    glEnableVertexAttribArray(1);
    glEnableVertexAttribArray(2);

    glGenTextures(1, &tx);
    glBindTexture(GL_TEXTURE_2D, tx);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, 1024, 1024,
                                0, GL_RGB, GL_UNSIGNED_BYTE, img);

    vh = glCreateShader(GL_VERTEX_SHADER);
    glShaderSource(vh, 1, &vsh, NULL);
    glCompileShader(vh);
    glGetShaderInfoLog(vh, 512, NULL, infoLog);
    printf("Shader log: %s\n", infoLog);

    fh = glCreateShader(GL_FRAGMENT_SHADER);
    glShaderSource(fh, 1, &fsh, NULL);
    glCompileShader(fh);
    glGetShaderInfoLog(fh, 512, NULL, infoLog);
    printf("Shader log: %s\n", infoLog);

    ph = glCreateProgram();
    glAttachShader(ph, vh);
    glAttachShader(ph, fh);
    glLinkProgram(ph);
    glUseProgram(ph);

    glUniform1i(glGetUniformLocation(ph, "tex"), 0);
    glUniform1i(glGetUniformLocation(ph, "use_blinn_phong"), GL_FALSE);

    glEnable(GL_DEPTH_TEST);

    window_loop();

    return 0;
}
