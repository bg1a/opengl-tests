// vim: set ft=glsl:
#version 330 core

in vec2 tex_coord;

out vec4 FragColor;

uniform sampler2D color_tex;
uniform sampler2D coord_tex;
uniform sampler2D normal_tex;

uniform sampler2D noise_tex;

uniform mat4 view;
uniform mat4 proj;
uniform vec3 kernel[64];

void main(){
    vec3 in_normal = texture(normal_tex, tex_coord).rgb;
    vec3 in_tangent = texture(noise_tex, vec2(tex_coord.x * 200.0, tex_coord.y * 150.0)).rgb;
    vec3 in_coord = texture(coord_tex, tex_coord).rgb;

    vec3 normal = normalize(in_normal);
    vec3 tangent = normalize(in_tangent - dot(normal, in_tangent) * normal);
    vec3 bitangent = normalize(cross(normal, tangent)); 
    mat3 TBN_inv = mat3(tangent, bitangent, normal);

    float bias = 0.025;
    float occlusion = 0.0;
    //for(int i = 0; i < 64; ++i){
    int i = 0;
        vec3 kernel_sample = normalize(kernel[i]);
        
        vec4 sample_pvm = proj * view * vec4(TBN_inv * kernel_sample, 1.0);
        vec3 sample_ndc = (sample_pvm.xyz / sample_pvm.w) / 2.0 + 0.5; 
        
        vec3 screen_point = texture(coord_tex, sample_ndc.xy).xyz;

        occlusion += (sample_ndc.z >= screen_point.z + bias ? 1.0 : 0.0);
    //}

    occlusion = occlusion / 64.0;

    //FragColor = vec4(texture(noise_tex, vec2(tex_coord.x * 200.0, tex_coord.y * 150.0)).rgb / 2.0 + 0.5, 1.0);
    //FragColor = vec4(vec3(1.0 - occlusion), 1.0); 
    FragColor = vec4(screen_point, 1.0);
}
