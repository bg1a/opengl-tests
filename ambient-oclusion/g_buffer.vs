// vim: set ft=glsl:
#version 330 core

layout (location = 0) in vec3 in_coord;
layout (location = 1) in vec3 in_normal;
layout (location = 2) in vec2 in_tex_coord;
layout (location = 3) in vec3 in_tangent;
layout (location = 4) in vec3 in_bitangent;

out VS_OUT{
    vec3 coord;
    vec2 tex_coord;
    vec3 normal;
}vs_out;

uniform mat4 model;
uniform mat4 view;
uniform mat4 proj;

void main(){
    vs_out.coord = vec3(model * vec4(in_coord, 1.0));
    vs_out.tex_coord = in_tex_coord;
    vs_out.normal = transpose(inverse(mat3(model))) * in_normal;

    gl_Position = proj*view*model*vec4(in_coord, 1.0);
}
