// vim: set ft=glsl:
#version 330 core

layout (location = 0) out vec3 coord;
layout (location = 1) out vec3 color;
layout (location = 2) out vec3 normal;

in VS_OUT{
    vec3 coord;
    vec2 tex_coord;
    vec3 normal;
}vs_out;

void main(){
    coord = vs_out.coord;
    color = vec3(0.95);
    normal = normalize(vs_out.normal);
}
