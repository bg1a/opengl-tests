// vim: set ft=glsl:
#version 330 core

out vec4 FragColor;

in vec2 tex_coord;

uniform sampler2D color_tex;

void main(){
    FragColor = vec4(texture(color_tex, tex_coord).rgb, 1.0);
}
