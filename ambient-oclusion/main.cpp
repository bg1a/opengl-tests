#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include <GLES3/gl31.h>
#include <GL/gl.h>

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

#include "../model-loader/loader.h"
#include "kernel_vertices.h"
#include "noise_vertices.h"

const float camX = 5.0f;
const float camY = 2.0f;
const float camZ = 3.0f;
const int width = 800;
const int height = 600;
const float aspect = (float)width / (float)height;
GLfloat view_pos[] = {
    camX, camY, camZ
};
GLfloat light_pos[] = {
    2.0, 4.0, -2.0
};
GLfloat light_color[] = {
    0.2, 0.2, 0.7
};

GLfloat vertices_cube[] = {
    //Vertex             Normals         Texture
   -1.0, -1.0, -1.0,  0.0,  0.0,  1.0,  0.0, 0.0,                                                             
    1.0, -1.0, -1.0,  0.0,  0.0,  1.0,  1.0, 0.0,                                                             
    1.0,  1.0, -1.0,  0.0,  0.0,  1.0,  1.0, 1.0,                                                             
    1.0,  1.0, -1.0,  0.0,  0.0,  1.0,  1.0, 1.0,                                                             
   -1.0,  1.0, -1.0,  0.0,  0.0,  1.0,  0.0, 1.0,                                                             
   -1.0, -1.0, -1.0,  0.0,  0.0,  1.0,  0.0, 0.0,                                                             
                                      
   -1.0, -1.0,  1.0,  0.0,  0.0, -1.0,  0.0, 0.0,                                                            
    1.0, -1.0,  1.0,  0.0,  0.0, -1.0,  1.0, 0.0,                                                            
    1.0,  1.0,  1.0,  0.0,  0.0, -1.0,  1.0, 1.0,                                                            
    1.0,  1.0,  1.0,  0.0,  0.0, -1.0,  1.0, 1.0,                                                            
   -1.0,  1.0,  1.0,  0.0,  0.0, -1.0,  0.0, 1.0,                                                            
   -1.0, -1.0,  1.0,  0.0,  0.0, -1.0,  0.0, 0.0,                                                            

   -1.0,  1.0,  1.0,  1.0,  0.0,  0.0,  1.0, 1.0,                                                   
   -1.0,  1.0, -1.0,  1.0,  0.0,  0.0,  0.0, 1.0,                                                   
   -1.0, -1.0, -1.0,  1.0,  0.0,  0.0,  0.0, 0.0,                                                   
   -1.0, -1.0, -1.0,  1.0,  0.0,  0.0,  0.0, 0.0,                                                   
   -1.0, -1.0,  1.0,  1.0,  0.0,  0.0,  1.0, 0.0,                                                   
   -1.0,  1.0,  1.0,  1.0,  0.0,  0.0,  1.0, 1.0,                                                   
                                                 
    1.0,  1.0,  1.0, -1.0,  0.0,  0.0,  0.0, 0.0,                                                   
    1.0,  1.0, -1.0, -1.0,  0.0,  0.0,  1.0, 0.0,                                                   
    1.0, -1.0, -1.0, -1.0,  0.0,  0.0,  1.0, 1.0,                                                   
    1.0, -1.0, -1.0, -1.0,  0.0,  0.0,  1.0, 1.0,                                                   
    1.0, -1.0,  1.0, -1.0,  0.0,  0.0,  0.0, 1.0,                                                   
    1.0,  1.0,  1.0, -1.0,  0.0,  0.0,  0.0, 0.0,                                                   

   -1.0, -1.0, -1.0,  0.0,  1.0,  0.0,  0.0, 0.0,                                                  
    1.0, -1.0, -1.0,  0.0,  1.0,  0.0,  1.0, 0.0,                                                  
    1.0, -1.0,  1.0,  0.0,  1.0,  0.0,  1.0, 1.0,                                                  
    1.0, -1.0,  1.0,  0.0,  1.0,  0.0,  1.0, 1.0,                                                  
   -1.0, -1.0,  1.0,  0.0,  1.0,  0.0,  0.0, 1.0,                                                  
   -1.0, -1.0, -1.0,  0.0,  1.0,  0.0,  0.0, 0.0,                                                  
                                                 
    1.0,  1.0,  1.0,  0.0, -1.0,  0.0,  1.0, 1.0,                                                  
    1.0,  1.0, -1.0,  0.0, -1.0,  0.0,  1.0, 0.0,                                                  
   -1.0,  1.0, -1.0,  0.0, -1.0,  0.0,  0.0, 0.0,                                                  
    1.0,  1.0,  1.0,  0.0, -1.0,  0.0,  1.0, 1.0,                                                  
   -1.0,  1.0, -1.0,  0.0, -1.0,  0.0,  0.0, 0.0,                                                  
   -1.0,  1.0,  1.0,  0.0, -1.0,  0.0,  0.0, 1.0,                                                  
};

GLfloat vertices_plane[] = {
    -1.0,   1.0,  0.0,      0.0, 1.0,
    -1.0,  -1.0,  0.0,      0.0, 0.0,
     1.0,   1.0,  0.0,      1.0, 1.0,
     1.0,  -1.0,  0.0,      1.0, 0.0,
};

extern void create_window(int width, int height);
extern void window_loop(void);

char *load_file(const char *filename)
{
    FILE *f = fopen(filename, "r");
    char *text = NULL;

    if(f){
        size_t size = 0;

        fseek(f, 0, SEEK_END);
        size = ftell(f);
        rewind(f);

        text = (char*)calloc(size + 1, sizeof(char));
        fread(text, sizeof(char), size, f);
        fclose(f); 
    }

    return text;
}

void load_shader(const char *vertex_source, const char *fragment_source,
        GLuint *vertex, GLuint *fragment, GLuint *program)
{
    const char * test_str = "#version ";
    char infoLog[512] = {0};
    char *shader_source = NULL;

    *vertex = glCreateShader(GL_VERTEX_SHADER);
    if(!strcmp(vertex_source, test_str)){
        glShaderSource(*vertex, 1, &vertex_source, NULL);
        glCompileShader(*vertex);
        glGetShaderInfoLog(*vertex, sizeof(infoLog), NULL, infoLog);
        printf("Vertex shader compile log: %s\n", infoLog);
    }else{
        shader_source = load_file(vertex_source);
        glShaderSource(*vertex, 1, &shader_source, NULL);
        glCompileShader(*vertex);
        glGetShaderInfoLog(*vertex, sizeof(infoLog), NULL, infoLog);
        printf("%s, compile log: %s\n", vertex_source, infoLog);
        free(shader_source);
    }

    *fragment = glCreateShader(GL_FRAGMENT_SHADER);
    if(!strcmp(fragment_source, test_str)){
        glShaderSource(*fragment, 1, &fragment_source, NULL);
        glCompileShader(*fragment);
        glGetShaderInfoLog(*fragment, sizeof(infoLog), NULL, infoLog);
        printf("Fragment shader compile log: %s\n", infoLog);
    }else{
        shader_source = load_file(fragment_source);
        glShaderSource(*fragment, 1, &shader_source, NULL);
        glCompileShader(*fragment);
        glGetShaderInfoLog(*fragment, sizeof(infoLog), NULL, infoLog);
        printf("%s, compile log: %s\n", fragment_source, infoLog);
        free(shader_source);
    }

    *program = glCreateProgram();
    glAttachShader(*program, *vertex);
    glAttachShader(*program, *fragment);
    glLinkProgram(*program);
}

std::vector<GLuint> vao, buf;
std::vector<size_t> vertices_num;

GLuint g_fbo, g_rbo, g_color_tex, g_coord_tex, g_normal_tex;
GLuint g_buffer_vs, g_buffer_fs, g_buffer_ph;
GLuint plane_vao, plane_buf, plane_vs, plane_fs, plane_ph;
GLuint cube_vao, cube_buf, cube_vs, cube_fs, cube_ph;

GLuint noise_tex;
GLuint ssao_fbo, ssao_tex, ssao_vs, ssao_fs, ssao_ph;

void draw_opengl(char c)
{
    glClearColor(0.0, 0.0, 0.0, 1.0);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    // Draw geometry
    glBindFramebuffer(GL_FRAMEBUFFER, g_fbo);
    glUseProgram(g_buffer_ph);

    glm::mat4 model(1.0);
    model = glm::translate(model, glm::vec3(0.0f, 7.0f, 0.0f));
    model = glm::scale(model, glm::vec3(7.5f, 7.5f, 7.5f));
    glUniformMatrix4fv(glGetUniformLocation(g_buffer_ph, "model"), 1, GL_FALSE, glm::value_ptr(model));
    glBindVertexArray(cube_vao);
    glDrawArrays(GL_TRIANGLES, 0, 36);

    model = glm::mat4(1.0);
    model = glm::translate(model, glm::vec3(0.0f, 0.5f, 0.0f));
    model = glm::rotate(model, glm::radians(-90.0f), glm::vec3(1.0f, 0.0f, 0.0f));
    model = glm::scale(model, glm::vec3(1.0f));
    glUniformMatrix4fv(glGetUniformLocation(g_buffer_ph, "model"), 1, GL_FALSE, glm::value_ptr(model));

    for(int i = 0; i < vao.size(); ++i){
        glBindVertexArray(vao[i]);
        glDrawArrays(GL_TRIANGLES, 0, vertices_num[i]);
    }
    glBindFramebuffer(GL_FRAMEBUFFER, 0);

    // SSAO
    glBindFramebuffer(GL_FRAMEBUFFER, ssao_fbo);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glUseProgram(ssao_ph);

    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, g_color_tex);
    glUniform1i(glGetUniformLocation(ssao_ph, "color_tex"), 0);

    glActiveTexture(GL_TEXTURE1);
    glBindTexture(GL_TEXTURE_2D, g_coord_tex);
    glUniform1i(glGetUniformLocation(ssao_ph, "coord_tex"), 1);

    glActiveTexture(GL_TEXTURE2);
    glBindTexture(GL_TEXTURE_2D, g_normal_tex);
    glUniform1i(glGetUniformLocation(ssao_ph, "normal_tex"), 2);

    glActiveTexture(GL_TEXTURE3);
    glBindTexture(GL_TEXTURE_2D, noise_tex);
    glUniform1i(glGetUniformLocation(ssao_ph, "noise_tex"), 3);

    glBindVertexArray(plane_vao);
    glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);

    glBindFramebuffer(GL_FRAMEBUFFER, 0);

    // Render final image
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    glUseProgram(plane_ph);
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, ssao_tex);
    glUniform1i(glGetUniformLocation(plane_ph, "color_tex"), 0);

    glBindVertexArray(plane_vao);
    glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);

}

int main(int argc, char **argv)
{
    glm::mat4 view = glm::lookAt(glm::vec3(camX, camY, camZ), glm::vec3(0.0f, 0.0f, 0.0f), glm::vec3(0.0f, 1.0f, 0.0f));
    glm::mat4 proj = glm::perspective(glm::radians(45.0f), aspect, 0.1f, 100.0f);

    create_window(width, height);
    {
        std::vector<std::vector<float>> vao_vertices;
        std::vector<LoaderObject> objects;

        load_obj("../model-loader/backpack.obj", objects);
        for(const auto &obj : objects){
            vao_vertices.push_back(get_vao_data(obj));
        }

        vao.resize(vao_vertices.size());
        buf.resize(vao_vertices.size());
        glGenVertexArrays(vao.size(), vao.data());
        glGenBuffers(buf.size(), buf.data());
        for(int i = 0; i < vao.size(); ++i){
            glBindVertexArray(vao[i]);
            glBindBuffer(GL_ARRAY_BUFFER, buf[i]);
            glBufferData(GL_ARRAY_BUFFER, sizeof(vao_vertices[0]) * vao_vertices[i].size(),
                    vao_vertices[i].data(), GL_STATIC_DRAW);
            glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, VAO_LINE_SZ, (void*)VAO_VERTEX_OFFSET);
            glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, VAO_LINE_SZ, (void*)VAO_NORMAL_OFFSET);
            glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, VAO_LINE_SZ, (void*)VAO_TEXTURE_OFFSET);
            glVertexAttribPointer(3, 3, GL_FLOAT, GL_FALSE, VAO_LINE_SZ, (void*)VAO_TANGENT_OFFSET);
            glVertexAttribPointer(4, 3, GL_FLOAT, GL_FALSE, VAO_LINE_SZ, (void*)VAO_BITANGENT_OFFSET);
            glEnableVertexAttribArray(0);
            glEnableVertexAttribArray(1);
            glEnableVertexAttribArray(2);
            glEnableVertexAttribArray(3);
            glEnableVertexAttribArray(4);

            vertices_num.push_back(vao_vertices[i].size() / VAO_IDX_LAST);
        }

        objects.clear();
        vao_vertices.clear();
    }

    load_shader("g_buffer.vs", "g_buffer.fs", &g_buffer_vs, &g_buffer_fs, &g_buffer_ph);

    glUseProgram(g_buffer_ph);

    glUniformMatrix4fv(glGetUniformLocation(g_buffer_ph, "view"), 1, GL_FALSE, glm::value_ptr(view));
    glUniformMatrix4fv(glGetUniformLocation(g_buffer_ph, "proj"), 1, GL_FALSE, glm::value_ptr(proj));

    glGenFramebuffers(1, &g_fbo);
    glBindFramebuffer(GL_FRAMEBUFFER, g_fbo);

    glGenTextures(1, &g_color_tex);
    glBindTexture(GL_TEXTURE_2D, g_color_tex);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height,
                                0, GL_RGBA, GL_UNSIGNED_BYTE, NULL);
    glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT1, GL_TEXTURE_2D, g_color_tex, 0);

    glGenTextures(1, &g_coord_tex);
    glBindTexture(GL_TEXTURE_2D, g_coord_tex);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA16F, width, height,
                                0, GL_RGBA, GL_FLOAT, NULL);
    glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, g_coord_tex, 0);

    glGenTextures(1, &g_normal_tex);
    glBindTexture(GL_TEXTURE_2D, g_normal_tex);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA16F, width, height,
                                0, GL_RGBA, GL_FLOAT, NULL);
    glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT2, GL_TEXTURE_2D, g_normal_tex, 0);

    glGenRenderbuffers(1, &g_rbo);
    glBindRenderbuffer(GL_RENDERBUFFER, g_rbo);
    glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT, width, height);
    glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, g_rbo);

    GLuint attachments[] = {GL_COLOR_ATTACHMENT0, GL_COLOR_ATTACHMENT1, GL_COLOR_ATTACHMENT2};
    glDrawBuffers(3, attachments);

    glBindFramebuffer(GL_FRAMEBUFFER, 0);

    glGenVertexArrays(1, &plane_vao);
    glBindVertexArray(plane_vao);
    glGenBuffers(1, &plane_buf);
    glBindBuffer(GL_ARRAY_BUFFER, plane_buf);
    glBufferData(GL_ARRAY_BUFFER, sizeof(vertices_plane), vertices_plane, GL_STATIC_DRAW);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 5*sizeof(GLfloat), 0);
    glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 5*sizeof(GLfloat), (void*)(3*sizeof(GLfloat)));
    glEnableVertexAttribArray(0);
    glEnableVertexAttribArray(1);

    load_shader("plane.vs", "plane.fs", &plane_vs, &plane_fs, &plane_ph);

    glGenVertexArrays(1, &cube_vao);
    glBindVertexArray(cube_vao);
    glGenBuffers(1, &cube_buf);
    glBindBuffer(GL_ARRAY_BUFFER, cube_buf);
    glBufferData(GL_ARRAY_BUFFER, sizeof(vertices_cube), vertices_cube, GL_STATIC_DRAW);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 8*sizeof(GLfloat), 0);
    glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 8*sizeof(GLfloat), (void*)(3*sizeof(GLfloat)));
    glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, 8*sizeof(GLfloat), (void*)(6*sizeof(GLfloat)));
    glEnableVertexAttribArray(0);
    glEnableVertexAttribArray(1);
    glEnableVertexAttribArray(2);

    glGenTextures(1, &noise_tex);
    glBindTexture(GL_TEXTURE_2D, noise_tex);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA32F, 4, 4,
                                0, GL_RGB, GL_FLOAT, noise_vertices);

    glGenFramebuffers(1, &ssao_fbo);
    glBindFramebuffer(GL_FRAMEBUFFER, ssao_fbo);
    glGenTextures(1, &ssao_tex);
    glBindTexture(GL_TEXTURE_2D, ssao_tex);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA16F, width, height,
                                0, GL_RGBA, GL_FLOAT, NULL);
    glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, ssao_tex, 0);
    glBindFramebuffer(GL_FRAMEBUFFER, 0);

    load_shader("plane.vs", "ssao.fs", &ssao_vs, &ssao_fs, &ssao_ph);
    glUseProgram(ssao_ph);

    for(int i = 0; i < 64; ++i){
        char kernel_cmp[30];

        snprintf(kernel_cmp, sizeof(kernel_cmp), "kernel[%d]", i);
        glUniform3fv(glGetUniformLocation(ssao_ph, kernel_cmp), 1, kernel_vertices[i]);
    }
    glUniformMatrix4fv(glGetUniformLocation(ssao_ph, "view"), 1, GL_FALSE, glm::value_ptr(view));
    glUniformMatrix4fv(glGetUniformLocation(ssao_ph, "proj"), 1, GL_FALSE, glm::value_ptr(proj));

    glEnable(GL_DEPTH_TEST);

    window_loop();

    return 0;
}
