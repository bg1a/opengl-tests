#include <stdlib.h>
#include <GLES3/gl31.h>
#include <GL/gl.h>

#include <iostream>

const int w = 800;
const int h = 600;

const char *vs = 
"#version 100 \n"
"attribute vec2 pos;"
"attribute vec3 color;"
"varying vec3 fcolor;"
"uniform float scale_f;"
"const float angle = 45.0;"
"void main(){"
" const float angle_r = angle * (3.14 / 180.0);"
" mat4 r = mat4(1.0);"
" r[0] = vec4(cos(angle_r), -sin(angle_r), 0.0, 0.0);"
" r[1] = vec4(sin(angle_r), cos(angle_r), 0.0, 0.0);"
" mat4 s = mat4(1.0);"
" s[0][0] = scale_f;"
" s[1][1] = scale_f;"
" mat4 t = mat4(1.0);"
" t[3] = vec4(0.5, 0.5, 0.0, 1.0);"
" gl_Position = s * r * t * vec4(pos, 0.0, 1.0);"
" fcolor = color;"
"}";

const char *fs = 
"#version 100 \n"
"precision mediump float;"
"varying vec3 fcolor;"
"uniform float color_f;"
"void main(){"
" gl_FragColor = vec4(color_f * fcolor, 1.0);"
"}";

GLfloat vxs[] = {
    //Coord.    Color
    -1.0, -1.0, 1.0, 0.0, 0.0,
     1.0, -1.0, 0.0, 1.0, 0.0,
     0.0,  1.0, 0.0, 0.0, 1.0,
};

GLfloat vertices[] = {
   -1.0, -1.0,
    1.0, -1.0,
    0.0,  1.0,
};
GLfloat colors[] = {
1.0, 0.0, 0.0,
0.0, 1.0, 0.0,
0.0, 0.0, 1.0,
};


extern void create_window(int width, int height);
extern void window_loop(void);

GLuint vao, buf, vsh, fsh, ph;
GLuint us, cs, zs;

void draw_opengl(void)
{
    glClear(GL_DEPTH_BUFFER_BIT | GL_COLOR_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);
    glClearColor(0.1, 0.1, 0.31, 0.2);

    us = glGetUniformLocation(ph, "scale_f");
    glUniform1f(us, 0.5);

    cs = glGetUniformLocation(ph, "color_f");
    glUniform1f(cs, 0.3);

    glStencilFunc(GL_ALWAYS, 1, 0xff);
    glStencilOp(GL_KEEP, GL_KEEP, GL_REPLACE);
    glStencilMask(0xff);

    glDrawArrays(GL_TRIANGLES, 0, 3);

    glDisable(GL_DEPTH_TEST);

    us = glGetUniformLocation(ph, "scale_f");
    glUniform1f(us, 0.49);

    cs = glGetUniformLocation(ph, "color_f");
    glUniform1f(cs, 1.0);

    glStencilFunc(GL_EQUAL, 1, 0xff);
    glStencilMask(0x00);


    //glEnable(GL_POLYGON_OFFSET_LINE);
    //glPolygonOffset(-10.0, -10.0);

    //glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
    //glDrawArrays(GL_TRIANGLES, 0, 3);
    //glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

    //glDisable(GL_POLYGON_OFFSET_LINE);

    glDrawArrays(GL_TRIANGLES, 0, 3);

}

int main(int argc, char **argv)
{
    create_window(w, h);
    glGenVertexArrays(1, &vao);
    glBindVertexArray(vao);

    glGenBuffers(1, &buf);
    glBindBuffer(GL_ARRAY_BUFFER, buf);
    //glBufferData(GL_ARRAY_BUFFER, sizeof(vxs), vxs, GL_STATIC_DRAW);
    glBufferData(GL_ARRAY_BUFFER, sizeof(vxs), 0, GL_STATIC_DRAW);
    glBufferSubData(GL_ARRAY_BUFFER, 0, sizeof(vertices), vertices);
    glBufferSubData(GL_ARRAY_BUFFER, sizeof(vertices), sizeof(colors), colors);

    //glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 5*sizeof(float), 0);
    glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 2*sizeof(float), 0);
    glEnableVertexAttribArray(0);
    //glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 5*sizeof(float), (void*)(2*sizeof(float)));
    glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 3*sizeof(float), (void*)(6*sizeof(float)));
    glEnableVertexAttribArray(1);

    vsh = glCreateShader(GL_VERTEX_SHADER);
    glShaderSource(vsh, 1, &vs, NULL);
    glCompileShader(vsh);
    fsh = glCreateShader(GL_FRAGMENT_SHADER);
    glShaderSource(fsh, 1, &fs, NULL);
    glCompileShader(fsh);

    ph = glCreateProgram();
    glAttachShader(ph, vsh);
    glAttachShader(ph, fsh);
    glLinkProgram(ph);
    glUseProgram(ph);

    us = glGetUniformLocation(ph, "scale_f");
    glUniform1f(us, 0.5);

    cs = glGetUniformLocation(ph, "color_f");
    glUniform1f(cs, 1.0);

    glEnable(GL_STENCIL_TEST);
    glEnable(GL_DEPTH_TEST);

    glEnable(GL_SCISSOR_TEST);
    glScissor(0, 0, 800, 600);

    //glLineWidth(20.0);
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    glEnable(GL_LINE_SMOOTH);
    glEnable(GL_POLYGON_SMOOTH);
    glHint(GL_LINE_SMOOTH_HINT, GL_NICEST);
    glHint(GL_POLYGON_SMOOTH_HINT, GL_NICEST);

    window_loop();

    return 0;
}
