#ifndef __OPENGL_TUTORIAL_LOADER__
#define __OPENGL_TUTORIAL_LOADER__

#include <vector>
#include <memory>
#include <string>

#define VAO_VERTEX_IDX 0
#define VAO_VERTEX_NUM 3
#define VAO_VERTEX_OFFSET 0
#define VAO_VERTEX_SZ VAO_VERTEX_NUM*sizeof(float)

#define VAO_NORMAL_IDX (VAO_VERTEX_IDX + VAO_VERTEX_NUM)
#define VAO_NORMAL_NUM 3 
#define VAO_NORMAL_OFFSET (VAO_VERTEX_SZ + VAO_VERTEX_OFFSET)
#define VAO_NORMAL_SZ VAO_NORMAL_NUM*sizeof(float)

#define VAO_TEXTURE_IDX (VAO_NORMAL_IDX + VAO_NORMAL_NUM)
#define VAO_TEXTURE_NUM 2
#define VAO_TEXTURE_OFFSET (VAO_NORMAL_SZ + VAO_NORMAL_OFFSET)
#define VAO_TEXTURE_SZ VAO_TEXTURE_NUM*sizeof(float)

#define VAO_TANGENT_IDX (VAO_TEXTURE_IDX + VAO_TEXTURE_NUM)
#define VAO_TANGENT_NUM 3
#define VAO_TANGENT_OFFSET (VAO_TEXTURE_OFFSET + VAO_TEXTURE_SZ)
#define VAO_TANGET_SZ VAO_TANGENT_NUM*sizeof(float)

#define VAO_BITANGENT_IDX (VAO_TANGENT_IDX + VAO_TANGENT_NUM)
#define VAO_BITANGENT_NUM 3
#define VAO_BITANGENT_OFFSET (VAO_TANGENT_OFFSET + VAO_TANGET_SZ)
#define VAO_BITANGENT_SZ VAO_BITANGENT_NUM*sizeof(float)

#define VAO_IDX_LAST (VAO_BITANGENT_IDX + VAO_BITANGENT_NUM)
#define VAO_LINE_SZ (VAO_BITANGENT_OFFSET + VAO_BITANGENT_SZ)

struct LoaderFaceElement{
    int v;
    int vt;
    int vn;
};

struct LoaderFace{
    struct LoaderFaceElement f[3];

    LoaderFace(void){
        for(LoaderFaceElement &fe : f){
            fe.v = -1;
            fe.vt = -1;
            fe.vn = -1;
        }
    }
};

struct LoaderCoord{
    union{
        float x;
        float u;
        float s;
    };
    union{
        float y;
        float v;
        float t;
    };
    union{
        float z;
        float p;
    };
};

struct LoaderObject{
    std::vector<LoaderCoord> vertices;
    std::vector<LoaderCoord> normals; 
    std::vector<LoaderCoord> tex_coords;
    std::vector<LoaderFace> faces;
    std::string name;

    LoaderObject(void) : LoaderObject("unknown"){}
    LoaderObject(const std::string &object_name){
        name = object_name;
    }
};

enum LineType{
    LINE_TYPE_UNKNOWN = 0,
    LINE_TYPE_VERTEX,
    LINE_TYPE_NORMAL,
    LINE_TYPE_TEXTURE,
    LINE_TYPE_OBJECT,
    LINE_TYPE_FACE_V,
    LINE_TYPE_FACE_V_VT,
    LINE_TYPE_FACE_V_VT_VN,
    LINE_TYPE_FACE_V_VN,
    LINE_TYPE_LAST,
};

LineType get_line_type(const char *line);
LoaderCoord get_line_coord(const char *line, LineType type);
LoaderFace get_line_face(const char *line, LineType type);
std::string get_line_object_name(const char *line);

bool load_obj(const char *filename, std::vector<LoaderObject> &objects);
std::vector<float> get_vao_data(const LoaderObject &obj);

#endif // __OPENGL_TUTORIAL_LOADER__
