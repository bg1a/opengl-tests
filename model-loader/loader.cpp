#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "loader.h"

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/epsilon.hpp>

LineType get_line_type(const char *line)
{
    LineType type = LINE_TYPE_UNKNOWN;

    if(line){
        char test_char = '\0';
        float test_float;
        float test_float1;
        float test_float2;

        if(1 == sscanf(line, " v %f", &test_float)){
            //printf("Detected Vertex descriptor (%f)\n", test_float);
            type = LINE_TYPE_VERTEX;
        }else if(1 == sscanf(line, " vt %f", &test_float)){
            //printf("Detected Texture Vertex Descriptor (%f)\n", test_float);
            type = LINE_TYPE_TEXTURE;
        }else if(1 == sscanf(line, " vn %f", &test_float)){
            //printf("Detected Normal Vertex Descriptor (%f)\n", test_float);
            type = LINE_TYPE_NORMAL;
        }else if(1 == sscanf(line, " o %c ", &test_char)){
            //printf("Detected Object Descriptor (%c)\n", test_char);
            type = LINE_TYPE_OBJECT;
        }else if(3 == sscanf(line, " f %f/%f/%f ", &test_float, &test_float1, &test_float2)){
            //printf("Detected Face Descriptor, V-VT-VN (%f/%f/%f)\n",
                    //test_float, test_float1, test_float2);
            type = LINE_TYPE_FACE_V_VT_VN;
        }else if(2 == sscanf(line, " f %f//%f ", &test_float, &test_float1)){
            //printf("Detected Face Descriptor, V-VN (%f//%f)\n",
                    //test_float, test_float1);
            type = LINE_TYPE_FACE_V_VN;
        }else if(2 == sscanf(line, " f %f/%f ", &test_float, &test_float1)){
            //printf("Detected Face Descriptor, V-VT (%f/%f)\n",
                    //test_float, test_float1);
            type = LINE_TYPE_FACE_V_VT;
        }else if(1 == sscanf(line, " f %f", &test_float)){
            //printf("Detected Face Descriptor, V (%f)\n", test_float);
            type = LINE_TYPE_FACE_V;
        }
    }

    return type;
}

LoaderCoord get_line_coord(const char *line, LineType type)
{
    LoaderCoord coord = {0};

    switch(type){
        case LINE_TYPE_VERTEX: {
            sscanf(line, " v %f %f %f", &coord.x, &coord.y, &coord.z);                      
        } break;
        case LINE_TYPE_TEXTURE: {
            if(3 != sscanf(line, " vt %f %f %f", &coord.s, &coord.t, &coord.p)){
                sscanf(line, " vt %f %f", &coord.u, &coord.v);
            }
        } break;
        case LINE_TYPE_NORMAL: {
            sscanf(line, " vn %f %f %f", &coord.x, &coord.y, &coord.z);                      
        } break;
        default: {
            printf("Bad line type: %d!\n", type);
        } break;
    }

    return coord;
}

LoaderFace get_line_face(const char *line, LineType type)
{
    LoaderFace face;

    switch(type){
        case LINE_TYPE_FACE_V: {
            sscanf(line, " f %d %d %d", &face.f[0].v, &face.f[1].v, &face.f[2].v);                            
        } break;
        case LINE_TYPE_FACE_V_VT: {
            sscanf(line, " f %d/%d %d/%d %d/%d", 
                    &face.f[0].v, &face.f[0].vt,
                    &face.f[1].v, &face.f[1].vt,
                    &face.f[2].v, &face.f[2].vt);                          
        } break;
        case LINE_TYPE_FACE_V_VT_VN: {
            sscanf(line, " f %d/%d/%d %d/%d/%d %d/%d/%d", 
                   &face.f[0].v, &face.f[0].vt, &face.f[0].vn,  
                   &face.f[1].v, &face.f[1].vt, &face.f[1].vn,  
                   &face.f[2].v, &face.f[2].vt, &face.f[2].vn);
        } break;
        case LINE_TYPE_FACE_V_VN: {
            sscanf(line, " f %d//%d %d//%d %d//%d",
                   &face.f[0].v, &face.f[0].vn, 
                   &face.f[1].v, &face.f[1].vn, 
                   &face.f[2].v, &face.f[2].vn);
        } break;
    }

    // Make sure that indices start with 0
    for(auto &f : face.f){
        if(f.v > 0) f.v--;
        if(f.vt > 0) f.vt--;
        if(f.vn > 0) f.vn--;
    }

    return face;
}

std::string get_line_object_name(const char *line)
{
    char *object_name = NULL;
    std::string name;

    sscanf(line, " o %ms", &object_name);
    name = std::move(std::string(object_name));
    free(object_name);

    return name;
}

bool load_obj(const char *filename, std::vector<LoaderObject> &objects)
{
    FILE *fd = fopen(filename, "r");
    bool ret = false;

    if(fd){
        std::unique_ptr<LoaderObject> parsed_object;
        char *line = (char*)malloc(50);
        int vertex_count = 0;
        int normal_count = 0;
        int texture_count = 0;
        int vertex_offset = 0;
        int normal_offset = 0;
        int texture_offset = 0;

        for(size_t n = 0; getline(&line, &n, fd) > 0; n = 0){
            LineType type = get_line_type(line);

            if(!parsed_object && 
            (LINE_TYPE_UNKNOWN != type) && (LINE_TYPE_OBJECT != type)){
                parsed_object = std::move(std::make_unique<LoaderObject>());
                vertex_offset = vertex_count;
                normal_offset = normal_count;
                texture_offset = texture_count;
            }

            switch(type){
                case LINE_TYPE_VERTEX: {
                    LoaderCoord coord = get_line_coord(line, type);
                    parsed_object->vertices.push_back(coord);
                    vertex_count++;
                } break;
                case LINE_TYPE_NORMAL: {
                    LoaderCoord coord = get_line_coord(line, type);
                    parsed_object->normals.push_back(coord);
                    normal_count++;
                } break;
                case LINE_TYPE_TEXTURE: {
                    LoaderCoord coord = get_line_coord(line, type);
                    parsed_object->tex_coords.push_back(coord);
                    texture_count++;
                } break;
                case LINE_TYPE_OBJECT: {
                    if(parsed_object){
                        for(auto &face : parsed_object->faces){
                            for(auto &f : face.f){
                                if(f.v > 0) f.v -= vertex_offset;
                                if(f.vn > 0) f.vn -= normal_offset;
                                if(f.vt > 0) f.vt -= texture_offset;
                            }
                        }
                        objects.push_back(*parsed_object.release());
                    }
                    parsed_object = std::move(std::make_unique<LoaderObject>(get_line_object_name(line)));
                    vertex_offset = vertex_count;
                    normal_offset = normal_count;
                    texture_offset = texture_count;
                } break;
                case LINE_TYPE_FACE_V ... LINE_TYPE_FACE_V_VN: {
                    LoaderFace face = get_line_face(line, type);
                    parsed_object->faces.push_back(face);
                } break;
            }
        }

        free(line);

        if(parsed_object){
            for(auto &face : parsed_object->faces){
                for(auto &f : face.f){
                    if(f.v > 0) f.v -= vertex_offset;
                    if(f.vn > 0) f.vn -= normal_offset;
                    if(f.vt > 0) f.vt -= texture_offset;
                }
            }
            objects.push_back(*parsed_object);
        }
    
        fclose(fd);
    }

    return ret;
}

void getTB(const LoaderCoord &p2, const LoaderCoord &p1, const LoaderCoord &p0,
           const LoaderCoord &t2, const LoaderCoord &t1, const LoaderCoord &t0,
           LoaderCoord &T, LoaderCoord &B) 
{
    const glm::vec3 p20(p2.x - p0.x, p2.y - p0.y, p2.z - p0.z);
    const glm::vec3 p10(p1.x - p0.x, p1.y - p0.y, p1.z - p0.z);
    const glm::vec2 dUV20(t2.u - t0.u, t2.v - t0.v);
    const glm::vec2 dUV10(t1.u - t0.u, t1.v - t0.v); 

    const glm::mat2 dUV(dUV10[0], dUV20[0],
                        dUV10[1], dUV20[1]);
    float det_dUV = glm::determinant(dUV);
    const glm::mat2 adj_dUV(dUV20[1], -dUV20[0],
                           -dUV10[1],  dUV10[0]);
    const glm::mat3x2 P(p10[0], p20[0],
                        p10[1], p20[1],
                        p10[2], p20[2]);
    
    if(glm::epsilonEqual(det_dUV, 0.0f, std::numeric_limits<float>::epsilon())){
        det_dUV += 1.0e-12f;
    }

    glm::mat3x2 TB = (1.0f / det_dUV) * adj_dUV * P;

    if(det_dUV > 0.0f){
        T.x = TB[0][0]; T.y = TB[1][0]; T.z = TB[2][0];
        B.x = TB[0][1]; B.y = TB[1][1]; B.z = TB[2][1];
    }else{
        T.x = -1.0f*TB[0][0]; T.y = -1.0f*TB[1][0]; T.z = -1.0f*TB[2][0];
        B.x = -1.0f*TB[0][1]; B.y = -1.0f*TB[1][1]; B.z = -1.0f*TB[2][1];
    }
#if 0
    printf("p0: %f %f %f\n", p0.x, p0.y, p0.z);
    printf("p1: %f %f %f\n", p1.x, p1.y, p1.z);
    printf("p2: %f %f %f\n", p2.x, p2.y, p2.z);
    printf("t2: %f %f\n", t2.u, t2.v);
    printf("t1: %f %f\n", t1.u, t1.v);
    printf("t0: %f %f\n", t0.u, t0.v);
    printf("Matrix dUV:\n%f %f\n%f %f\n",
            dUV[0][0], dUV[1][0], dUV[0][1], dUV[1][1]);
    printf("Matrix det.: %f\n", det_dUV);
    auto invUV = glm::inverse(dUV);
    printf("Inverse Matrix dUV:\n%f %f\n%f %f\n",
            invUV[0][0], invUV[1][0], invUV[0][1], invUV[1][1]);
    printf("Matrix P:\n%f %f %f\n%f %f %f\n",
            P[0][0], P[1][0], P[2][0], P[0][1], P[1][1], P[2][1]);
    printf("T: %f %f %f\n", T.x, T.y, T.z);
    printf("B: %f %f %f\n", B.x, B.y, B.z);

    glm::vec3 N = glm::normalize(glm::cross(glm::vec3(T.x, T.y, T.z),
                                            glm::vec3(B.x, B.y, B.z)));
    printf("N: %f %f %f\n", N.x, N.y, N.z);
    glm::vec3 act_normal = glm::normalize(glm::cross(p10, p20));
    printf("actual normal: %f %f %f\n", act_normal.x, act_normal.y, act_normal.z);
#endif
}

std::vector<float> get_vao_data(const LoaderObject &obj){
    const size_t num = obj.faces.size();
    std::vector<float> vao_data(3*num*VAO_IDX_LAST, 0);
    auto p = vao_data.data();

    if(num){
        for(const LoaderFace &face : obj.faces){
            const LoaderCoord &p0 = obj.vertices[face.f[0].v];
            const LoaderCoord &p1 = obj.vertices[face.f[1].v];
            const LoaderCoord &p2 = obj.vertices[face.f[2].v];
            const LoaderCoord &t0 = obj.tex_coords[face.f[0].vt];
            const LoaderCoord &t1 = obj.tex_coords[face.f[1].vt];
            const LoaderCoord &t2 = obj.tex_coords[face.f[2].vt];
            LoaderCoord T = {0.0f, 0.0f, 0.0f};
            LoaderCoord B = {0.0f, 0.0f, 0.0f};

            getTB(p2, p1, p0, t2, t1, t0, T, B);

            for(const LoaderFaceElement &fe : face.f){
                const LoaderCoord &vertex = obj.vertices[fe.v];
                const LoaderCoord &normal = obj.normals[fe.vn];
                const LoaderCoord &texture = obj.tex_coords[fe.vt];

                p[VAO_VERTEX_IDX]     = vertex.x;
                p[VAO_VERTEX_IDX + 1] = vertex.y;
                p[VAO_VERTEX_IDX + 2] = vertex.z;
                p[VAO_NORMAL_IDX]     = normal.x;
                p[VAO_NORMAL_IDX + 1] = normal.y;
                p[VAO_NORMAL_IDX + 2] = normal.z;
                p[VAO_TEXTURE_IDX]     = texture.u;
                p[VAO_TEXTURE_IDX + 1] = texture.v;
                p[VAO_TANGENT_IDX]       = T.x;
                p[VAO_TANGENT_IDX + 1]   = T.y;
                p[VAO_TANGENT_IDX + 2]   = T.z;
                p[VAO_BITANGENT_IDX]     = B.x;
                p[VAO_BITANGENT_IDX + 1] = B.y;
                p[VAO_BITANGENT_IDX + 2] = B.z;

                p += VAO_IDX_LAST;
            }
        }
    }

    return vao_data;
}

#ifdef LOADER_STANDALONE
int main(int argc, char **argv)
{
    std::vector<LoaderObject> objects;

    if(argc >= 2){
        load_obj(argv[1], objects);
        for(LoaderObject &obj : objects){
            printf("==============================================\n");
            printf("Object name: %s\n", obj.name.c_str());
            printf("==============================================\n");
            printf("Vertices:\n");
            for(LoaderCoord &coord : obj.vertices){
                printf("(%f, %f, %f)\n", coord.x, coord.y, coord.z);
            }
            printf("Normals:\n");
            for(LoaderCoord &coord : obj.normals){
                printf("(%f, %f, %f)\n", coord.x, coord.y, coord.z);
            }
            printf("Textures:\n");
            for(LoaderCoord &coord : obj.tex_coords){
                printf("(%f, %f)\n", coord.u, coord.v);
            }
            printf("Faces:\n");
            for(LoaderFace &face : obj.faces){
                printf("%d/%d/%d %d/%d/%d %d/%d/%d\n",
                        face.f[0].v, face.f[0].vt, face.f[0].vn,
                        face.f[1].v, face.f[1].vt, face.f[1].vn,
                        face.f[2].v, face.f[2].vt, face.f[2].vn);
            }
            get_vao_data(obj);
        }
    }

    return 0;
}
#endif
