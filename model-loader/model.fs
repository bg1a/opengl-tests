// vim: set ft=glsl:
#version 330 core

in VS_OUT{
    vec3 coord;
    vec2 tex_coord;
    vec3 light_pos;
    vec3 view_pos;
}vs_out;

out vec4 FragColor;

uniform sampler2D tex;
uniform sampler2D tex_normal;

void main(){
    vec3 color = texture(tex, vs_out.tex_coord).rgb;
    vec3 N = texture(tex_normal, vs_out.tex_coord).rgb;
    float D = length(vs_out.coord - vs_out.light_pos);
    vec3 I = normalize(vs_out.coord - vs_out.light_pos);
    vec3 V = normalize(vs_out.view_pos - vs_out.coord);

    float Kamb = 0.1;
    vec3 Iamb = Kamb * color;

    float Kdiff = 0.7;
    vec3 Idiff = Kdiff * max(dot(-I, N), 0.0) * color;

    FragColor = vec4(Iamb + Idiff, 1.0);
}

