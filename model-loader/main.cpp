#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include <sys/time.h>

#include <GLES3/gl31.h>
#include <GL/gl.h>

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

#include "loader.h"

const float PI = 3.14159265359f;
const float omega = 2.0f * PI / 16.0f;
const float camX = 0.0f;
const float camY = 0.0f;
const float camZ = 5.0f;
const int width = 800;
const int height = 600;
const float aspect = (float)width / (float)height;
GLfloat view_pos[] = {
    camX, camY, camZ
};
GLfloat light_distance = 3.0;
GLfloat light_pos[] = {
    light_distance, 0.0, light_distance,
};

extern void create_window(int width, int height);
extern void window_loop(void);

char *load_file(const char *filename)
{
    FILE *f = fopen(filename, "r");
    char *text = NULL;

    if(f){
        size_t size = 0;

        fseek(f, 0, SEEK_END);
        size = ftell(f);
        rewind(f);

        text = (char*)calloc(size + 1, sizeof(char));
        fread(text, sizeof(char), size, f);
        fclose(f); 
    }

    return text;
}

void load_shader(const char *vertex_source, const char *fragment_source,
        GLuint *vertex, GLuint *fragment, GLuint *program)
{
    const char * test_str = "#version ";
    char infoLog[512] = {0};
    char *shader_source = NULL;

    *vertex = glCreateShader(GL_VERTEX_SHADER);
    if(!strcmp(vertex_source, test_str)){
        glShaderSource(*vertex, 1, &vertex_source, NULL);
        glCompileShader(*vertex);
        glGetShaderInfoLog(*vertex, sizeof(infoLog), NULL, infoLog);
        printf("Vertex shader compile log: %s\n", infoLog);
    }else{
        shader_source = load_file(vertex_source);
        glShaderSource(*vertex, 1, &shader_source, NULL);
        glCompileShader(*vertex);
        glGetShaderInfoLog(*vertex, sizeof(infoLog), NULL, infoLog);
        printf("%s, compile log: %s\n", vertex_source, infoLog);
        free(shader_source);
    }

    *fragment = glCreateShader(GL_FRAGMENT_SHADER);
    if(!strcmp(fragment_source, test_str)){
        glShaderSource(*fragment, 1, &fragment_source, NULL);
        glCompileShader(*fragment);
        glGetShaderInfoLog(*fragment, sizeof(infoLog), NULL, infoLog);
        printf("Fragment shader compile log: %s\n", infoLog);
    }else{
        shader_source = load_file(fragment_source);
        glShaderSource(*fragment, 1, &shader_source, NULL);
        glCompileShader(*fragment);
        glGetShaderInfoLog(*fragment, sizeof(infoLog), NULL, infoLog);
        printf("%s, compile log: %s\n", fragment_source, infoLog);
        free(shader_source);
    }

    *program = glCreateProgram();
    glAttachShader(*program, *vertex);
    glAttachShader(*program, *fragment);
    glLinkProgram(*program);
}
std::vector<GLuint> vao, buf;
std::vector<size_t> vertices_num;
GLuint vs, fs, ph;
GLuint tex, tex_normal;

static struct timeval init_tv;

static double tv2d(struct timeval *tv)
{
    return (double)tv->tv_sec + ((double) tv->tv_usec) / 1000000.0;
}

static float get_time(void){
    struct timeval tv = {0};

    gettimeofday(&tv, NULL);

    return tv2d(&tv) - tv2d(&init_tv);
}


void draw_opengl(char c){
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    float t = get_time();
    light_pos[0] = light_distance * cos(omega*t);
    light_pos[2] = light_distance * sin(omega*t);
    glUniform3fv(glGetUniformLocation(ph, "in_light_pos"), 1, light_pos);
    for(int i = 0; i < vao.size(); ++i){
        glBindVertexArray(vao[i]);
        glDrawArrays(GL_TRIANGLES, 0, vertices_num[i]);
    }
}


int main(int argc, char **argv)
{
    glm::mat4 model(1.0);
    glm::mat4 view = glm::lookAt(glm::vec3(camX, camY, camZ),
                                 glm::vec3(0.0f, 0.0f, 0.0f),
                                 glm::vec3(0.0f, 1.0f, 0.0f));
    glm::mat4 proj = glm::perspective(glm::radians(45.0f), aspect, 0.1f, 100.0f);
    char *img = load_file("diffuse.rgb");
    char *img_normal = load_file("normal.rgb");

    gettimeofday(&init_tv, NULL);
    create_window(width, height);

    {
        std::vector<std::vector<float>> vao_vertices;
        std::vector<LoaderObject> objects;

        load_obj("backpack.obj", objects);
        for(const auto &obj : objects){
            vao_vertices.push_back(get_vao_data(obj));
        }

        vao.resize(vao_vertices.size());
        buf.resize(vao_vertices.size());
        glGenVertexArrays(vao.size(), vao.data());
        glGenBuffers(buf.size(), buf.data());
        for(int i = 0; i < vao.size(); ++i){
            glBindVertexArray(vao[i]);
            glBindBuffer(GL_ARRAY_BUFFER, buf[i]);
            glBufferData(GL_ARRAY_BUFFER, sizeof(vao_vertices[0]) * vao_vertices[i].size(),
                    vao_vertices[i].data(), GL_STATIC_DRAW);
            glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, VAO_LINE_SZ, (void*)VAO_VERTEX_OFFSET);
            glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, VAO_LINE_SZ, (void*)VAO_NORMAL_OFFSET);
            glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, VAO_LINE_SZ, (void*)VAO_TEXTURE_OFFSET);
            glVertexAttribPointer(3, 3, GL_FLOAT, GL_FALSE, VAO_LINE_SZ, (void*)VAO_TANGENT_OFFSET);
            glVertexAttribPointer(4, 3, GL_FLOAT, GL_FALSE, VAO_LINE_SZ, (void*)VAO_BITANGENT_OFFSET);
            glEnableVertexAttribArray(0);
            glEnableVertexAttribArray(1);
            glEnableVertexAttribArray(2);
            glEnableVertexAttribArray(3);
            glEnableVertexAttribArray(4);

            vertices_num.push_back(vao_vertices[i].size() / VAO_IDX_LAST);
        }

        objects.clear();
        vao_vertices.clear();
    }

    glGenTextures(1, &tex);
    glBindTexture(GL_TEXTURE_2D, tex);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, 4096, 4096,
                                0, GL_RGB, GL_UNSIGNED_BYTE, img);

    glGenTextures(1, &tex_normal);
    glBindTexture(GL_TEXTURE_2D, tex_normal);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, 4096, 4096,
                                0, GL_RGB, GL_UNSIGNED_BYTE, img_normal);

    load_shader("model.vs", "model.fs", &vs, &fs, &ph);
    glUseProgram(ph);

    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, tex);
    glUniform1i(glGetUniformLocation(ph, "tex"), 0);

    glActiveTexture(GL_TEXTURE1);
    glBindTexture(GL_TEXTURE_2D, tex_normal);
    glUniform1i(glGetUniformLocation(ph, "tex_normal"), 1);

    glUniformMatrix4fv(glGetUniformLocation(ph, "model"), 1, GL_FALSE, glm::value_ptr(model));
    glUniformMatrix4fv(glGetUniformLocation(ph, "view"), 1, GL_FALSE, glm::value_ptr(view));
    glUniformMatrix4fv(glGetUniformLocation(ph, "proj"), 1, GL_FALSE, glm::value_ptr(proj));
    glUniform3fv(glGetUniformLocation(ph, "in_view_pos"), 1, view_pos);

    glEnable(GL_DEPTH_TEST);
    glEnable(GL_CULL_FACE);
    glCullFace(GL_BACK);

    free(img);
    free(img_normal);

    window_loop();

    return 0;
}


