// vim: set ft=glsl:
#version 330 core

layout (location = 0) in vec3 in_coord;
layout (location = 1) in vec3 in_normal;
layout (location = 2) in vec2 in_tex_coord;
layout (location = 3) in vec3 in_tangent;
layout (location = 4) in vec3 in_bitangent;

out VS_OUT{
    vec3 coord;
    vec2 tex_coord;
    vec3 light_pos;
    vec3 view_pos;
}vs_out;

uniform mat4 model;
uniform mat4 view;
uniform mat4 proj;

uniform vec3 in_light_pos;
uniform vec3 in_view_pos;

void main(){
    vec4 coord = model*vec4(in_coord, 1.0);

    vec3 normal = normalize(in_normal);
    vec3 tangent = normalize(in_tangent - dot(in_tangent, normal)*normal);

    mat3 m = mat3(model);
    vec3 m_normal = inverse(transpose(m)) * normal;
    vec3 m_tangent = m * tangent;
    vec3 m_bitangent = normalize(cross(m_normal, m_tangent));

    mat3 TBN = transpose(mat3(m_tangent, m_bitangent, m_normal));

    vs_out.coord = TBN*vec3(coord);
    vs_out.tex_coord = in_tex_coord;
    vs_out.light_pos = TBN*in_light_pos;
    vs_out.view_pos = TBN*in_view_pos;

    gl_Position = proj*view*coord;
}
