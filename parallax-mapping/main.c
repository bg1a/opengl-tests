#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>

#include <GLES3/gl31.h>
#include <GL/gl.h>

#include <glm/glm.hpp>

extern void create_window(int w, int h);
extern void window_loop(void);

GLfloat light_pos[] = {
    0.0, 4.0, 4.0,
};

GLfloat view_pos[] = {
    0.0, 0.0, 2.0,
};

//GLfloat alpha = -3.14 / 4.0;
GLfloat alpha = 0.0;
GLfloat model[] = {
    1.0, 0.0,         0.0,        0.0,
    0.0, cos(alpha), -sin(alpha), 0.0,
    0.0, sin(alpha),  cos(alpha), 0.0,
    0.0, 0.0,         0.0,        1.0,
};

GLfloat view[] = {
    1.0,  0.0,  0.0,  view_pos[0],
    0.0,  1.0,  0.0,  view_pos[1],
    0.0,  0.0, -1.0,  view_pos[2],
    0.0,  0.0,  0.0,  1.0,
};

GLfloat zn = 0.1;
GLfloat zf = 100.0;
GLfloat proj[] = {
    1.0,   0.0,   0.0,                  0.0,
    0.0,   1.0,   0.0,                  0.0,
    0.0,   0.0,  -(zn + zf)/(zn - zf),  2*zn*zf/(zn - zf),
    0.0,   0.0,   1.0,                  0.0,
};

GLfloat a[][5] = {
    {-1.0, -1.0,  0.0,  0.0,  0.0,},
    { 1.0, -1.0,  0.0,  1.0,  0.0,},
    { 1.0,  1.0,  0.0,  1.0,  1.0,},
};

GLfloat b[][5] = {
    {-1.0, -1.0,  0.0,  0.0,  0.0,},
    { 1.0,  1.0,  0.0,  1.0,  1.0,},
    {-1.0,  1.0,  0.0,  0.0,  1.0,},
};

void get_TB(const GLfloat v[][5], GLfloat T[3], GLfloat B[3])
{
    glm::vec3 p0(v[0][0], v[0][1], v[0][2]);
    glm::vec3 p1(v[1][0], v[1][1], v[1][2]);
    glm::vec3 p2(v[2][0], v[2][1], v[2][2]);

    glm::vec2 uv0(v[0][3], v[0][4]);
    glm::vec2 uv1(v[1][3], v[1][4]);
    glm::vec2 uv2(v[2][3], v[2][4]);

    glm::mat2 dUV(uv2 - uv0, uv1 - uv0);
    glm::mat3x2 P(p2[0] - p0[0], p1[0] - p0[0],
                  p2[1] - p0[1], p1[1] - p0[1],
                  p2[2] - p0[2], p1[2] - p0[2]);

    glm::mat3x2 TB = glm::inverse(glm::transpose(dUV))*P;
    T[0] = TB[0][0];
    T[1] = TB[1][0];
    T[2] = TB[2][0];

    B[0] = TB[0][1];
    B[1] = TB[1][1];
    B[2] = TB[2][1];

    //printf("===========================================\n");
    //printf("Input points:\n");
    //printf("p0(%g; %g; %g)\n", v[0][0], v[0][1], v[0][2]);
    //printf("p1(%g; %g; %g)\n", v[1][0], v[1][1], v[1][2]);
    //printf("p2(%g; %g; %g)\n", v[2][0], v[2][1], v[2][2]);
    //printf("Tangent Vector:\n");
    //printf("T(%g; %g; %g)\n", T[0], T[1], T[2]);
    //printf("Bitangent Vector:\n");
    //printf("B(%g; %g; %g)\n", B[0], B[1], B[2]);
    //printf("===========================================\n");
}

char *load_file(const char *filename)
{
    FILE *f = fopen(filename, "r");
    char *text = NULL;

    if(f){
        size_t size = 0;

        fseek(f, 0, SEEK_END);
        size = ftell(f);
        rewind(f);

        text = (char*)calloc(size + 1, sizeof(char));
        fread(text, sizeof(char), size, f);
        fclose(f); 
    }

    return text;
}

void load_shader(const char *vertex_source, const char *fragment_source,
        GLuint *vertex, GLuint *fragment, GLuint *program)
{
    const char * test_str = "#version ";
    char infoLog[512] = {0};
    char *shader_source = NULL;

    *vertex = glCreateShader(GL_VERTEX_SHADER);
    if(!strcmp(vertex_source, test_str)){
        glShaderSource(*vertex, 1, &vertex_source, NULL);
        glCompileShader(*vertex);
        glGetShaderInfoLog(*vertex, sizeof(infoLog), NULL, infoLog);
        printf("Vertex shader compile log: %s\n", infoLog);
    }else{
        shader_source = load_file(vertex_source);
        glShaderSource(*vertex, 1, &shader_source, NULL);
        glCompileShader(*vertex);
        glGetShaderInfoLog(*vertex, sizeof(infoLog), NULL, infoLog);
        printf("%s, compile log: %s\n", vertex_source, infoLog);
        free(shader_source);
    }

    *fragment = glCreateShader(GL_FRAGMENT_SHADER);
    if(!strcmp(fragment_source, test_str)){
        glShaderSource(*fragment, 1, &fragment_source, NULL);
        glCompileShader(*fragment);
        glGetShaderInfoLog(*fragment, sizeof(infoLog), NULL, infoLog);
        printf("Fragment shader compile log: %s\n", infoLog);
    }else{
        shader_source = load_file(fragment_source);
        glShaderSource(*fragment, 1, &shader_source, NULL);
        glCompileShader(*fragment);
        glGetShaderInfoLog(*fragment, sizeof(infoLog), NULL, infoLog);
        printf("%s, compile log: %s\n", fragment_source, infoLog);
        free(shader_source);
    }

    *program = glCreateProgram();
    glAttachShader(*program, *vertex);
    glAttachShader(*program, *fragment);
    glLinkProgram(*program);
}

GLuint diff_tex, disp_tex, normal_tex, vs, fs, ph;
GLuint vao, buf;

void draw_opengl(char c)
{
    GLfloat a_T[3] = {0};
    GLfloat a_B[3] = {0};
    GLfloat b_T[3] = {0};
    GLfloat b_B[3] = {0};

    get_TB(a, a_T, a_B);
    get_TB(b, b_T, b_B);

    GLfloat vertices[] = {
        // Vertices                 Textures            Tangent                 Binormal
        a[0][0], a[0][1], a[0][2],  a[0][3], a[0][4],  a_T[0], a_T[1], a_T[2],  a_B[0], a_B[1], a_B[2],
        a[1][0], a[1][1], a[1][2],  a[1][3], a[1][4],  a_T[0], a_T[1], a_T[2],  a_B[0], a_B[1], a_B[2],
        a[2][0], a[2][1], a[2][2],  a[2][3], a[2][4],  a_T[0], a_T[1], a_T[2],  a_B[0], a_B[1], a_B[2],

        b[0][0], b[0][1], b[0][2],  b[0][3], b[0][4],  b_T[0], b_T[1], b_T[2],  b_B[0], b_B[1], b_B[2],
        b[1][0], b[1][1], b[1][2],  b[1][3], b[1][4],  b_T[0], b_T[1], b_T[2],  b_B[0], b_B[1], b_B[2],
        b[2][0], b[2][1], b[2][2],  b[2][3], b[2][4],  b_T[0], b_T[1], b_T[2],  b_B[0], b_B[1], b_B[2],
    };

    glGenVertexArrays(1, &vao);
    glBindVertexArray(vao);
    glGenBuffers(1, &buf);
    glBindBuffer(GL_ARRAY_BUFFER, buf);
    glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 11*sizeof(GLfloat), 0);
    glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 11*sizeof(GLfloat), (void*)(3*sizeof(GLfloat)));
    glVertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE, 11*sizeof(GLfloat), (void*)(5*sizeof(GLfloat)));
    glVertexAttribPointer(3, 3, GL_FLOAT, GL_FALSE, 11*sizeof(GLfloat), (void*)(8*sizeof(GLfloat)));
    glEnableVertexAttribArray(0);
    glEnableVertexAttribArray(1);
    glEnableVertexAttribArray(2);
    glEnableVertexAttribArray(3);

    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, diff_tex);
    glActiveTexture(GL_TEXTURE1);
    glBindTexture(GL_TEXTURE_2D, normal_tex);
    glActiveTexture(GL_TEXTURE2);
    glBindTexture(GL_TEXTURE_2D, disp_tex);

    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glDrawArrays(GL_TRIANGLES, 0, 6);
}

int main(int argc, char **argv)
{
    char *bricks = load_file("bricks2.rgb");
    char *bricks_disp = load_file("bricks2_disp.rgb");
    char *bricks_normal = load_file("bricks2_normal.rgb");

    create_window(800, 600);

    glGenTextures(1, &diff_tex);
    glBindTexture(GL_TEXTURE_2D, diff_tex);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, 512, 512,
                                0, GL_RGB, GL_UNSIGNED_BYTE, bricks);

    glGenTextures(1, &disp_tex);
    glBindTexture(GL_TEXTURE_2D, disp_tex);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, 512, 512,
                                0, GL_RGB, GL_UNSIGNED_BYTE, bricks_disp);

    glGenTextures(1, &normal_tex);
    glBindTexture(GL_TEXTURE_2D, normal_tex);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, 512, 512,
                                0, GL_RGB, GL_UNSIGNED_BYTE, bricks_normal);

    load_shader("pm.vs", "pm.fs", &vs, &fs, &ph);

    glUseProgram(ph);
    glUniform1i(glGetUniformLocation(ph, "diff_tex"), 0);
    glUniform1i(glGetUniformLocation(ph, "normal_tex"), 1);
    glUniform1i(glGetUniformLocation(ph, "disp_tex"), 2);
    glUniform3fv(glGetUniformLocation(ph, "view_pos"), 1, view_pos);
    glUniform3fv(glGetUniformLocation(ph, "light_pos"), 1, light_pos);
    glUniformMatrix4fv(glGetUniformLocation(ph, "model"), 1, GL_TRUE, model);
    glUniformMatrix4fv(glGetUniformLocation(ph, "view"),  1, GL_TRUE, view);
    glUniformMatrix4fv(glGetUniformLocation(ph, "proj"),  1, GL_TRUE, proj);

    window_loop();

    return 0;
}
