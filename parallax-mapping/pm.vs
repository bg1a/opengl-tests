// vim: set ft=glsl:
#version 330 core

layout (location = 0) in vec3 coord;
layout (location = 1) in vec2 tcoord;
layout (location = 2) in vec3 in_tangent;
layout (location = 3) in vec3 in_binormal;

out VS_OUT{
    vec3 coord;
    vec2 tcoord;
    vec3 view_pos;
    vec3 light_pos;
}vs_out;

uniform vec3 view_pos;
uniform vec3 light_pos;

uniform mat4 model;
uniform mat4 view;
uniform mat4 proj;

void main(){
    gl_Position = proj*inverse(view)*model*vec4(coord, 1.0);

    vec3 tangent = normalize(in_tangent);
    vec3 binormal = normalize(in_binormal);
    vec3 fix_binormal = normalize(in_binormal - dot(in_binormal, tangent) * tangent);
    
    vec3 tangent_m = vec3(model*vec4(tangent, 1.0));
    vec3 binormal_m = vec3(model*vec4(fix_binormal, 1.0));
    vec3 normal_m = normalize(cross(tangent_m, binormal_m));

    mat3 TBN = transpose(mat3(tangent_m, binormal_m, normal_m));

    vs_out.coord = TBN*vec3(model*vec4(coord, 1.0));
    vs_out.tcoord = tcoord;
    vs_out.view_pos = TBN*view_pos;
    vs_out.light_pos = TBN*light_pos;
}
