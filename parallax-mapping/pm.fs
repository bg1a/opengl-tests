// vim: set ft=glsl:
#version 330 core

in VS_OUT{
    vec3 coord;
    vec2 tcoord;
    vec3 view_pos;
    vec3 light_pos;
}vs_out;

uniform sampler2D diff_tex;
uniform sampler2D normal_tex;
uniform sampler2D disp_tex;

out vec4 FragColor;

vec2 getParallaxMap(vec2 tcoord, vec3 viewDir)
{
    float disp = texture(disp_tex, vs_out.tcoord).r;
    vec2 tcoord_new = tcoord - viewDir.xy * disp * 0.1;

    return tcoord_new;
}

void main(){
    vec3 V = normalize(vs_out.view_pos - vs_out.coord);
    vec2 texCoord = getParallaxMap(vs_out.tcoord, V);

    if(texCoord.x > 1.0 || texCoord.y > 1.0 || texCoord.x < 0 || texCoord.y < 0)
        discard;

    vec3 color = texture(diff_tex, texCoord).rgb;
    vec3 N = normalize(texture(normal_tex, texCoord).rgb * 2.0 - 1.0);
    vec3 I = normalize(vs_out.coord - vs_out.light_pos);
    vec3 H = normalize(V - I);

    float Kamb = 0.1;
    vec3 Iamb = Kamb * color;

    float Kdiff = 0.5;
    vec3 Idiff = Kdiff * max(dot(-I, N), 0.0) * color;

    float Kspec = 1.0;
    vec3 Ispec = Kspec * pow(max(dot(H, N), 0.0), 16.0) * vec3(1.0);

    FragColor = vec4((Iamb + Idiff + Ispec), 1.0);
}
