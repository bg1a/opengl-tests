// vim: set ft=glsl:
#version 330 core

in VS_OUT{
    vec3 coord;
    vec3 normal;
    vec2 tcoord;
}vs_out;

struct Light{
    vec3 position;
    vec3 color;
};

uniform sampler2D tex;
uniform Light light_source[4];
uniform vec3 view_pos;

out vec4 FragColor;

void main(){
    vec3 color = texture(tex, vs_out.tcoord).rgb;
    vec3 N = normalize(vs_out.normal);

    float Kamb = 0.1;
    vec3 Iamb = Kamb*color;

    vec3 Idiff_all = vec3(0.0);
    float Kdiff = 1.0;

    for(int i = 0; i < 4; ++i){
        vec3 I = normalize(vs_out.coord - light_source[i].position);
        float D = length(vs_out.coord - light_source[i].position);
        vec3 Idiff = Kdiff*max(dot(-I, N), 0.0)*light_source[i].color*color;

        Idiff_all += Idiff / (D*D);
    }

    FragColor = vec4(Iamb + Idiff_all, 1.0);
}
