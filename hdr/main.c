#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

#include <GLES3/gl31.h>
#include <GL/gl.h>

extern void create_window(int w, int h);
extern void window_loop(void);

GLfloat view_pos[] = {
    0.0, 0.0, 5.0,
};
GLfloat light_pos[] = {
    0.0,  0.0,  49.5,
   -1.4, -1.9,  9.0,
    0.0, -1.8,  4.0,
    0.8, -1.7,  6.0,
};
GLfloat light_color[] = {
    200.0,  200.0,  200.0,
      0.1,    0.0,    0.0,
      0.0,    0.0,    0.2,
      0.0,    0.1,    0.0,
};

GLfloat vertices[] = {
    //Vertex             Normals         Texture
   -1.0, -1.0, -1.0,  0.0,  0.0, -1.0,  0.0, 0.0,                                                             
    1.0, -1.0, -1.0,  0.0,  0.0, -1.0,  1.0, 0.0,                                                             
    1.0,  1.0, -1.0,  0.0,  0.0, -1.0,  1.0, 1.0,                                                             
    1.0,  1.0, -1.0,  0.0,  0.0, -1.0,  1.0, 1.0,                                                             
   -1.0,  1.0, -1.0,  0.0,  0.0, -1.0,  0.0, 1.0,                                                             
   -1.0, -1.0, -1.0,  0.0,  0.0, -1.0,  0.0, 0.0,                                                             
                                      
   -1.0, -1.0,  1.0,  0.0,  0.0,  1.0,  0.0, 0.0,                                                            
    1.0, -1.0,  1.0,  0.0,  0.0,  1.0,  1.0, 0.0,                                                            
    1.0,  1.0,  1.0,  0.0,  0.0,  1.0,  1.0, 1.0,                                                            
    1.0,  1.0,  1.0,  0.0,  0.0,  1.0,  1.0, 1.0,                                                            
   -1.0,  1.0,  1.0,  0.0,  0.0,  1.0,  0.0, 1.0,                                                            
   -1.0, -1.0,  1.0,  0.0,  0.0,  1.0,  0.0, 0.0,                                                            

   -1.0,  1.0,  1.0, -1.0,  0.0,  0.0,  1.0, 1.0,                                                   
   -1.0,  1.0, -1.0, -1.0,  0.0,  0.0,  0.0, 1.0,                                                   
   -1.0, -1.0, -1.0, -1.0,  0.0,  0.0,  0.0, 0.0,                                                   
   -1.0, -1.0, -1.0, -1.0,  0.0,  0.0,  0.0, 0.0,                                                   
   -1.0, -1.0,  1.0, -1.0,  0.0,  0.0,  1.0, 0.0,                                                   
   -1.0,  1.0,  1.0, -1.0,  0.0,  0.0,  1.0, 1.0,                                                   
                                                 
    1.0,  1.0,  1.0,  1.0,  0.0,  0.0,  0.0, 0.0,                                                   
    1.0,  1.0, -1.0,  1.0,  0.0,  0.0,  1.0, 0.0,                                                   
    1.0, -1.0, -1.0,  1.0,  0.0,  0.0,  1.0, 1.0,                                                   
    1.0, -1.0, -1.0,  1.0,  0.0,  0.0,  1.0, 1.0,                                                   
    1.0, -1.0,  1.0,  1.0,  0.0,  0.0,  0.0, 1.0,                                                   
    1.0,  1.0,  1.0,  1.0,  0.0,  0.0,  0.0, 0.0,                                                   

   -1.0, -1.0, -1.0,  0.0, -1.0,  0.0,  0.0, 0.0,                                                  
    1.0, -1.0, -1.0,  0.0, -1.0,  0.0,  1.0, 0.0,                                                  
    1.0, -1.0,  1.0,  0.0, -1.0,  0.0,  1.0, 1.0,                                                  
    1.0, -1.0,  1.0,  0.0, -1.0,  0.0,  1.0, 1.0,                                                  
   -1.0, -1.0,  1.0,  0.0, -1.0,  0.0,  0.0, 1.0,                                                  
   -1.0, -1.0, -1.0,  0.0, -1.0,  0.0,  0.0, 0.0,                                                  
                                                 
    1.0,  1.0,  1.0,  0.0,  1.0,  0.0,  1.0, 1.0,                                                  
    1.0,  1.0, -1.0,  0.0,  1.0,  0.0,  1.0, 0.0,                                                  
   -1.0,  1.0, -1.0,  0.0,  1.0,  0.0,  0.0, 0.0,                                                  
    1.0,  1.0,  1.0,  0.0,  1.0,  0.0,  1.0, 1.0,                                                  
   -1.0,  1.0, -1.0,  0.0,  1.0,  0.0,  0.0, 0.0,                                                  
   -1.0,  1.0,  1.0,  0.0,  1.0,  0.0,  0.0, 1.0,                                                  
};

GLfloat vertices_hdr[] = {
    -1.0, -1.0,  0.0,   0.0, 0.0,
     1.0, -1.0,  0.0,   1.0, 0.0,
     1.0,  1.0,  0.0,   1.0, 1.0,

    -1.0, -1.0,  0.0,   0.0, 0.0,
     1.0,  1.0,  0.0,   1.0, 1.0,
    -1.0,  1.0,  0.0,   0.0, 1.0,
};

char *load_file(const char *filename)
{
    FILE *f = fopen(filename, "r");
    char *text = NULL;

    if(f){
        size_t size = 0;

        fseek(f, 0, SEEK_END);
        size = ftell(f);
        rewind(f);

        text = (char*)calloc(size + 1, sizeof(char));
        fread(text, sizeof(char), size, f);
        fclose(f); 
    }

    return text;
}

void load_shader(const char *vertex_source, const char *fragment_source,
        GLuint *vertex, GLuint *fragment, GLuint *program)
{
    const char * test_str = "#version ";
    char infoLog[512] = {0};
    char *shader_source = NULL;

    *vertex = glCreateShader(GL_VERTEX_SHADER);
    if(!strcmp(vertex_source, test_str)){
        glShaderSource(*vertex, 1, &vertex_source, NULL);
        glCompileShader(*vertex);
        glGetShaderInfoLog(*vertex, sizeof(infoLog), NULL, infoLog);
        printf("Vertex shader compile log: %s\n", infoLog);
    }else{
        shader_source = load_file(vertex_source);
        glShaderSource(*vertex, 1, &shader_source, NULL);
        glCompileShader(*vertex);
        glGetShaderInfoLog(*vertex, sizeof(infoLog), NULL, infoLog);
        printf("%s, compile log: %s\n", vertex_source, infoLog);
        free(shader_source);
    }

    *fragment = glCreateShader(GL_FRAGMENT_SHADER);
    if(!strcmp(fragment_source, test_str)){
        glShaderSource(*fragment, 1, &fragment_source, NULL);
        glCompileShader(*fragment);
        glGetShaderInfoLog(*fragment, sizeof(infoLog), NULL, infoLog);
        printf("Fragment shader compile log: %s\n", infoLog);
    }else{
        shader_source = load_file(fragment_source);
        glShaderSource(*fragment, 1, &shader_source, NULL);
        glCompileShader(*fragment);
        glGetShaderInfoLog(*fragment, sizeof(infoLog), NULL, infoLog);
        printf("%s, compile log: %s\n", fragment_source, infoLog);
        free(shader_source);
    }

    *program = glCreateProgram();
    glAttachShader(*program, *vertex);
    glAttachShader(*program, *fragment);
    glLinkProgram(*program);
}

GLuint tex, vao, buf;
GLuint cube_vs, cube_fs, cube_ph;
GLuint fbo, color_tex, rbo;
GLuint hdr_vao, hdr_buf, hdr_vs, hdr_fs, hdr_ph;

void draw_opengl(char c)
{
    glBindFramebuffer(GL_FRAMEBUFFER, fbo);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        glBindVertexArray(vao);
        glActiveTexture(GL_TEXTURE0);
        glBindTexture(GL_TEXTURE_2D, tex);
        glUseProgram(cube_ph);

        glDrawArrays(GL_TRIANGLES, 0, 36);
    glBindFramebuffer(GL_FRAMEBUFFER, 0);

    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glBindVertexArray(hdr_vao);
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, color_tex);
    glUseProgram(hdr_ph);

    glDrawArrays(GL_TRIANGLES, 0, 6);
}

int main(int argc, char **argv)
{
    char *img = load_file("wood.rgb"); 
    int width = 800;
    int height = 600;
    glm::mat4 proj = glm::perspective(glm::radians(45.0f), (float)width/(float)height, 0.1f, 100.0f);
    glm::mat4 view = glm::lookAt(glm::vec3(view_pos[0], view_pos[1], view_pos[2]),
                                 glm::vec3(0.0, 0.0, 10.0),
                                 glm::vec3(0.0, 1.0, 0.0));
    glm::mat4 model(1.0);
    model = glm::translate(model, glm::vec3(0.0f, 0.0f, 25.0f));
    model = glm::scale(model, glm::vec3(2.5f, 2.5f, 27.5f));

    create_window(width, height);

    glGenVertexArrays(1, &vao);
    glBindVertexArray(vao);
    glGenBuffers(1, &buf);
    glBindBuffer(GL_ARRAY_BUFFER, buf);
    glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 8*sizeof(GLfloat), 0);
    glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 8*sizeof(GLfloat), (void*)(3*sizeof(GLfloat)));
    glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, 8*sizeof(GLfloat), (void*)(6*sizeof(GLfloat)));
    glEnableVertexAttribArray(0);
    glEnableVertexAttribArray(1);
    glEnableVertexAttribArray(2);

    glGenTextures(1, &tex);
    glBindTexture(GL_TEXTURE_2D, tex);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_SRGB, 1024, 1024,
                                0, GL_RGB, GL_UNSIGNED_BYTE, img);
    glGenerateMipmap(GL_TEXTURE_2D);

    load_shader("cube.vs", "cube.fs", &cube_vs, &cube_fs, &cube_ph);
    glUseProgram(cube_ph);

    glUniform1i(glGetUniformLocation(cube_ph, "tex"), 0);
    glUniformMatrix4fv(glGetUniformLocation(cube_ph, "model"), 1, GL_FALSE, glm::value_ptr(model));
    glUniformMatrix4fv(glGetUniformLocation(cube_ph, "view"), 1, GL_FALSE, glm::value_ptr(view));
    glUniformMatrix4fv(glGetUniformLocation(cube_ph, "proj"), 1, GL_FALSE, glm::value_ptr(proj));
    glUniform1fv(glGetUniformLocation(cube_ph, "view_pos"), 1, view_pos);

    for(int i = 0; i < 4; ++i){
        char uniform_position[30] = {0};
        char uniform_color[30] = {0};

        snprintf(uniform_position, sizeof(uniform_position), "light_source[%d].position", i);
        glUniform3fv(glGetUniformLocation(cube_ph, uniform_position), 1, &light_pos[i*3]);
        snprintf(uniform_color, sizeof(uniform_color), "light_source[%d].color", i);
        glUniform3fv(glGetUniformLocation(cube_ph, uniform_color), 1, &light_color[i*3]);
    }

    glGenFramebuffers(1, &fbo);
    glBindFramebuffer(GL_FRAMEBUFFER, fbo);
    glGenTextures(1, &color_tex);
    glBindTexture(GL_TEXTURE_2D, color_tex);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA16F, 800, 600,
                                0, GL_RGBA, GL_FLOAT, NULL);
    glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, 
                                color_tex, 0);
    glGenRenderbuffers(1, &rbo);
    glBindRenderbuffer(GL_RENDERBUFFER, rbo);
    glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT, 800, 600);
    glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, rbo);
    glBindFramebuffer(GL_FRAMEBUFFER, 0);

    // Render the HDR image
    glGenVertexArrays(1, &hdr_vao);
    glBindVertexArray(hdr_vao);
    glGenBuffers(1, &hdr_buf);
    glBindBuffer(GL_ARRAY_BUFFER, hdr_buf);
    glBufferData(GL_ARRAY_BUFFER, sizeof(vertices_hdr), vertices_hdr, GL_STATIC_DRAW);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 5*sizeof(GLfloat), 0);
    glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 5*sizeof(GLfloat), (void*)(3*sizeof(GLfloat)));
    glEnableVertexAttribArray(0);
    glEnableVertexAttribArray(1);

    load_shader("hdr.vs", "hdr.fs", &hdr_vs, &hdr_fs, &hdr_ph);
    glUseProgram(hdr_ph);
    glUniform1i(glGetUniformLocation(hdr_ph, "tex"), 0);

    glEnable(GL_DEPTH_TEST);

    window_loop();

    return 0;
}

