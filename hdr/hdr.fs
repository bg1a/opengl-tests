// vim: set ft=glsl:
#version 330 core

in VS_OUT{
    vec2 tcoord;
}vs_out;

out vec4 FragColor;
uniform sampler2D tex;

void main(){
    float gamma = 2.2;
    float exposure = 0.1;

    vec3 color = texture(tex, vs_out.tcoord).rgb;
    //color = color / (color + vec3(1.0));
    color = vec3(1.0) - exp(-color * exposure);

    FragColor = vec4(pow(color, vec3(1/gamma)), 1.0);
}
