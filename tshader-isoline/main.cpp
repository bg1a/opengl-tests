#include "glad.h"
#define GLFW_INCLUDE_NONE
#include <GLFW/glfw3.h>

#include <glm/glm.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtc/matrix_transform.hpp>

#include "load.h"

const int width = 800;
const int height = 600;
const float camX = 1.0f;
const float camY = 0.0f;
const float camZ = 3.0f;

#define ARRAY_SZ(_a) (sizeof(_a) / sizeof((_a)[0]))

GLfloat vertices[] = 
{
    -1.0, -1.0,  1.0,
     0.0, -0.5, -1.0,
     0.5, -0.7,  0.7,
    -0.7,  0.0,  0.2, 
    -0.5,  0.7, -0.2,
     1.0,  1.0, -1.0,
};

GLuint indices[] = 
{
    0, 0, 0, 1,
    0, 0, 1, 2,
    0, 1, 2, 3,
    1, 2, 3, 4,
    2, 3, 4, 5,
    3, 4, 5, 5,
    4, 5, 5, 5,
};

const shader_handle_t shaders[] = {
    {GL_VERTEX_SHADER, "main.vs"},
    {GL_TESS_CONTROL_SHADER, "main.tc"},
    {GL_TESS_EVALUATION_SHADER, "main.te"},
    {GL_FRAGMENT_SHADER, "main.fs"},
};

GLuint vao, ebo, buf, ph;
GLFWwindow* window;
void draw_opengl(void)
{
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glClearColor(0.12, 0.1, 0.12, 1.0);

    glDrawElements(GL_PATCHES, ARRAY_SZ(indices), GL_UNSIGNED_INT, 0);
    //glDrawArrays(GL_LINE_STRIP, 0, 6);
}

int main(int argc, char **argv)
{
    glfwInit();
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 6);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
    if(!(window = glfwCreateWindow(width, height, "Tesselation Shader, Teapot", NULL, NULL))){
        printf("Failed to create a window!\n");
        return -1;
    }
    glfwMakeContextCurrent(window);

    gladLoadGL();
    glfwSwapInterval(1);

    glGenVertexArrays(1, &vao);
    glBindVertexArray(vao);
    glGenBuffers(1, &ebo);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ebo);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(indices), indices, GL_STATIC_DRAW);
    glGenBuffers(1, &buf);
    glBindBuffer(GL_ARRAY_BUFFER, buf);
    glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3*sizeof(GLfloat), 0);
    glEnableVertexAttribArray(0);

    load_shaders(shaders, SHADER_ARR_SZ(shaders), &ph);
    glUseProgram(ph);

    glm::mat4 model(1.0);
    glUniformMatrix4fv(glGetUniformLocation(ph, "model"), 1, GL_FALSE, glm::value_ptr(model));
    glm::mat4 proj = glm::perspective(glm::radians(45.0f), (float)width/(float)height, 0.1f, 100.0f);
    glm::mat4 view = glm::lookAt(glm::vec3(camX, camY, camZ),
                                 glm::vec3(0.0f, 0.0f, 0.0f), glm::vec3(0.0f, 1.0f, 0.0f));
    glUniformMatrix4fv(glGetUniformLocation(ph, "view"), 1, GL_FALSE, glm::value_ptr(view));
    glUniformMatrix4fv(glGetUniformLocation(ph, "proj"), 1, GL_FALSE, glm::value_ptr(proj));

    glPatchParameteri(GL_PATCH_VERTICES, 4);
    glEnable(GL_DEPTH_TEST);

    while(!glfwWindowShouldClose(window)){
        int width, height;
        glfwGetFramebufferSize(window, &width, &height);
        glViewport(0, 0, width, height);

        draw_opengl();
        glfwSwapBuffers(window);
        glfwPollEvents();
    }
    glfwDestroyWindow(window);
    glfwTerminate();

    return 0;
}
