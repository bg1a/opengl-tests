#include <stdlib.h>
#include <stdio.h>

char *load_file(const char *filename)
{
    FILE *f = fopen(filename, "r");
    char *text = NULL;

    if(f){
        size_t size = 0;

        fseek(f, 0, SEEK_END);
        size = ftell(f);
        rewind(f);

        text = (char*)calloc(size + 1, sizeof(char));
        fread(text, sizeof(char), size, f);
        fclose(f); 
    }

    return text;
}

const char *shader_type_str(GLuint shader_type)
{
    const char *type = "unknown";
    switch(shader_type){
        case GL_VERTEX_SHADER: type = "vertex"; break;
        case GL_FRAGMENT_SHADER: type = "fragment"; break;
        case GL_TESS_CONTROL_SHADER: type = "tesselation control"; break;
        case GL_TESS_EVALUATION_SHADER: type = "tesselation evaluation"; break;
    }
    return type;
}

GLuint load_shader(GLuint shader_type, const char *src)
{
    const char * test_str = "#version ";
    char infoLog[512] = {0};
    GLuint shader_handle;

    shader_handle = glCreateShader(shader_type);
    if(!strcmp(src, test_str)){
        glShaderSource(shader_handle, 1, &src, NULL);
        glCompileShader(shader_handle);
        glGetShaderInfoLog(shader_handle, sizeof(infoLog), NULL, infoLog);
    }else{
        char *shader_source = load_file(src);
        //printf("Processing shader: %s\n", shader_source);
        glShaderSource(shader_handle, 1, &shader_source, NULL);
        glCompileShader(shader_handle);
        glGetShaderInfoLog(shader_handle, sizeof(infoLog), NULL, infoLog);
        free(shader_source);
    }
    printf("%s shader compile log: %s\n", shader_type_str(shader_type), infoLog);

    return shader_handle;
}

typedef struct shader_handle_t{
    GLuint type; 
    const char *src;
}shader_handle_t;

#define SHADER_ARR_SZ(_a) (sizeof(_a) / sizeof(shader_handle_t))

void load_shaders(const shader_handle_t *handles, int handles_num, GLuint *program)
{
    char infoLog[512] = {0};

    if(handles && (handles_num > 0) && program){
        GLuint hdls[handles_num];
        int i;

        for(i = 0; i < handles_num; ++i){
            hdls[i] = load_shader(handles[i].type, handles[i].src); 
        }

        *program = glCreateProgram();

        for(i = 0; i < handles_num; ++i){
            glAttachShader(*program, hdls[i]);
        }
        glLinkProgram(*program);
        //glGetProgramiv(program, GL_LINK_STATUS, &linkSuccess);          
        glGetProgramInfoLog(*program, sizeof(infoLog), 0, infoLog);
        printf("Linker log: %s\n", infoLog);
    }
}

