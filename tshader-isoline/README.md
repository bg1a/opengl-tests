Teapot
======

Links
-----
[OpenGL Insights Teapot Example](https://github.com/prideout/opengl-insights)
[Catmul-Rom](https://enochtsang.com/articles/tessellation-shaders-isolines)
[Catmul-Rom, GitHub](https://github.com/enochtsang/catmull_rom_spine_opengl)
[BSpline](http://www2.cs.uregina.ca/~anima/408/Notes/Interpolation/UniformBSpline.htm)
[BSpline, StackExchange](https://math.stackexchange.com/questions/699113/what-is-the-relationship-between-cubic-b-splines-and-cubic-splines/700183)

