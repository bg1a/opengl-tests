#include <stdlib.h>
#include <stdio.h>
#include <iostream>

#include <GLES3/gl31.h>
#include <GL/gl.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

int width = 800; int height = 600;

GLfloat skybox_vs[] = {
    // Face
    -1.0, -1.0,  1.0,
     1.0, -1.0,  1.0,
    -1.0,  1.0,  1.0,
    -1.0,  1.0,  1.0,
     1.0, -1.0,  1.0,
     1.0,  1.0,  1.0,
                     
    // Back
    -1.0, -1.0, -1.0,
    -1.0,  1.0, -1.0,
     1.0, -1.0, -1.0,
     1.0, -1.0, -1.0,
     1.0,  1.0, -1.0,
    -1.0,  1.0, -1.0,

    // Top
    -1.0,  1.0, -1.0,
     1.0,  1.0,  1.0,
    -1.0,  1.0,  1.0,
    -1.0,  1.0, -1.0,
     1.0,  1.0, -1.0,
     1.0,  1.0,  1.0,
                     
    // Bottom
    -1.0, -1.0,  1.0,
     1.0, -1.0, -1.0,
    -1.0, -1.0, -1.0,
    -1.0, -1.0,  1.0,
     1.0, -1.0,  1.0,
     1.0, -1.0, -1.0,
                     
    // Left
    -1.0,  1.0, -1.0,
    -1.0, -1.0,  1.0,
    -1.0, -1.0, -1.0,
    -1.0,  1.0, -1.0,
    -1.0,  1.0,  1.0,
    -1.0, -1.0,  1.0,
                     
    // Right
     1.0,  1.0,  1.0,
     1.0, -1.0, -1.0,
     1.0, -1.0,  1.0,
     1.0,  1.0,  1.0,
     1.0,  1.0, -1.0,
     1.0, -1.0, -1.0,
};

const char *skybox_vsh = 
"#version 140\n"
"attribute vec3 pos;"
"varying vec3 vpos;"
"void main(){"
" mat4 p = mat4(1.0);"
" mat4 v = mat4(1.0);"
" mat4 vt = mat4(1.0);"
" "
" float zn = 0.1;"
" float zf = 100.0;"
" "
" v[2][2] = -1.0;"
" vt[3] = vec4(0.0, 0.0, 1.0, 1.0);"
" p[2][2] = -(zn + zf)/(zn - zf); p[2][3] = 1.0;"
" p[3][2] = (2.0*zn*zf)/(zn - zf); p[3][3] = 0.0;"
" gl_Position = p*inverse(vt*v)*vec4(pos, 1.0);"
" vpos = vec3(pos.x, pos.y, pos.z);"
"}";

const char *skybox_fsh = 
"#version 140\n"
"#extension GL_NV_shadow_samplers_cube : enable\n"
"precision mediump float;"
"varying vec3 vpos;"
"uniform samplerCube tex;"
"void main(){"
" gl_FragColor = textureCube(tex, vpos);"
"}";

char *load_file(const char *fname)
{
    char *fbuf = NULL;

    if(fname){
        FILE *f = fopen(fname, "rb");
        if(f){
            size_t fsize;

            fseek(f, 0, SEEK_END);
            fsize = ftell(f);
            rewind(f);

            fbuf = (char *)malloc(fsize);
            fread(fbuf, sizeof(char), fsize, f);
        
            fclose(f);
        }
    }else{
        printf("Bad filename!\n");
    }

    return fbuf;
}

GLuint skybox_vao, skybox_buf, skybox_vh, skybox_fh, skybox_ph, skybox_tex;

void draw_opengl(void)
{
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    glDepthMask(GL_FALSE);
    glDrawArrays(GL_TRIANGLES, 0, 36);
    glDepthMask(GL_TRUE);
}

extern void create_window(int width, int height);
extern void window_loop();

int main(int argc, char **argv)
{
    char infoLog[512] = {0};
    char *top_data = load_file("top.rgb");
    char *bottom_data = load_file("bottom.rgb");
    char *front_data = load_file("front.rgb");
    char *back_data = load_file("back.rgb");
    char *left_data = load_file("left.rgb");
    char *right_data = load_file("right.rgb");

    create_window(800, 600);

    glGenVertexArrays(1, &skybox_vao);
    glBindVertexArray(skybox_vao);
    glGenBuffers(1, &skybox_buf);
    glBindBuffer(GL_ARRAY_BUFFER, skybox_buf);
    glBufferData(GL_ARRAY_BUFFER, sizeof(skybox_vs), skybox_vs, GL_STATIC_DRAW);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3*sizeof(GLfloat), 0);
    glEnableVertexAttribArray(0);

    skybox_vh = glCreateShader(GL_VERTEX_SHADER);
    glShaderSource(skybox_vh, 1, &skybox_vsh, NULL);
    glCompileShader(skybox_vh);
    glGetShaderInfoLog(skybox_vh, sizeof(infoLog), NULL, infoLog);
    printf("Skybox vertex shader info: %s\n", infoLog);

    skybox_fh = glCreateShader(GL_FRAGMENT_SHADER);
    glShaderSource(skybox_fh, 1, &skybox_fsh, NULL);
    glCompileShader(skybox_fh);
    glGetShaderInfoLog(skybox_fh, sizeof(infoLog), NULL, infoLog);
    printf("Skybox fragment shader info: %s\n", infoLog);

    glGenTextures(1, &skybox_tex);
    glBindTexture(GL_TEXTURE_CUBE_MAP, skybox_tex);
    glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X, 0, GL_RGB, 2048, 2048,
                                                 0, GL_RGB, GL_UNSIGNED_BYTE, right_data);
    glTexImage2D(GL_TEXTURE_CUBE_MAP_NEGATIVE_X, 0, GL_RGB, 2048, 2048,
                                                 0, GL_RGB, GL_UNSIGNED_BYTE, left_data);
    glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_Y, 0, GL_RGB, 2048, 2048,
                                                 0, GL_RGB, GL_UNSIGNED_BYTE, top_data);
    glTexImage2D(GL_TEXTURE_CUBE_MAP_NEGATIVE_Y, 0, GL_RGB, 2048, 2048,
                                                 0, GL_RGB, GL_UNSIGNED_BYTE, bottom_data);
    glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_Z, 0, GL_RGB, 2048, 2048,
                                                 0, GL_RGB, GL_UNSIGNED_BYTE, front_data);
    glTexImage2D(GL_TEXTURE_CUBE_MAP_NEGATIVE_Z, 0, GL_RGB, 2048, 2048,
                                                 0, GL_RGB, GL_UNSIGNED_BYTE, back_data);

    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);

    skybox_ph = glCreateProgram();
    glAttachShader(skybox_ph, skybox_vh);
    glAttachShader(skybox_ph, skybox_fh);
    glLinkProgram(skybox_ph);
    glUseProgram(skybox_ph);

    glEnable(GL_DEPTH_TEST);

    window_loop();

    free(top_data);
    free(bottom_data);
    free(front_data);
    free(back_data);
    free(left_data);
    free(right_data);
    return 0;
}
