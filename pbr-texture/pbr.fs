// vim: set ft=glsl:
#version 330 core

in VS_OUT{
    vec3 coord;
    vec3 normal;
    vec2 tcoord;
}vs_out;

out vec4 FragColor;

struct LightSource{
    vec3 pos;
    vec3 color;
};

uniform LightSource lsrc[1];
uniform vec3 view_pos;

uniform sampler2D albedo_tex;
uniform sampler2D ao_tex;
uniform sampler2D metallic_tex;
uniform sampler2D normal_tex;
uniform sampler2D roughness_tex;

const float PI = 3.14159265359;

vec3 mapNormal(){
#if 0
    vec3 normal_t = texture(normal_tex, vs_out.tcoord).rgb * 2.0 - 1.0;
    vec2 dUV10 = dFdx(vs_out.tcoord);
    vec2 dUV20 = dFdy(vs_out.tcoord);
    vec3 dP10 = dFdx(vs_out.coord);
    vec3 dP20 = dFdy(vs_out.coord);

    mat2 dUV = transpose(mat2(dUV10, dUV20));
    mat3x2 dP = transpose(mat2x3(dP10, dP20));

    mat2x3 TB = transpose(inverse(dUV) * dP);

    vec3 N = normalize(vs_out.normal);
    vec3 T = TB[0] - dot(N, TB[0]) * N;
    vec3 B = -normalize(cross(N, T));

    mat3 TBN_inv = mat3(T, B, N);

    return normalize(TBN_inv * normal_t);
#else
    vec3 tangentNormal = texture(normal_tex, vs_out.tcoord).xyz * 2.0 - 1.0;

    vec3 Q1  = dFdx(vs_out.coord);
    vec3 Q2  = dFdy(vs_out.coord);
    vec2 st1 = dFdx(vs_out.tcoord);
    vec2 st2 = dFdy(vs_out.tcoord);

    vec3 N   = normalize(vs_out.normal);
    vec3 T  = normalize(Q1*st2.t - Q2*st1.t);
    vec3 B  = -normalize(cross(N, T));
    mat3 TBN = mat3(T, B, N);

    return normalize(TBN * tangentNormal);
#endif
}

float DistributionGGX(vec3 N, vec3 H, float roughness){
    float a = roughness * roughness;
    float a2 = a * a;
    float NdotH = max(dot(N, H), 0.0);
    float NdotH2 = NdotH * NdotH;

    float nom = a2;
    float denom = (NdotH2 * (a2 - 1.0) + 1.0);
    denom = PI * denom * denom;

    return nom / max(denom, 0.001);
}

float GeometrySchlickGGX(float NdotV, float roughness){
    float r = (roughness + 1.0);
    float k = (r*r) / 8.0;

    float nom = NdotV;
    float denom = NdotV * (1.0 - k) + k;

    return nom / denom;
}

float GeometrySmith(vec3 N, vec3 V, vec3 L, float roughness){
    float NdotV = max(dot(N, V), 0.0);
    float NdotL = max(dot(N, L), 0.0);
    float ggx2 = GeometrySchlickGGX(NdotV, roughness);
    float ggx1 = GeometrySchlickGGX(NdotL, roughness);

    return ggx1 * ggx2;
}

vec3 fresnelSchlick(float cosTheta, vec3 F0){
    return F0 + (1.0 - F0) * pow(1.0 - cosTheta, 5.0);
}

void main(){
    //vec3 N = normalize(vs_out.normal);
    vec3 N = mapNormal();
    vec3 V = normalize(view_pos - vs_out.coord);
    vec3 albedo = pow(texture(albedo_tex, vs_out.tcoord).rgb, vec3(2.2));
    float ao = texture(ao_tex, vs_out.tcoord).r;
    float metallic = texture(metallic_tex, vs_out.tcoord).r;
    float roughness = texture(roughness_tex, vs_out.tcoord).r;

    vec3 F0 = vec3(0.04);
    F0 = mix(F0, albedo, metallic);

    vec3 Lo = vec3(0.0);
    for(int i = 0; i < lsrc.length(); ++i){
        vec3 L = normalize(lsrc[i].pos - vs_out.coord);
        vec3 H = normalize(V + L);
        float D = length(lsrc[i].pos - vs_out.coord);
        float A = 1.0 / (D * D);
        vec3 radiance = lsrc[i].color * A;

        float NDF = DistributionGGX(N, H, roughness);
        float G = GeometrySmith(N, V, L, roughness);
        vec3 F = fresnelSchlick(clamp(dot(H, V), 0.0, 1.0), F0);

        vec3 nominator = NDF * G * F;
        float denominator = 4 * max(dot(N, V), 0.0) * max(dot(N, L), 0.0);
        vec3 specular = nominator / max(denominator, 0.001);

        vec3 kS = F;
        vec3 kD = vec3(1.0) - kS;
        kD *= 1.0 - metallic;

        float NdotL = max(dot(N, L), 0.0);

        Lo += (kD * albedo / PI + specular) * radiance * NdotL;
    }

    vec3 ambient = vec3(0.03) * albedo * ao;
    vec3 color = ambient + Lo;

    color = color / (vec3(1.0) + color);
    color = pow(color, vec3(1.0/2.2));

    FragColor = vec4(color, 1.0);
}
