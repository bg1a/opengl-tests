// vim: set ft=glsl:
#version 330 core

layout (location = 0) in vec3 coord;
layout (location = 1) in vec3 normal;
layout (location = 2) in vec2 tex_coord;

uniform mat4 model;
uniform mat4 view;
uniform mat4 proj;

out VS_OUT{
    vec3 coord;
    vec3 normal;
    vec2 tcoord;
}vs_out;


void main(){
    mat3 model_m3 = mat3(model);

    gl_Position = proj*view*model*vec4(coord, 1.0);

    vs_out.coord = model_m3*coord;
    vs_out.normal = transpose(inverse(model_m3))*normal;
    vs_out.tcoord = tex_coord;
}
