#version 330 core

layout (location = 0) in vec3 coord;
layout (location = 1) in vec2 tcoord;
layout (location = 2) in vec3 tangent;
layout (location = 3) in vec3 binormal;

out VS_OUT{
    vec3 coord;
    vec2 tcoord;
    vec3 view_pos;
    vec3 light_pos;
}vs_out;

uniform mat4 proj;
uniform mat4 view;
uniform mat4 model;

uniform vec3 light_pos;
uniform vec3 view_pos;
uniform bool enable_tex_map;
void main(){
    gl_Position = proj*inverse(view)*model*vec4(coord, 1.0);

    vec3 in_tangent = normalize(tangent);
    vec3 in_binormal = normalize(binormal);
    vec3 fix_binormal = normalize(in_binormal - dot(in_tangent, in_binormal)*in_tangent);

    vec3 model_vcoord = vec3(model*vec4(coord, 1.0));
    vec3 model_tangent = vec3(model*vec4(in_tangent, 1.0));
    vec3 model_binormal = vec3(model*vec4(fix_binormal, 1.0));
    vec3 model_normal = cross(model_tangent, model_binormal);

    mat3 TBN = mat3(normalize(model_tangent),
                    normalize(model_binormal),
                    normalize(model_normal));

    if(enable_tex_map){
        vs_out.view_pos = TBN * view_pos;
        vs_out.light_pos = TBN * light_pos;
        vs_out.coord = TBN * model_vcoord;
    }else{
        vs_out.view_pos = view_pos;
        vs_out.light_pos = light_pos;
        vs_out.coord = model_vcoord;
    }
    vs_out.tcoord = tcoord;
}
