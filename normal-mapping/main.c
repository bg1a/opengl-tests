#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>

#include <sys/time.h>

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/mat2x3.hpp>

#include <GLES3/gl31.h>
#include <GL/gl.h>

bool is_bpressed = false;
extern void window_loop(void);
extern void create_window(int w, int h);

const GLfloat zn = 0.1;
const GLfloat zf = 100.0;

GLfloat view_pos[] = {
    // X    Y    Z
      0.0, 1.0, 1.0,
};

GLfloat light_distance = 3.0;
GLfloat light_pos[] = {
    // X                Y               Z
     light_distance,  light_distance,   0.0,
};

GLfloat proj[] = {
    1.0, 0.0,  0.0,                 0.0,
    0.0, 1.0,  0.0,                 0.0,
    0.0, 0.0, -(zn + zf)/(zn - zf), 2*zn*zf/(zn -zf),
    0.0, 0.0,  1.0,                 0.0,                
};

GLfloat view[] = {
    1.0,  0.0,  0.0,  view_pos[0],
    0.0,  1.0,  0.0,  view_pos[1],
    0.0,  0.0, -1.0,  view_pos[2],
    0.0,  0.0,  0.0,  1.0,
};

const GLfloat alpha = 3.14 / 2.0;
GLfloat model[] = {
    1.0, 0.0,         0.0,        0.0,
    0.0, cos(alpha), -sin(alpha), 0.0,
    0.0, sin(alpha),  cos(alpha), 0.0,
    0.0, 0.0,         0.0,        1.0,
};

// Triangle A
GLfloat a[][5] = {
    // vertices       texture 
   -1.0, -1.0,  0.0,  0.0, 0.0, 
    1.0, -1.0,  0.0,  1.0, 0.0,
    1.0,  1.0,  0.0,  1.0, 1.0,
};

// Triangle B
GLfloat b[][5] = {
    // vertices       texture 
   -1.0, -1.0,  0.0,  0.0, 0.0,
    1.0,  1.0,  0.0,  1.0, 1.0,
   -1.0,  1.0,  0.0,  0.0, 1.0,
};

void get_tb(const GLfloat v[][5], GLfloat T[3], GLfloat B[3])
{
    //                   X                  Y               Z
    GLfloat p1[]   = {v[0][0],          v[0][1],        v[0][2]};
    GLfloat p2[]   = {v[1][0],          v[1][1],        v[1][2]};
    GLfloat p3[]   = {v[2][0],          v[2][1],        v[2][2]};
    GLfloat p1p3[] = {p3[0] - p1[0],    p3[1] - p1[1],  p3[2] - p1[2]}; 
    GLfloat p1p2[] = {p2[0] - p1[0],    p2[1] - p1[1],  p2[2] - p1[2]};

    //                U         V
    GLfloat m1[] = {v[0][3], v[0][4]};
    GLfloat m2[] = {v[1][3], v[1][4]};
    GLfloat m3[] = {v[2][3], v[2][4]};

    GLfloat dU31 = m3[0] - m1[0];
    GLfloat dU21 = m2[0] - m1[0];
    GLfloat dV31 = m3[1] - m1[1];
    GLfloat dV21 = m2[1] - m1[1];

    glm::mat2 UV = glm::mat2(dU31, dU21, dV31, dV21);
    glm::mat2 invUV = glm::inverse(UV); 
    glm::mat3x2 p = glm::mat3x2(p1p3[0], p1p2[0],
                                p1p3[1], p1p2[1],
                                p1p3[2], p1p2[2]);
    glm::mat3x2 TB = invUV * p;

    T[0] = TB[0][0];
    T[1] = TB[1][0];
    T[2] = TB[2][0];

    B[0] = TB[0][1];
    B[1] = TB[1][1];
    B[2] = TB[2][1];
}

char *load_file(const char *filename)
{
    FILE *f = fopen(filename, "r");
    char *text = NULL;

    if(f){
        size_t size = 0;

        fseek(f, 0, SEEK_END);
        size = ftell(f);
        rewind(f);

        text = (char*)calloc(size + 1, sizeof(char));
        fread(text, sizeof(char), size, f);
        fclose(f); 
    }

    return text;
}

void load_shader(const char *vertex_source, const char *fragment_source,
        GLuint *vertex, GLuint *fragment, GLuint *program)
{
    const char * test_str = "#version ";
    char infoLog[512] = {0};
    char *shader_source = NULL;

    *vertex = glCreateShader(GL_VERTEX_SHADER);
    if(!strcmp(vertex_source, test_str)){
        glShaderSource(*vertex, 1, &vertex_source, NULL);
        glCompileShader(*vertex);
        glGetShaderInfoLog(*vertex, sizeof(infoLog), NULL, infoLog);
        printf("Vertex shader compile log: %s\n", infoLog);
    }else{
        shader_source = load_file(vertex_source);
        glShaderSource(*vertex, 1, &shader_source, NULL);
        glCompileShader(*vertex);
        glGetShaderInfoLog(*vertex, sizeof(infoLog), NULL, infoLog);
        printf("%s, compile log: %s\n", vertex_source, infoLog);
        free(shader_source);
    }

    *fragment = glCreateShader(GL_FRAGMENT_SHADER);
    if(!strcmp(fragment_source, test_str)){
        glShaderSource(*fragment, 1, &fragment_source, NULL);
        glCompileShader(*fragment);
        glGetShaderInfoLog(*fragment, sizeof(infoLog), NULL, infoLog);
        printf("Fragment shader compile log: %s\n", infoLog);
    }else{
        shader_source = load_file(fragment_source);
        glShaderSource(*fragment, 1, &shader_source, NULL);
        glCompileShader(*fragment);
        glGetShaderInfoLog(*fragment, sizeof(infoLog), NULL, infoLog);
        printf("%s, compile log: %s\n", fragment_source, infoLog);
        free(shader_source);
    }

    *program = glCreateProgram();
    glAttachShader(*program, *vertex);
    glAttachShader(*program, *fragment);
    glLinkProgram(*program);
}

GLuint vao, buf, tex, tex_norm, wall_vs, wall_fs, wall_ph;

static struct timeval init_tv;

static double tv2d(struct timeval *tv)
{
    return (double)tv->tv_sec + ((double) tv->tv_usec) / 1000000.0;
}

static float get_time(void){
    struct timeval tv = {0};

    gettimeofday(&tv, NULL);

    return tv2d(&tv) - tv2d(&init_tv);
}
void draw_opengl(char c)
{
    GLfloat a_T[3] = {0};
    GLfloat a_B[3] = {0};
    GLfloat b_T[3] = {0};
    GLfloat b_B[3] = {0};

    get_tb(a, a_T, a_B);
    get_tb(b, b_T, b_B);

    GLfloat vertices[] = {
          // Vertex                   Texture            Tangent                   Binormal
        a[0][0], a[0][1], a[0][2],  a[0][3], a[0][4],  a_T[0], a_T[1], a_T[2],  a_B[0], a_B[1], a_B[2],
        a[1][0], a[1][1], a[1][2],  a[1][3], a[1][4],  a_T[0], a_T[1], a_T[2],  a_B[0], a_B[1], a_B[2],
        a[2][0], a[2][1], a[2][2],  a[2][3], a[2][4],  a_T[0], a_T[1], a_T[2],  a_B[0], a_B[1], a_B[2],

        b[0][0], b[0][1], b[0][2],  b[0][3], b[0][4],  b_T[0], b_T[1], b_T[2],  b_B[0], b_B[1], b_B[2],
        b[1][0], b[1][1], b[1][2],  b[1][3], b[1][4],  b_T[0], b_T[1], b_T[2],  b_B[0], b_B[1], b_B[2],
        b[2][0], b[2][1], b[2][2],  b[2][3], b[2][4],  b_T[0], b_T[1], b_T[2],  b_B[0], b_B[1], b_B[2],
    };

    glGenVertexArrays(1, &vao);
    glBindVertexArray(vao);
    glGenBuffers(1, &buf);
    glBindBuffer(GL_ARRAY_BUFFER, buf);
    glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 11*sizeof(GLfloat), 0);
    glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 11*sizeof(GLfloat), (void*)(3*sizeof(GLfloat)));
    glVertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE, 11*sizeof(GLfloat), (void*)(5*sizeof(GLfloat)));
    glVertexAttribPointer(3, 3, GL_FLOAT, GL_FALSE, 11*sizeof(GLfloat), (void*)(8*sizeof(GLfloat)));
    glEnableVertexAttribArray(0);
    glEnableVertexAttribArray(1);
    glEnableVertexAttribArray(2);
    glEnableVertexAttribArray(3);

    float t = get_time();
    light_pos[0] = light_distance * cos(t);
    light_pos[1] = light_distance * sin(t);
    glUniform3fv(glGetUniformLocation(wall_ph, "light_pos"), 1, light_pos);

    if('b' == c){
        if(is_bpressed){
            glUniform1i(glGetUniformLocation(wall_ph, "enable_tex_map"), GL_FALSE);
            is_bpressed = false;
        }else{
            glUniform1i(glGetUniformLocation(wall_ph, "enable_tex_map"), GL_TRUE);
            is_bpressed = true;
        }
    }

    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glDrawArrays(GL_TRIANGLES, 0, 6);
}

int main(int argc, char **argv)
{
    const char * image = load_file("brickwall.rgb");
    const char * image_norm = load_file("brickwall_normal.rgb");

    gettimeofday(&init_tv, NULL);
    create_window(800, 600);

    glGenTextures(1, &tex);
    glBindTexture(GL_TEXTURE_2D, tex);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, 1024, 1024,
                                0, GL_RGB, GL_UNSIGNED_BYTE, image);
    glGenTextures(1, &tex_norm);
    glBindTexture(GL_TEXTURE_2D, tex_norm);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, 1024, 1024,
                                0, GL_RGB, GL_UNSIGNED_BYTE, image_norm);

    load_shader("wall.vs", "wall.fs", &wall_vs, &wall_fs, &wall_ph);

    glUseProgram(wall_ph);
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, tex);
    glUniform1i(glGetUniformLocation(wall_ph, "tex"), 0);
    glActiveTexture(GL_TEXTURE1);
    glBindTexture(GL_TEXTURE_2D, tex_norm);
    glUniform1i(glGetUniformLocation(wall_ph, "tex_norm"), 1);

    glUniform3fv(glGetUniformLocation(wall_ph, "view_pos"), 1, view_pos);
    glUniform3fv(glGetUniformLocation(wall_ph, "light_pos"), 1, light_pos);

    glUniformMatrix4fv(glGetUniformLocation(wall_ph, "proj"), 1, GL_TRUE, proj);
    glUniformMatrix4fv(glGetUniformLocation(wall_ph, "view"), 1, GL_TRUE, view);
    glUniformMatrix4fv(glGetUniformLocation(wall_ph, "model"), 1, GL_TRUE, model);

    glUniform1i(glGetUniformLocation(wall_ph, "enable_tex_map"), GL_FALSE);

    glEnable(GL_DEPTH_TEST);
    window_loop();

    return 0;
}

