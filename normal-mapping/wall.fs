#version 330 core

in VS_OUT{
    vec3 coord;
    vec2 tcoord;
    vec3 view_pos;
    vec3 light_pos;
}vs_out;

uniform sampler2D tex;
uniform sampler2D tex_norm;
uniform bool enable_tex_map;

out vec4 FragColor;
void main(){
    vec3 color = texture(tex, vs_out.tcoord).rgb;
    vec3 norm = texture(tex_norm, vs_out.tcoord).rgb;
    vec3 L = normalize(vs_out.light_pos - vs_out.coord);
    vec3 V = normalize(vs_out.view_pos - vs_out.coord);
    vec3 H = normalize(V + L);
    vec3 N = vec3(0.0);

    if(enable_tex_map){
        N = normalize(norm*2.0 - 1.0);
    }else{
        N = vec3(0.0, 1.0, 0.0); 
    }

    float Kamb = 0.2;
    float Iamb = Kamb;

    float Kdiff = 0.5;
    float Idiff = Kdiff*max(dot(L, N), 0.0);

    float Kspec = 1.0;
    float Ispec = Kspec*pow(max(dot(H, N), 0.0), 16.0);

    FragColor = vec4((Iamb + Idiff + Ispec)*color, 1.0);
}
