#include <stdlib.h>
#include <stdio.h>

#include <GLES3/gl31.h>
#include <GL/gl.h>

void create_window(int width, int height);
void window_loop();

GLfloat vs[] = {
    // Vertices         Texture
    -1.0,  1.0,  1.0,   0.0, 0.0,
    -1.0, -1.0,  1.0,   0.0, 1.0,
     1.0, -1.0,  1.0,   1.0, 1.0,

     1.0, -1.0,  1.0,   1.0, 1.0,
     1.0,  1.0,  1.0,   1.0, 0.0,
    -1.0,  1.0,  1.0,   0.0, 0.0,
};

GLfloat fb_vs[] = {
    -1.0,  1.0,  1.0,   0.0,  1.0,
    -1.0, -1.0,  1.0,   0.0,  0.0,
     1.0, -1.0,  1.0,   1.0,  0.0,

    -1.0,  1.0,  1.0,   0.0,  1.0,
     1.0, -1.0,  1.0,   1.0,  0.0,
     1.0,  1.0,  1.0,   1.0,  1.0,
};

const char *vsh = 
"#version 100\n"
"attribute vec3 pos;"
"attribute vec2 tpos;"
"varying vec2 tp;"
"void main(){"
" tp = tpos;"
" gl_Position = vec4(pos, 1.0);"
"}";

const char *fsh = 
"#version 100\n"
"precision mediump float;"
"varying vec2 tp;"
"uniform sampler2D tex;"
"void main(){"
" gl_FragColor = texture2D(tex, tp);"
"}";

// Color invert shader
//const char *fb_fsh = 
//"#version 100\n"
//"precision mediump float;"
//"varying vec2 tp;"
//"uniform sampler2D screen_tex;"
//"void main(){"
//" gl_FragColor = vec4(vec3(1.0) - texture2D(screen_tex, tp).rgb, 1.0);"
//"}";

// Sharpen
const char *fb_fsh = 
"#version 100\n"
"precision mediump float;"
"varying vec2 tp;"
"uniform sampler2D screen_tex;"
"const float offset = 1.0 / 500.0;"
"void main(){"
" vec2 offsets[9];"
" offsets[0] = vec2(-offset, offset);"
" offsets[1] = vec2(0.0,     offset);"
" offsets[2] = vec2(offset,  offset);"
" offsets[3] = vec2(-offset, 0.0);"
" offsets[4] = vec2(0.0,     0.0);"
" offsets[5] = vec2(offset,  0.0);"
" offsets[6] = vec2(-offset, -offset);"
" offsets[7] = vec2(0.0,     -offset);"
" offsets[8] = vec2(offset,  -offset);"
" float kernel[9];"
" kernel[0] = -1.0;"
" kernel[1] = -1.0;"
" kernel[2] = -1.0;"
" kernel[3] = -1.0;"
" kernel[4] = 9.0;"
" kernel[5] = -1.0;"
" kernel[6] = -1.0;"
" kernel[7] = -1.0;"
" kernel[8] = -1.0;"
" vec3 col = vec3(0.0);"
" for(int i = 0; i < 9; i++)"
"   col += kernel[i] * vec3(texture2D(screen_tex, tp.st + offsets[i]));"
" gl_FragColor = vec4(col, 1.0);"
"}";

// Blur
//const char *fb_fsh = 
//"#version 100\n"
//"precision mediump float;"
//"varying vec2 tp;"
//"uniform sampler2D screen_tex;"
//"const float offset = 1.0 / 700.0;"
//"void main(){"
//" vec2 offsets[9];"
//" offsets[0] = vec2(-offset, offset);"
//" offsets[1] = vec2(0.0,     offset);"
//" offsets[2] = vec2(offset,  offset);"
//" offsets[3] = vec2(-offset, 0.0);"
//" offsets[4] = vec2(0.0,     0.0);"
//" offsets[5] = vec2(offset,  0.0);"
//" offsets[6] = vec2(-offset, -offset);"
//" offsets[7] = vec2(0.0,     -offset);"
//" offsets[8] = vec2(offset,  -offset);"
//" float kernel[9];"
//" kernel[0] = 1.0 / 16.0;"
//" kernel[1] = 2.0 / 16.0;"
//" kernel[2] = 1.0 / 16.0;"
//" kernel[3] = 2.0 / 16.0;"
//" kernel[4] = 4.0 / 16.0;"
//" kernel[5] = 2.0 / 16.0;"
//" kernel[6] = 1.0 / 16.0;"
//" kernel[7] = 2.0 / 16.0;"
//" kernel[8] = 1.0 / 16.0;"
//" vec3 col = vec3(0.0);"
//" for(int i = 0; i < 9; i++)"
//"   col += kernel[i] * vec3(texture2D(screen_tex, tp.st + offsets[i]));"
//" gl_FragColor = vec4(col, 1.0);"
//"}";

GLuint vao, buf, vh, fh, ph, tx;
GLuint fb, fb_vao, fb_buf, fb_vh, fb_fh, fb_ph, fb_tx;

const int width = 800;
const int height = 600;

void draw_opengl()
{
    glBindFramebuffer(GL_FRAMEBUFFER, fb);
    glBindVertexArray(vao);
    glBindTexture(GL_TEXTURE_2D, tx);
    glClear(GL_COLOR_BUFFER_BIT);

    glUseProgram(ph);
    glDrawArrays(GL_TRIANGLES, 0, 6);

    glBindVertexArray(fb_vao);
    glBindFramebuffer(GL_FRAMEBUFFER, 0);
    glBindTexture(GL_TEXTURE_2D, fb_tx);
    glClear(GL_COLOR_BUFFER_BIT);
    glUseProgram(fb_ph);
    glDrawArrays(GL_TRIANGLES, 0, 6);
}

int main(int argc, char **argv)
{
    char infoLog[512] = {0};

    FILE *f = fopen("texture.rgba", "rb");
    unsigned char *fdata;
    size_t fsize;

    fseek(f, 0, SEEK_END);
    fsize = ftell(f);
    rewind(f);
    
    fdata = (unsigned char*)malloc(fsize);
    fread(fdata, sizeof(unsigned char), fsize, f);
    fclose(f);

    create_window(800, 600);

    glGenVertexArrays(1, &vao);
    glBindVertexArray(vao);
    glGenBuffers(1, &buf);
    glBindBuffer(GL_ARRAY_BUFFER, buf);
    glBufferData(GL_ARRAY_BUFFER, sizeof(vs), vs, GL_STATIC_DRAW);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 5*sizeof(GLfloat), 0);
    glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 5*sizeof(GLfloat), (void*)(3*sizeof(GLfloat)));
    glEnableVertexAttribArray(0);
    glEnableVertexAttribArray(1);

    glGenTextures(1, &tx);
    glBindTexture(GL_TEXTURE_2D, tx);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height,
                                0, GL_RGBA, GL_UNSIGNED_BYTE, fdata);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

    vh = glCreateShader(GL_VERTEX_SHADER);
    glShaderSource(vh, 1, &vsh, NULL);
    glCompileShader(vh);

    fh = glCreateShader(GL_FRAGMENT_SHADER);
    glShaderSource(fh, 1, &fsh, NULL);
    glCompileShader(fh);

    ph = glCreateProgram();
    glAttachShader(ph, vh);
    glAttachShader(ph, fh);
    glLinkProgram(ph);

    // Framebuffer details
    glGenFramebuffers(1, &fb);
    glBindFramebuffer(GL_FRAMEBUFFER, fb);

    glGenVertexArrays(1, &fb_vao);
    glBindVertexArray(fb_vao);
    glGenBuffers(1, &fb_buf);
    glBindBuffer(GL_ARRAY_BUFFER, fb_buf);
    glBufferData(GL_ARRAY_BUFFER, sizeof(fb_vs), fb_vs, GL_STATIC_DRAW);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 5*sizeof(GLfloat), 0);
    glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 5*sizeof(GLfloat), (void*)(3*sizeof(GLfloat)));
    glEnableVertexAttribArray(0);
    glEnableVertexAttribArray(1);

    glGenTextures(1, &fb_tx);
    glBindTexture(GL_TEXTURE_2D, fb_tx);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height,
                                0, GL_RGBA, GL_UNSIGNED_BYTE, NULL);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, fb_tx, 0);

    fb_vh = glCreateShader(GL_VERTEX_SHADER);
    glShaderSource(fb_vh, 1, &vsh, NULL);
    glCompileShader(fb_vh);

    fb_fh = glCreateShader(GL_FRAGMENT_SHADER);
    glShaderSource(fb_fh, 1, &fb_fsh, NULL);
    glCompileShader(fb_fh);
    glGetShaderInfoLog(fb_fh, sizeof(infoLog), 0, infoLog);
    printf("Shader log: %s\n", infoLog);

    fb_ph = glCreateProgram();
    glAttachShader(fb_ph, fb_vh);
    glAttachShader(fb_ph, fb_fh);
    glLinkProgram(fb_ph);

    window_loop();

    return 0;
}
