#include <stdio.h>
#include <GLES3/gl31.h>
#include <GL/gl.h>

const char *vsh = 
"#version 330 core\n"
"layout (location = 0) in vec2 vpos;"
"layout (location = 1) in vec3 vcol;"
"out vec3 fcol;"
"const int raw_items = 8;"
"void main(){"
" const vec2 init = vec2(-0.8, 0.8);"
" const float step = 1.6 / raw_items;"
" vec2 pos;"
" pos.x = vpos.x + init.x + step*(gl_InstanceID % raw_items);"
" pos.y = vpos.y + init.y - step*(gl_InstanceID / raw_items);"
" gl_Position = vec4(pos, 1.0, 1.0);"
" fcol = vcol;"
"}";
const char *fsh = 
"#version 330 core\n"
"in vec3 fcol;"
"out vec4 FragColor;"
"void main(){"
" FragColor = vec4(fcol, 1.0);"
"}";

GLfloat vxs[] = {
    -0.1, -0.1, 1.0, 0.0, 0.0,
     0.1, -0.1, 0.0, 1.0, 0.0,
     0.1,  0.1, 0.0, 0.0, 1.0,
};

extern void create_window(int width, int height);
extern void window_loop(void);

GLuint vao, buf, vh, fh, ph;

void draw_opengl(void)
{
    glClear(GL_COLOR_BUFFER_BIT);

    glDrawArraysInstanced(GL_TRIANGLES, 0, 3, 1000);
}

int main(int argc, char **argv)
{
    char infoLog[512] = {0};
    create_window(800, 600);

    glGenVertexArrays(1, &vao); 
    glBindVertexArray(vao);
    glGenBuffers(1, &buf);
    glBindBuffer(GL_ARRAY_BUFFER, buf);
    glBufferData(GL_ARRAY_BUFFER, sizeof(vxs), vxs, GL_STATIC_DRAW);
    glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 5*sizeof(GLfloat), (void*)0);
    glEnableVertexAttribArray(0);
    glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 5*sizeof(GLfloat), (void*)(2*sizeof(GLfloat)));
    glEnableVertexAttribArray(1);

    vh = glCreateShader(GL_VERTEX_SHADER);
    glShaderSource(vh, 1, &vsh, 0);
    glCompileShader(vh);
    glGetShaderInfoLog(vh, sizeof(infoLog), 0, infoLog);
    printf("Vertex shader log: %s\n", infoLog);
    fh = glCreateShader(GL_FRAGMENT_SHADER);
    glShaderSource(fh, 1, &fsh, 0);
    glCompileShader(fh);
    glGetShaderInfoLog(fh, sizeof(infoLog), 0, infoLog);
    printf("Fragment shader log: %s\n", infoLog);
    ph = glCreateProgram();
    glAttachShader(ph, vh);
    glAttachShader(ph, fh);
    glLinkProgram(ph);
    glUseProgram(ph);

    window_loop();

    return 0;
}


