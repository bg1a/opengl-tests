#version 330 core
layout (location = 0) in vec3 coord;
layout (location = 1) in vec3 norm;
layout (location = 2) in vec2 tcoord;

out VS_OUT{
    vec3 norm;
    vec3 coord;
    vec2 tcoord;
}vs_out;

uniform mat4 proj;
uniform mat4 view;
uniform mat4 model;

void main(){
    gl_Position = proj*inverse(view)*model*vec4(coord, 1.0);

    vs_out.norm = vec3(transpose(inverse(model))*vec4(norm, 1.0));
    vs_out.coord = vec3(model*vec4(coord, 1.0));
    vs_out.tcoord = tcoord;
}
