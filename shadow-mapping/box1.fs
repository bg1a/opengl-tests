#version 330 core

in VS_OUT{
    vec3 norm;
    vec3 coord;
    vec2 tcoord;
}vs_out;

out vec4 FragColor;
uniform sampler2D tex;
uniform sampler2D shadow_tex;
uniform vec3 light_coord;
uniform vec3 view_coord;

void main(){
    vec3 color = texture(tex, vs_out.tcoord).rgb;
    vec3 I = normalize(vs_out.coord - light_coord);
    vec3 N = normalize(vs_out.norm);
    vec3 R = reflect(I, N);
    vec3 V = normalize(view_coord - vs_out.coord);

    float Kamb = 0.2;
    float Iamb = Kamb;

    float Kdiff = 0.5;
    float Idiff = Kdiff*max(dot(-I, N), 0.0); 

    float Kspec = 1.0;
    float Ispec = Kspec*pow(max(dot(R, V), 0.0), 8.0);

    FragColor = vec4((Iamb + Idiff + Ispec) * color, 1.0);
}
