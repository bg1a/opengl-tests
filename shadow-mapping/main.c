#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>

#include <GLES3/gl31.h>
#include <GL/gl.h>

const GLfloat zn = 0.1;
const GLfloat zf = 100.0;
GLfloat proj[] = {
  1.0, 0.0, 0.0,                    0.0,
  0.0, 1.0, 0.0,                    0.0,    
  0.0, 0.0, -(zn + zf)/(zn - zf),   2*zn*zf/(zn - zf),
  0.0, 0.0, 1.0,                    0.0,
};
GLfloat view_coord[] = {
    // X    Y    Z
      0.0, 1.0, 10.0,
};
GLfloat light_coord[] = {
    // X    Y    Z
     0.0,  5.0, 0.0,
};
GLfloat view_common[] = {
  1.0, 0.0,  0.0, view_coord[0],
  0.0, 1.0,  0.0, view_coord[1],
  0.0, 0.0, -1.0, view_coord[2],
  0.0, 0.0,  0.0, 1.0,
};
GLfloat view_light[] = {
  1.0,  0.0,    0.0,   light_coord[0],
  0.0,  0.0,   -1.0,   light_coord[1],
  0.0,  1.0,    0.0,   light_coord[2],
  0.0,  0.0,    0.0,   1.0,
};
GLfloat model_common[] = {
  1.0, 0.0, 0.0, 0.0,
  0.0, 1.0, 0.0, 0.0,
  0.0, 0.0, 1.0, 0.0,
  0.0, 0.0, 0.0, 1.0,
};

const float alpha = 3.14 / 6.0;
GLfloat model_rotate[] = {
  cos(alpha),               -sin(alpha),             0.0,        0.0,
  cos(alpha)*sin(alpha),     cos(alpha)*cos(alpha), -sin(alpha), 0.0,
  sin(alpha)*sin(alpha),     sin(alpha)*cos(alpha),  cos(alpha), 0.0,
  0.0,                       0.0,                    0.0,        1.0,
};

GLfloat box1_vertices [] = {
    // Vertices           Normal             Texture
    
    // Face
    -1.0,  1.0,  1.0,    0.0,  0.0,  1.0,    0.0, 1.0,
    -1.0, -1.0,  1.0,    0.0,  0.0,  1.0,    0.0, 0.0,
     1.0, -1.0,  1.0,    0.0,  0.0,  1.0,    1.0, 0.0,
    -1.0,  1.0,  1.0,    0.0,  0.0,  1.0,    0.0, 1.0,
     1.0, -1.0,  1.0,    0.0,  0.0,  1.0,    1.0, 0.0,
     1.0,  1.0,  1.0,    0.0,  0.0,  1.0,    1.0, 1.0,

    // Back
     1.0,  1.0, -1.0,    0.0,  0.0, -1.0,    0.0, 1.0,
     1.0, -1.0, -1.0,    0.0,  0.0, -1.0,    0.0, 0.0,
    -1.0, -1.0, -1.0,    0.0,  0.0, -1.0,    1.0, 0.0,
     1.0,  1.0, -1.0,    0.0,  0.0, -1.0,    0.0, 1.0,
    -1.0, -1.0, -1.0,    0.0,  0.0, -1.0,    1.0, 0.0,
    -1.0,  1.0, -1.0,    0.0,  0.0, -1.0,    1.0, 1.0,

    // Top
    -1.0,  1.0, -1.0,    0.0,  1.0,  0.0,    0.0, 1.0,
    -1.0,  1.0,  1.0,    0.0,  1.0,  0.0,    0.0, 0.0,
     1.0,  1.0,  1.0,    0.0,  1.0,  0.0,    1.0, 0.0,
    -1.0,  1.0, -1.0,    0.0,  1.0,  0.0,    0.0, 1.0,
     1.0,  1.0,  1.0,    0.0,  1.0,  0.0,    1.0, 0.0,
     1.0,  1.0, -1.0,    0.0,  1.0,  0.0,    1.0, 1.0,
                                                      
    // Bottom                                         
    -1.0, -1.0,  1.0,    0.0, -1.0,  0.0,    0.0, 1.0,
    -1.0, -1.0, -1.0,    0.0, -1.0,  0.0,    0.0, 0.0,
     1.0, -1.0, -1.0,    0.0, -1.0,  0.0,    1.0, 0.0,
    -1.0, -1.0,  1.0,    0.0, -1.0,  0.0,    0.0, 1.0,
     1.0, -1.0, -1.0,    0.0, -1.0,  0.0,    1.0, 0.0,
     1.0, -1.0,  1.0,    0.0, -1.0,  0.0,    1.0, 1.0,

    // Left
    -1.0,  1.0, -1.0,   -1.0,  0.0,  0.0,    0.0, 1.0,
    -1.0, -1.0, -1.0,   -1.0,  0.0,  0.0,    0.0, 0.0,
    -1.0, -1.0,  1.0,   -1.0,  0.0,  0.0,    1.0, 0.0,
    -1.0,  1.0, -1.0,   -1.0,  0.0,  0.0,    0.0, 1.0,
    -1.0, -1.0,  1.0,   -1.0,  0.0,  0.0,    1.0, 0.0,
    -1.0,  1.0,  1.0,   -1.0,  0.0,  0.0,    1.0, 1.0,
                                                      
    // Right                                          
     1.0,  1.0,  1.0,    1.0,  0.0,  0.0,    0.0, 1.0,
     1.0, -1.0,  1.0,    1.0,  0.0,  0.0,    0.0, 0.0,
     1.0, -1.0, -1.0,    1.0,  0.0,  0.0,    1.0, 0.0,
     1.0,  1.0,  1.0,    1.0,  0.0,  0.0,    0.0, 1.0,
     1.0, -1.0, -1.0,    1.0,  0.0,  0.0,    1.0, 0.0,
     1.0,  1.0, -1.0,    1.0,  0.0,  0.0,    1.0, 1.0,
};

GLfloat floor_vertices [] = {
    // vertices          normal          texture 
   -10.0, -4.0,  10.0,  0.0, 1.0, 0.0,  0.0, 0.0,
    10.0, -4.0,  10.0,  0.0, 1.0, 0.0,  1.0, 0.0,
    10.0, -4.0, -10.0,  0.0, 1.0, 0.0,  1.0, 1.0,

   -10.0, -4.0,  10.0,  0.0, 1.0, 0.0,  0.0, 0.0,
    10.0, -4.0, -10.0,  0.0, 1.0, 0.0,  1.0, 1.0,
   -10.0, -4.0, -10.0,  0.0, 1.0, 0.0,  0.0, 1.0,
};

GLfloat debug_vertices [] = {
    // vertices       texture 
   -1.0, -1.0,  0.0,  0.0, 0.0,
    1.0, -1.0,  0.0,  1.0, 0.0,
    1.0,  1.0,  0.0,  1.0, 1.0,

   -1.0, -1.0,  0.0,  0.0, 0.0,
    1.0,  1.0,  0.0,  1.0, 1.0,
   -1.0,  1.0,  0.0,  0.0, 1.0,
};

const char *light_fsh = 
"#version 330 core\n"
"void main(){"
"}";

const char *debug_vsh = 
"#version 330 core\n"
"layout (location = 0) in vec3 coord;"
"layout (location = 1) in vec2 tex_coord;"
"out vec2 tcoord;"
"void main(){"
" gl_Position = vec4(coord, 1.0);"
" tcoord = tex_coord;"
"}";

const char *debug_fsh = 
"#version 330 core\n"
"in vec2 tcoord;"
"out vec4 FragColor;"
"uniform sampler2D tex;"
"void main(){"
" float zn = 0.1;"
" float zf = 100.0;"
" float depth = texture(tex, tcoord).r;"
" float n = 2*zn*zf/(zn - zf);"
" float m = (zn + zf)/(zn - zf);"
" float z = depth*2.0 - 1.0;"
" float linear_depth = n / (z + m);"
" FragColor = vec4(vec3(linear_depth/10.0), 1.0);"
"}";

extern void window_loop(void);
extern void create_window(int w, int h);

char *wood_img;

GLuint box1_vao, box1_buf, box1_tex, box1_vs, box1_fs, box1_ph;
GLuint light_box1_vs, light_box1_fs, light_box1_ph;

GLuint floor_vao, floor_buf, floor_tex, floor_vs, floor_fs, floor_ph;
GLuint light_floor_vs, light_floor_fs, light_floor_ph;

GLuint depth_fbo, depth_tex;

GLuint debug_vao, debug_buf, debug_vs, debug_fs, debug_ph;

void draw_opengl(void){
    glViewport(0, 0, 1024, 1024);
    // Get shadow depth component
    glBindFramebuffer(GL_FRAMEBUFFER, depth_fbo);
    glClear(GL_DEPTH_BUFFER_BIT);

    glBindVertexArray(floor_vao);
    glUseProgram(light_floor_ph);
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, floor_tex);
    glUniform1i(glGetUniformLocation(light_floor_ph, "tex"), 0);
    glDrawArrays(GL_TRIANGLES, 0, 6);

    glBindVertexArray(box1_vao);
    glUseProgram(light_box1_ph);
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, box1_tex);
    glUniform1i(glGetUniformLocation(light_box1_ph, "tex"), 0);
    glDrawArrays(GL_TRIANGLES, 0, 36);

    glBindFramebuffer(GL_FRAMEBUFFER, 0);

#if 0
    // Draw debug
    glViewport(0, 0, 800, 600);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glBindVertexArray(debug_vao);
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, depth_tex);
    glUseProgram(debug_ph);
    glUniform1i(glGetUniformLocation(debug_ph, "tex"), 0);
    glDrawArrays(GL_TRIANGLES, 0, 6);

#else
    // Draw scene
    glViewport(0, 0, 800, 600);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glClearColor(0.2, 0.2, 0.2, 0.2);

    glBindVertexArray(floor_vao);
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, floor_tex);
    glActiveTexture(GL_TEXTURE1);
    glBindTexture(GL_TEXTURE_2D, depth_tex);
    glUseProgram(floor_ph);
    glUniform1i(glGetUniformLocation(floor_ph, "tex"), 0);
    glUniform1i(glGetUniformLocation(floor_ph, "shadow_tex"), 1);
    glDrawArrays(GL_TRIANGLES, 0, 6);

    glBindVertexArray(box1_vao);
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, box1_tex);
    glActiveTexture(GL_TEXTURE1);
    glBindTexture(GL_TEXTURE_2D, depth_tex);
    glUseProgram(box1_ph);
    glUniform1i(glGetUniformLocation(box1_ph, "tex"), 0);
    glUniform1i(glGetUniformLocation(box1_ph, "shadow_tex"), 1);
    glDrawArrays(GL_TRIANGLES, 0, 36);

#endif
}

char *load_file(const char *filename)
{
    FILE *f = fopen(filename, "r");
    char *text = NULL;

    if(f){
        size_t size = 0;

        fseek(f, 0, SEEK_END);
        size = ftell(f);
        rewind(f);

        text = (char*)calloc(size + 1, sizeof(char));
        fread(text, sizeof(char), size, f);
        fclose(f); 
    }

    return text;
}

void load_shader(const char *vertex_source, const char *fragment_source,
        GLuint *vertex, GLuint *fragment, GLuint *program)
{
    const char * test_str = "#version 330 core\n";
    char infoLog[512] = {0};
    char *shader_source = NULL;

    *vertex = glCreateShader(GL_VERTEX_SHADER);
    if(strlen(vertex_source) <= strlen(test_str)){
        shader_source = load_file(vertex_source);
        glShaderSource(*vertex, 1, &shader_source, NULL);
        glCompileShader(*vertex);
        glGetShaderInfoLog(*vertex, sizeof(infoLog), NULL, infoLog);
        printf("%s, compile log: %s\n", vertex_source, infoLog);
        free(shader_source);
    }else{
        glShaderSource(*vertex, 1, &vertex_source, NULL);
        glCompileShader(*vertex);
        glGetShaderInfoLog(*vertex, sizeof(infoLog), NULL, infoLog);
        printf("Vertex shader compile log: %s\n", infoLog);
    }

    *fragment = glCreateShader(GL_FRAGMENT_SHADER);
    if(strlen(fragment_source) <= strlen(test_str)){
        shader_source = load_file(fragment_source);
        glShaderSource(*fragment, 1, &shader_source, NULL);
        glCompileShader(*fragment);
        glGetShaderInfoLog(*fragment, sizeof(infoLog), NULL, infoLog);
        printf("%s, compile log: %s\n", fragment_source, infoLog);
        free(shader_source);
    }else{
        glShaderSource(*fragment, 1, &fragment_source, NULL);
        glCompileShader(*fragment);
        glGetShaderInfoLog(*fragment, sizeof(infoLog), NULL, infoLog);
        printf("Fragment shader compile log: %s\n", infoLog);
    }

    *program = glCreateProgram();
    glAttachShader(*program, *vertex);
    glAttachShader(*program, *fragment);
    glLinkProgram(*program);
}

int main(int argc, char **argv)
{
    create_window(800, 600);

    wood_img = load_file("wood.rgb");

    // Box #1 vertices
    glGenVertexArrays(1, &box1_vao);
    glBindVertexArray(box1_vao);
    glGenBuffers(1, &box1_buf);
    glBindBuffer(GL_ARRAY_BUFFER, box1_buf);
    glBufferData(GL_ARRAY_BUFFER, sizeof(box1_vertices), box1_vertices, GL_STATIC_DRAW);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 8*sizeof(GLfloat), (void*)0);
    glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 8*sizeof(GLfloat), (void*)(3*sizeof(GLfloat)));
    glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, 8*sizeof(GLfloat), (void*)(6*sizeof(GLfloat)));
    glEnableVertexAttribArray(0);
    glEnableVertexAttribArray(1);
    glEnableVertexAttribArray(2);
    glGenTextures(1, &box1_tex);
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, box1_tex);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, 1024, 1024,
                                0, GL_RGB, GL_UNSIGNED_BYTE, wood_img);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

    load_shader("box1.vs", "box1.fs", &box1_vs, &box1_fs, &box1_ph);
    glUseProgram(box1_ph);
    glUniformMatrix4fv(glGetUniformLocation(box1_ph, "proj"), 1, GL_TRUE, proj);
    glUniformMatrix4fv(glGetUniformLocation(box1_ph, "view"), 1, GL_TRUE, view_common);
    glUniformMatrix4fv(glGetUniformLocation(box1_ph, "model"), 1, GL_TRUE, model_rotate);
    glUniform1i(glGetUniformLocation(box1_ph, "tex"), 0);
    glUniform3f(glGetUniformLocation(box1_ph, "view_coord"),
            view_coord[0], view_coord[1], view_coord[2]);
    glUniform3f(glGetUniformLocation(box1_ph, "light_coord"),
            light_coord[0], light_coord[1], light_coord[2]);

    // Floor vertices
    glGenVertexArrays(1, &floor_vao);
    glBindVertexArray(floor_vao);
    glGenBuffers(1, &floor_buf);
    glBindBuffer(GL_ARRAY_BUFFER, floor_buf);
    glBufferData(GL_ARRAY_BUFFER, sizeof(floor_vertices), floor_vertices, GL_STATIC_DRAW);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 8*sizeof(GLfloat), (void*)0);
    glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 8*sizeof(GLfloat), (void*)(3*sizeof(GLfloat)));
    glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, 8*sizeof(GLfloat), (void*)(6*sizeof(GLfloat)));
    glEnableVertexAttribArray(0);
    glEnableVertexAttribArray(1);
    glEnableVertexAttribArray(2);

    glGenTextures(1, &floor_tex);
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, floor_tex);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, 1024, 1024,
                                0, GL_RGB, GL_UNSIGNED_BYTE, wood_img);

    load_shader("floor.vs", "floor.fs", &floor_vs, &floor_fs, &floor_ph);
    glUseProgram(floor_ph);
    glUniformMatrix4fv(glGetUniformLocation(floor_ph, "proj"), 1, GL_TRUE, proj);
    glUniformMatrix4fv(glGetUniformLocation(floor_ph, "view"), 1, GL_TRUE, view_common);
    glUniformMatrix4fv(glGetUniformLocation(floor_ph, "model"), 1, GL_TRUE, model_common);
    glUniformMatrix4fv(glGetUniformLocation(floor_ph, "light_proj"), 1, GL_TRUE, proj);
    glUniformMatrix4fv(glGetUniformLocation(floor_ph, "light_view"), 1, GL_TRUE, view_light);
    glUniformMatrix4fv(glGetUniformLocation(floor_ph, "light_model"), 1, GL_TRUE, model_common);
    glUniform1i(glGetUniformLocation(floor_ph, "tex"), 0);
    glUniform3f(glGetUniformLocation(floor_ph, "view_coord"),
            view_coord[0], view_coord[1], view_coord[2]);
    glUniform3f(glGetUniformLocation(floor_ph, "light_coord"),
            light_coord[0], light_coord[1], light_coord[2]);

    // Light source shaders
    load_shader("box1.vs", light_fsh, &light_box1_vs, &light_box1_fs, &light_box1_ph);
    glUseProgram(light_box1_ph);
    glUniformMatrix4fv(glGetUniformLocation(light_box1_ph, "proj"), 1, GL_TRUE, proj);
    glUniformMatrix4fv(glGetUniformLocation(light_box1_ph, "view"), 1, GL_TRUE, view_light);
    glUniformMatrix4fv(glGetUniformLocation(light_box1_ph, "model"), 1, GL_TRUE, model_rotate);

    load_shader("floor.vs", light_fsh, &light_floor_vs, &light_floor_fs, &light_floor_ph);
    glUseProgram(light_floor_ph);
    glUniformMatrix4fv(glGetUniformLocation(light_floor_ph, "proj"), 1, GL_TRUE, proj);
    glUniformMatrix4fv(glGetUniformLocation(light_floor_ph, "view"), 1, GL_TRUE, view_light);
    glUniformMatrix4fv(glGetUniformLocation(light_floor_ph, "model"), 1, GL_TRUE, model_common);

    // Debug 
    glGenVertexArrays(1, &debug_vao);
    glBindVertexArray(debug_vao);
    glGenBuffers(1, &debug_buf);
    glBindBuffer(GL_ARRAY_BUFFER, debug_buf);
    glBufferData(GL_ARRAY_BUFFER, sizeof(debug_vertices), debug_vertices, GL_STATIC_DRAW);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 5*sizeof(GLfloat), 0);
    glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 5*sizeof(GLfloat), (void*)(3*sizeof(GLfloat)));
    glEnableVertexAttribArray(0);
    glEnableVertexAttribArray(1);

    load_shader(debug_vsh, debug_fsh, &debug_vs, &debug_fs, &debug_ph);

    // Depth framebuffer
    glGenFramebuffers(1, &depth_fbo);
    glBindFramebuffer(GL_FRAMEBUFFER, depth_fbo);
    glGenTextures(1, &depth_tex);
    glBindTexture(GL_TEXTURE_2D, depth_tex);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT, 1024, 1024,
                                   0, GL_DEPTH_COMPONENT, GL_FLOAT, NULL);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
    glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D, depth_tex, 0);
    glDrawBuffer(GL_NONE);
    glReadBuffer(GL_NONE);
    glBindFramebuffer(GL_FRAMEBUFFER, 0);

    glEnable(GL_DEPTH_TEST);
    window_loop();

    return 0;
}


