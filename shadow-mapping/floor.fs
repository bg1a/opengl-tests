#version 330 core

in VS_OUT{
    vec3 norm;
    vec3 coord;
    vec2 tcoord;
}vs_out;

out vec4 FragColor;

uniform sampler2D tex;
uniform sampler2D shadow_tex;
uniform vec3 light_coord;
uniform vec3 view_coord;

uniform mat4 light_proj;
uniform mat4 light_view;
uniform mat4 light_model;

vec3 get_light_space_coord(vec3 coord){
    vec4 light_space_coord = light_proj*inverse(light_view)*light_model*vec4(coord, 1.0);

    return (light_space_coord.xyz / light_space_coord.w) * 0.5 + 0.5;
}

void main(){
    vec3 color = texture(tex, vs_out.tcoord).rgb;
    vec3 light_space_coord = get_light_space_coord(vs_out.coord);
    vec3 I = normalize(vs_out.coord - light_coord);
    vec3 N = normalize(vs_out.norm);
    vec3 R = reflect(I, N);
    vec3 V = normalize(view_coord - vs_out.coord);
    
    float Kamb = 0.15;
    float Iamb = Kamb;

    float Kdiff = 0.5;
    float Idiff = Kdiff*max(dot(-I, N), 0.0);

    float Kspec = 1.0;
    float Ispec = Kspec*pow(max(dot(R, V), 0.0), 1.0); 

    float shadow = 0.0;
    vec2 texel_sz = 1.0 / textureSize(shadow_tex, 0);
    for(int x = -1; x <= 1; ++x){
        for(int y = -1; y <= 1; ++y){
            float depth = texture(shadow_tex, light_space_coord.xy + vec2(x, y) * texel_sz).r;
            shadow += (light_space_coord.z - 0.005) > depth ? 1.0 : 0.0;
        }
    }
    shadow /= 9.0;

    FragColor = vec4((Iamb + (1.0 - shadow) * (Idiff + Ispec)) * color, 1.0);
}
