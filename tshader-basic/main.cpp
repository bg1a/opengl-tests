#include <stdlib.h>
#include <stdio.h>

#include "glad.h"
#define GLFW_INCLUDE_NONE
#include <GLFW/glfw3.h>

GLfloat vs[] = 
{
    -0.5,  0.5,
     0.5,  0.5,
     0.5, -0.5, 
    -0.5, -0.5,    
};

const char *vsh = 
"#version 420 core\n"
"layout (location=0) in vec2 vpos;"
"out vec4 position;"
"void main(){"
" gl_Position = vec4(vpos, 0.0, 1.0);"
"}";

const char *tcsh = 
"#version 420\n"
"layout (vertices = 16) out;"
"void main(){"
"  gl_TessLevelInner[0] = 2;"
"  gl_TessLevelInner[1] = 2;"
"  gl_TessLevelOuter[0] = 4;"
"  gl_TessLevelOuter[1] = 4;"
"  gl_TessLevelOuter[2] = 4;"
"  gl_TessLevelOuter[3] = 4;"
"  gl_out[gl_InvocationID].gl_Position = gl_in[gl_InvocationID].gl_Position;"
"}";

const char *tesh = 
"#version 440 core\n"
"layout (quads,  equal_spacing, ccw) in;"
"out vec4 color;"
"void main(void){"
"    gl_Position = vec4(gl_TessCoord.xy - vec2(0.5, 0.5), 1.0, 1.0);"
"    color = vec4(gl_TessCoord.xy, 0.0, 1.0);"
"}";

const char *fsh = 
"#version 420 core\n"
"in vec4 color;"
"out vec4 FragColor;"
"void main(){"
" FragColor = color;"
"}";
GLuint vao, buf, vh, fh, tch, teh, ph;
GLFWwindow* window;
void draw_opengl(void)
{
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glClearColor(0.02, 0.0, 0.02, 1.0);

    glDrawArrays(GL_PATCHES, 0, 4);
}

int main(int argc, char **argv)
{
    GLchar infoLog[512] = {0};

    glfwInit();
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 6);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
    if(!(window = glfwCreateWindow(800, 600, "LearnOpenGL", NULL, NULL))){
        printf("Failed to create a window!\n");
        return -1;
    }
    glfwMakeContextCurrent(window);

    gladLoadGL();
    glfwSwapInterval(1);

    glGenVertexArrays(1, &vao);
    glBindVertexArray(vao);
    glGenBuffers(1, &buf);
    glBindBuffer(GL_ARRAY_BUFFER, buf);
    glBufferData(GL_ARRAY_BUFFER, sizeof(vs), vs, GL_STATIC_DRAW);
    glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 2*sizeof(GLfloat), 0);
    glEnableVertexAttribArray(0);

    vh = glCreateShader(GL_VERTEX_SHADER);
    glShaderSource(vh, 1, &vsh, NULL);
    glCompileShader(vh);
    glGetShaderInfoLog(vh, 512, NULL, infoLog);
    printf("Vertex shader log: %s\n", infoLog);

    fh = glCreateShader(GL_FRAGMENT_SHADER);
    glShaderSource(fh, 1, &fsh, NULL);
    glCompileShader(fh);
    glGetShaderInfoLog(fh, 512, NULL, infoLog);
    printf("Fragment shader log: %s\n", infoLog);

    tch = glCreateShader(GL_TESS_CONTROL_SHADER);
    glShaderSource(tch, 1, &tcsh, NULL);
    glCompileShader(tch);
    glGetShaderInfoLog(tch, 512, NULL, infoLog);
    printf("Tesselation control shader log: %s\n", infoLog);

    teh = glCreateShader(GL_TESS_EVALUATION_SHADER);
    glShaderSource(teh, 1, &tesh, NULL);
    glCompileShader(teh);
    glGetShaderInfoLog(teh, 512, NULL, infoLog);
    printf("Tesselation evaluation shader log: %s\n", infoLog);

    ph = glCreateProgram();
    glAttachShader(ph, vh);
    glAttachShader(ph, fh);
    glAttachShader(ph, tch);
    glAttachShader(ph, teh);
    glLinkProgram(ph);

    glGetShaderInfoLog(ph, 512, NULL, infoLog);
    printf("Program log: %s\n", infoLog);

    glUseProgram(ph);
    glPatchParameteri(GL_PATCH_VERTICES, 4);

    glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);

    while(!glfwWindowShouldClose(window)){
        int width, height;
        glfwGetFramebufferSize(window, &width, &height);
        glViewport(0, 0, width, height);

        draw_opengl();
        glfwSwapBuffers(window);
        glfwPollEvents();
    }
    glfwDestroyWindow(window);
    glfwTerminate();

    return 0;
}
