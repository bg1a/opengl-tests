// vim: set ft=glsl:
#version 330 core

in VS_OUT{
    vec3 coord;
    vec3 normal;
    vec2 tex_coord;
}vs_out;

out vec4 FragColor;

const vec4 HS[5] = vec4[5](
    vec4(1.0, 0.0, 0.0, 0.2),
    vec4(0.309016994, 0.951056516, 0.0, 0.2),
    vec4(-0.809016994, 0.587785252, 0.0, 0.2),
    vec4(-0.809016994, -0.587785252, 0.0, 0.2),
    vec4(0.309116994, -0.951056516, 0.0, 0.2)
);

const vec3 base_color = vec3(0.6, 0.5, 0.0);
const vec3 star_color = vec3(0.6, 0.0, 0.0);
const vec3 band_color = vec3(0.0, 0.3, 0.6);
const float band_width = 0.3;
const float s_scale = 10.0;
const float t_scale = 10.0;

uniform bool draw_lattice;
uniform bool set_addaptive;

void main(){
    vec3 color = vec3(0.0);
    vec4 p = vec4(vs_out.coord, 1.0);
    float star_in = -3.0;
    float s = vs_out.tex_coord.s;
    float t = vs_out.tex_coord.t;

    float band_in = 0.0;
    if(set_addaptive){
        band_in = smoothstep(-fwidth(p.z), fwidth(p.z), band_width - abs(p.z));
    }else{
        band_in = smoothstep(-0.005, 0.005, band_width - abs(p.z));
    }

    for(int i = 0; i < HS.length(); ++i){
        star_in += smoothstep(-0.005, 0.005, dot(p, HS[i]));
    }

    color = mix(base_color, star_color, clamp(star_in, 0.0, 1.0));
    color = mix(color, band_color, band_in);

    if(draw_lattice){
        if((fract(s * s_scale) > 0.2) && (fract(t * t_scale) > 0.2))
            discard;
    }

    FragColor = vec4(color, 1.0);
}
