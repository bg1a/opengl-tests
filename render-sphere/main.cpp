#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>

#include <vector>

#include <sys/time.h>
#include <unistd.h>

#include <GLES3/gl31.h>
#include <GL/gl.h>

#include <glm/glm.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtc/matrix_transform.hpp>

extern void create_window(int width, int height);
extern void window_loop();

const int width = 800;
const int height = 600;
const float PI = 3.14159265359f;

#define VERTEX_OFF  0
#define VERTEX_SZ   (3*sizeof(GLfloat))
#define NORMAL_OFF  (VERTEX_OFF + VERTEX_SZ)
#define NORMAL_SZ   (3*sizeof(GLfloat))
#define TEX_OFF     (NORMAL_OFF + NORMAL_SZ)
#define TEX_SZ      (2*sizeof(GLfloat))
#define POINT_SZ    (TEX_OFF + TEX_SZ)

std::vector<float> generate_sphere_vertices(int N, float R)
{
    const float lat_step = PI / (float)N;
    const float long_step = 2.0f * (PI / (float)N);
    std::vector<float> vertices;

    auto add_vertex = [&](int long_idx, int lat_idx, float u_idx, float v_idx){
        int t = long_idx % N;
        int m = lat_idx;

        float Px = R * sin(m * lat_step) * sin(t * long_step);
        float Py = R * cos(m * lat_step);
        float Pz = R * sin(m * lat_step) * cos(t * long_step);
        
        // Push vertix coordinates
        vertices.push_back(Px);
        vertices.push_back(Py);
        vertices.push_back(Pz);
        // Push normal coordinates
        vertices.push_back(Px);
        vertices.push_back(Py);
        vertices.push_back(Pz);
        // Push texture coordinates
        vertices.push_back(u_idx / (float)N);
        vertices.push_back(v_idx / (float)N);
        //printf("Add vertex %f, %f, %f, \t%f, %f\n",
                //Px, Py, Pz, u_idx / (float)N, v_idx / (float)N);
    };
    auto add_point = [&](int long_idx, int lat_idx){
        add_vertex(long_idx, lat_idx, (float)long_idx, (float)lat_idx);
    };
    auto add_pole_point = [&](int long_idx, int lat_idx, float u, float v){
        add_vertex(long_idx, lat_idx, u, v);
    };

    // Topmost band
    printf("Top band\n");
    for(int i = 0; i < N; i += 2){
        if(i != 0) add_point(i, 1);
        add_point(i, 1);
        add_point(i + 1, 1);
        add_pole_point(0, 0, i, 0);
        add_point(i + 2, 1);
        add_point(i + 2, 1);
    }
        
    printf("Center band\n");
    for(int j = 1, i = 0; (j + 1) < N; ++j){
        add_point(0, j);
        add_point(0, j);
        add_point(0, j + 1);
        for(i = 0; i < N; ++i){
            add_point(i + 1, j);
            add_point(i + 1, j + 1);
        }
        add_point(i, j + 1);
    }

    // Bottom band
    printf("Bottom band\n");
    for(int i = 0; i < N; i += 2){
        add_point(i, N - 1);
        add_point(i, N - 1);
        add_pole_point(0, N, i, N);
        add_point(i + 1, N - 1);
        add_point(i + 2, N - 1);
        if(i != (N - 1))add_point(i + 2, N - 1);
    }

    return vertices;
}

char *load_file(const char *filename)
{
    FILE *f = fopen(filename, "r");
    char *text = NULL;

    if(f){
        size_t size = 0;

        fseek(f, 0, SEEK_END);
        size = ftell(f);
        rewind(f);

        text = (char*)calloc(size + 1, sizeof(char));
        fread(text, sizeof(char), size, f);
        fclose(f); 
    }

    return text;
}

void load_shader(const char *vertex_source, const char *fragment_source,
        GLuint *vertex, GLuint *fragment, GLuint *program)
{
    const char * test_str = "#version ";
    char infoLog[512] = {0};
    char *shader_source = NULL;

    *vertex = glCreateShader(GL_VERTEX_SHADER);
    if(!strcmp(vertex_source, test_str)){
        glShaderSource(*vertex, 1, &vertex_source, NULL);
        glCompileShader(*vertex);
        glGetShaderInfoLog(*vertex, sizeof(infoLog), NULL, infoLog);
        printf("Vertex shader compile log: %s\n", infoLog);
    }else{
        shader_source = load_file(vertex_source);
        glShaderSource(*vertex, 1, &shader_source, NULL);
        glCompileShader(*vertex);
        glGetShaderInfoLog(*vertex, sizeof(infoLog), NULL, infoLog);
        printf("%s, compile log: %s\n", vertex_source, infoLog);
        free(shader_source);
    }

    *fragment = glCreateShader(GL_FRAGMENT_SHADER);
    if(!strcmp(fragment_source, test_str)){
        glShaderSource(*fragment, 1, &fragment_source, NULL);
        glCompileShader(*fragment);
        glGetShaderInfoLog(*fragment, sizeof(infoLog), NULL, infoLog);
        printf("Fragment shader compile log: %s\n", infoLog);
    }else{
        shader_source = load_file(fragment_source);
        glShaderSource(*fragment, 1, &shader_source, NULL);
        glCompileShader(*fragment);
        glGetShaderInfoLog(*fragment, sizeof(infoLog), NULL, infoLog);
        printf("%s, compile log: %s\n", fragment_source, infoLog);
        free(shader_source);
    }

    *program = glCreateProgram();
    glAttachShader(*program, *vertex);
    glAttachShader(*program, *fragment);
    glLinkProgram(*program);
}

GLuint vao, buf, tex, vs, fs, ph;
std::vector<float> vertices;

static struct timeval init_tv;

static double tv2d(struct timeval *tv)
{
    return (double)tv->tv_sec + ((double) tv->tv_usec) / 1000000.0;
}

static float get_time(void){
    struct timeval tv = {0};

    gettimeofday(&tv, NULL);

    return tv2d(&tv) - tv2d(&init_tv);
}
void draw_opengl(char c){
    const float radius = 2.0f;
    auto cam_time = get_time();
    //auto cam_time = 0.0;

    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    auto camX = std::sin(cam_time) * radius;
    auto camZ = std::cos(cam_time) * radius;
    glm::mat4 view = glm::lookAt(glm::vec3(camX, 2.0f, camZ),
                                 glm::vec3(0.0f, 0.0f, 0.0f), glm::vec3(0.0f, 1.0f, 0.0f));
    glUniformMatrix4fv(glGetUniformLocation(ph, "view"), 1, GL_FALSE, glm::value_ptr(view));
    glDrawArrays(GL_TRIANGLE_STRIP, 0, 
            (vertices.size() * sizeof(vertices[0])) / POINT_SZ);
}

int main(int argc, char **argv)
{
    char *img = load_file("world_winter.rgb");
    vertices = generate_sphere_vertices(64, 1.0);

    gettimeofday(&init_tv, NULL);
    create_window(width, height);
    
    glGenVertexArrays(1, &vao);
    glBindVertexArray(vao);
    glGenBuffers(1, &buf);
    glBindBuffer(GL_ARRAY_BUFFER, buf);
    glBufferData(GL_ARRAY_BUFFER, vertices.size() * sizeof(vertices[0]),
            vertices.data(), GL_STATIC_DRAW);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, POINT_SZ, (void*)VERTEX_OFF);
    glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, POINT_SZ, (void*)NORMAL_OFF);
    glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, POINT_SZ, (void*)TEX_OFF);
    glEnableVertexAttribArray(0);
    glEnableVertexAttribArray(1);
    glEnableVertexAttribArray(2);

    load_shader("render.vs", "render.fs", &vs, &fs, &ph);
    glUseProgram(ph);

    glm::mat4 model(1.0);
    glUniformMatrix4fv(glGetUniformLocation(ph, "model"), 1, GL_FALSE, glm::value_ptr(model));
    glm::mat4 proj = glm::perspective(glm::radians(45.0f), (float)width/(float)height, 0.1f, 100.0f);
    glUniformMatrix4fv(glGetUniformLocation(ph, "proj"), 1, GL_FALSE, glm::value_ptr(proj));

    glUniform1i(glGetUniformLocation(ph, "tex"), 0);

    glGenTextures(1, &tex);
    glBindTexture(GL_TEXTURE_2D, tex);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, 5400, 2700,
                                0, GL_RGB, GL_UNSIGNED_BYTE, img);
    
    glEnable(GL_DEPTH_TEST);
    glEnable(GL_CULL_FACE);
    glCullFace(GL_BACK);

    window_loop();

    return 0;
}
