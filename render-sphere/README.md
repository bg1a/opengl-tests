Sphere Rendering
================

Links
-----
[Earth Texture Mapping in Blender](https://b3d.interplanety.org/en/mapping-texture-to-planet/)
[Earth Image](https://visibleearth.nasa.gov/images/73580/january-blue-marble-next-generation-w-topography-and-bathymetry)
[SONGCO](http://www.songho.ca/opengl/gl_sphere.html)  
[Triangle Strip](https://www.learnopengles.com/tag/triangle-strips/)  
[Sphere Rendering Example](https://gist.github.com/zwzmzd/0195733fa1210346b00d)  

