// vim: set ft=glsl:
#version 330 core

in VS_OUT{
    vec3 coord;
    vec3 normal;
    vec2 tcoord;
}vs_out;

out vec4 FragColor;

uniform sampler2D tex;

void main(){
    vec3 color = texture(tex, vs_out.tcoord).rgb;
    FragColor = vec4(color, 1.0);
}
