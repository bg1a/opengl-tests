#include <stdlib.h>
#include <stdio.h>
#include <GLES3/gl31.h>
#include <GL/gl.h>

extern void create_window(int width, int height);
extern void window_loop();

GLfloat vs[] = {
    // Vertices         Colors
    
    // Face
    -1.0,  1.0,  1.0,  1.0, 0.0, 0.0,
    -1.0, -1.0,  1.0,  0.0, 0.0, 0.0,
     1.0, -1.0,  1.0,  0.0, 0.0, 0.0,
    -1.0,  1.0,  1.0,  1.0, 0.0, 0.0,
     1.0, -1.0,  1.0,  0.0, 0.0, 0.0,
     1.0,  1.0,  1.0,  0.0, 0.0, 1.0,

    // Back
     1.0,  1.0, -1.0,  0.0, 1.0, 0.0,
     1.0, -1.0, -1.0,  0.0, 0.0, 0.0,
    -1.0, -1.0, -1.0,  0.0, 0.0, 0.0, 
     1.0,  1.0, -1.0,  0.0, 1.0, 0.0,
    -1.0, -1.0, -1.0,  0.0, 0.0, 0.0,
    -1.0,  1.0, -1.0,  0.0, 0.0, 0.0,

    // Top
    -1.0,  1.0, -1.0,  0.0, 0.0, 0.0,
    -1.0,  1.0,  1.0,  0.0, 1.0, 0.0,
     1.0,  1.0,  1.0,  0.0, 0.0, 1.0,
    -1.0,  1.0, -1.0,  0.0, 0.0, 0.0,
     1.0,  1.0,  1.0,  0.0, 0.0, 1.0,
     1.0,  1.0, -1.0,  0.0, 0.0, 0.0,

    // Bottom
    -1.0, -1.0,  1.0,  0.0, 0.0, 0.0,
    -1.0, -1.0, -1.0,  0.0, 0.0, 0.0,
     1.0, -1.0, -1.0,  0.0, 0.0, 1.0,
    -1.0, -1.0,  1.0,  0.0, 0.0, 0.0,
     1.0, -1.0, -1.0,  0.0, 0.0, 1.0,
     1.0, -1.0,  1.0,  0.0, 0.0, 0.0,

    // Left
    -1.0,  1.0, -1.0,  0.0, 1.0, 0.0,
    -1.0, -1.0, -1.0,  0.0, 0.0, 0.0,
    -1.0, -1.0,  1.0,  0.0, 1.0, 0.0,
    -1.0,  1.0, -1.0,  0.0, 1.0, 0.0,
    -1.0, -1.0,  1.0,  0.0, 1.0, 0.0,
    -1.0,  1.0,  1.0,  1.0, 0.0, 0.0,

    // Right
     1.0,  1.0,  1.0,  1.0, 0.0, 0.0,
     1.0, -1.0,  1.0,  0.0, 0.0, 0.0,
     1.0, -1.0, -1.0,  0.0, 0.0, 0.0,
     1.0,  1.0,  1.0,  1.0, 0.0, 0.0,
     1.0, -1.0, -1.0,  0.0, 0.0, 0.0,
     1.0,  1.0, -1.0,  0.0, 0.0, 0.0,
};

const char *vsh = 
"#version 100\n"
"attribute vec3 v;"
"attribute vec3 c;"
"varying vec3 c_fs;"
"void main(){"
" mat4 s = mat4(1.0);"
" mat4 rx = mat4(1.0);"
" mat4 ry = mat4(1.0);"
" mat4 r;"
" mat4 t = mat4(1.0);"
" "
" float alpha = 15.0 * (3.1415 / 180.0);"
" s[0][0] = 0.5; s[1][1] = 0.5; s[2][2] = 0.5;"
" rx[1][1] = cos(alpha); rx[2][1] = -sin(alpha); rx[1][2] = sin(alpha); rx[2][2] = cos(alpha);"
" ry[0][0] = cos(alpha); ry[2][0] = sin(alpha); ry[0][2] = -sin(alpha); ry[2][2] = cos(alpha);"
" r = rx * ry;"
" t[3] = vec4(0.0, 0.0, 0.5, 1.0);"
" gl_Position = s * r * t * vec4(v, 1.0);"
" c_fs = c;"
"}";

const char *fsh = 
"#version 100\n"
"precision mediump float;"
"varying vec3 c_fs;"
"void main(){"
" gl_FragColor = vec4(c_fs, 1.0);"
"}";

GLuint vao, buf, vh, fh, ph;

void draw_opengl(void){
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glClearColor(0.05, 0.15, 0.35, 1.0);

    glDrawArrays(GL_TRIANGLES, 0, 36);
}

int main(int argc, char **argv)
{
    create_window(800, 600);

    glGenVertexArrays(1, &vao);
    glBindVertexArray(vao);
    glGenBuffers(1, &buf);
    glBindBuffer(GL_ARRAY_BUFFER, buf);
    glBufferData(GL_ARRAY_BUFFER, sizeof(vs), vs, GL_STATIC_DRAW);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 6*sizeof(GLfloat), 0);
    glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 6*sizeof(GLfloat), (void*)(3*sizeof(GLfloat)));
    glEnableVertexAttribArray(0);
    glEnableVertexAttribArray(1);

    vh = glCreateShader(GL_VERTEX_SHADER);
    glShaderSource(vh, 1, &vsh, NULL);
    glCompileShader(vh);
    fh = glCreateShader(GL_FRAGMENT_SHADER);
    glShaderSource(fh, 1, &fsh, NULL);
    glCompileShader(fh);
    ph = glCreateProgram();
    glAttachShader(ph, vh);
    glAttachShader(ph, fh);
    glLinkProgram(ph);
    glUseProgram(ph);

    glEnable(GL_DEPTH_TEST);
    glEnable(GL_CULL_FACE);
    glCullFace(GL_BACK);

    glEnable(GL_POLYGON_SMOOTH);
    glHint(GL_POLYGON_SMOOTH_HINT, GL_NICEST);
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    GLint sb = 0;
glEnable(GL_MULTISAMPLE_ARB);
glGetIntegerv(GL_SAMPLES, &sb);
printf("Sample buffers: %d\n", sb);
    window_loop();

    return 0;
}
