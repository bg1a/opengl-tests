#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include <vector>
#include <limits>

#include <GLES3/gl31.h>
#include <GL/gl.h>

#define GLM_SWIZZLE 
#include <glm/glm.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtc/matrix_transform.hpp>

extern void create_window(int width, int height);
extern void window_loop();

const float camX = 0.0f;
const float camY = 0.0f;
const float camZ = 0.0f;
const int width = 1280;
const int height = 640;
const float PI = 3.14159265359f;
const float aspect = (float)width / (float)height;
glm::vec3 view_pos(camX, camY, camZ);

const int background_img_width = 1280;
const int background_img_height = 640;
unsigned char *background_img;

GLfloat vertices_plane[] = {
    -1.0,   1.0,  0.0,      0.0, 1.0,
    -1.0,  -1.0,  0.0,      0.0, 0.0,
     1.0,   1.0,  0.0,      1.0, 1.0,
     1.0,  -1.0,  0.0,      1.0, 0.0,
};

// View Matrix (Raw-order)
// 1.0  0.0  0.0  camX
// 0.0  1.0  0.0  camY
// 0.0  0.0 -1.0  camZ
// 0.0  0.0  0.0  1.0
//
// View Matrix (Column-order)
glm::mat4 view(1.0f,  0.0f,  0.0f,  0.0f,
               0.0f,  1.0f,  0.0f,  0.0f,
               0.0f,  0.0f, -1.0f,  0.0f,
               camX,  camY,  camZ,  1.0f);
glm::mat4 view_inv = glm::inverse(view);

float zn = 1.0f;
float zf = 100.0f;
// Projection Matrix (Raw-order), angle 45 degrees
// 1.0  0.0     0.0                 0.0
// 0.0  1.0     0.0                 0.0
// 0.0  0.0 -(zn + zf)/(zn - zf)    2*zn*zf/(zn - zf)
// 0.0  0.0     1.0                 0.0
//
// Projection Matrix (Column-order)
glm::mat4 proj(1.0f, 0.0f,      0.0f,               0.0f,
               0.0f, 1.0f,      0.0f,               0.0f,
               0.0f, 0.0f, -(zn + zf)/(zn - zf),    1.0f,
               0.0f, 0.0f,    2*zn*zf/(zn - zf),    0.0f);
glm::mat4 proj_inv = glm::inverse(proj);

char *load_file(const char *filename)
{
    FILE *f = fopen(filename, "r");
    char *text = NULL;

    if(f){
        size_t size = 0;

        fseek(f, 0, SEEK_END);
        size = ftell(f);
        rewind(f);

        text = (char*)calloc(size + 1, sizeof(char));
        fread(text, sizeof(char), size, f);
        fclose(f); 
    }

    return text;
}

void load_shader(const char *vertex_source, const char *fragment_source,
        GLuint *vertex, GLuint *fragment, GLuint *program)
{
    const char * test_str = "#version ";
    char infoLog[512] = {0};
    char *shader_source = NULL;

    *vertex = glCreateShader(GL_VERTEX_SHADER);
    if(!strcmp(vertex_source, test_str)){
        glShaderSource(*vertex, 1, &vertex_source, NULL);
        glCompileShader(*vertex);
        glGetShaderInfoLog(*vertex, sizeof(infoLog), NULL, infoLog);
        printf("Vertex shader compile log: %s\n", infoLog);
    }else{
        shader_source = load_file(vertex_source);
        glShaderSource(*vertex, 1, &shader_source, NULL);
        glCompileShader(*vertex);
        glGetShaderInfoLog(*vertex, sizeof(infoLog), NULL, infoLog);
        printf("%s, compile log: %s\n", vertex_source, infoLog);
        free(shader_source);
    }

    *fragment = glCreateShader(GL_FRAGMENT_SHADER);
    if(!strcmp(fragment_source, test_str)){
        glShaderSource(*fragment, 1, &fragment_source, NULL);
        glCompileShader(*fragment);
        glGetShaderInfoLog(*fragment, sizeof(infoLog), NULL, infoLog);
        printf("Fragment shader compile log: %s\n", infoLog);
    }else{
        shader_source = load_file(fragment_source);
        glShaderSource(*fragment, 1, &shader_source, NULL);
        glCompileShader(*fragment);
        glGetShaderInfoLog(*fragment, sizeof(infoLog), NULL, infoLog);
        printf("%s, compile log: %s\n", fragment_source, infoLog);
        free(shader_source);
    }

    *program = glCreateProgram();
    glAttachShader(*program, *vertex);
    glAttachShader(*program, *fragment);
    glLinkProgram(*program);
}
GLuint plane_vao, plane_buf, plane_vs, plane_fs, plane_ph;
GLuint tex;

void draw_opengl(char c){
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glBindVertexArray(plane_vao);
    glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
}

struct Light{
    glm::vec3 position;
    float diffuse_intensity;

    Light(const glm::vec3 &position, float diffuse_intensity) :
        position(position), diffuse_intensity(diffuse_intensity) {}

};

struct Material{
    glm::vec3 diffuse_color;
    float specular_exponent;
    float refractive_index;
    glm::vec4 albedo;

    Material(float refractive_index, const glm::vec4 &albedo, const glm::vec3 &color, float specular_exponent) :
        refractive_index(refractive_index), diffuse_color(color), specular_exponent(specular_exponent), albedo(albedo){}
    Material(void) : Material(1.0f, glm::vec4(1.0f, 0.0f, 0.0f, 0.0f), glm::vec3(0.0f), 32.0f){}
};

struct Sphere{
    Material material;
    glm::vec3 center;
    float radius;

    Sphere(const glm::vec3 &center, const float &radius, const Material &material) :
        center(center), radius(radius), material(material) {}
    Sphere(const glm::vec3 &center, const float &radius) :
        center(center), radius(radius) {}

    bool intersects(glm::vec3 ray_origin, glm::vec3 ray, glm::vec3 &intersect_point, float &intersect_dist) const{
        glm::vec3 ray_unit_vector = glm::normalize(ray);
        bool ret = false;
    
        glm::vec3 center_to_ray_origin_vector = glm::vec3(center - ray_origin);
        float center_to_ray_origin_dist = glm::length(center_to_ray_origin_vector);
        float center_to_ray_proj = glm::dot(center_to_ray_origin_vector, ray_unit_vector);
        float center_to_ray_dist = glm::sqrt(center_to_ray_origin_dist * center_to_ray_origin_dist - 
                                                 center_to_ray_proj * center_to_ray_proj);
    
        if(center_to_ray_dist <= radius){
            float adjust = glm::sqrt(radius*radius - center_to_ray_dist*center_to_ray_dist);
        
            intersect_dist = (center_to_ray_proj - adjust) > 0.0f ?
                             (center_to_ray_proj - adjust) :
                             (adjust + center_to_ray_proj);
            if(intersect_dist > 0){
                intersect_point = ray_origin + ray_unit_vector * intersect_dist;
                ret = true;
            }
        }

        return ret;
    }
};

bool scene_intersect(const glm::vec3 &origin, const glm::vec3 &ray, const std::vector<Sphere> &spheres,
        glm::vec3 &point, glm::vec3 &N, Material &material)
{
    float max_dist = std::numeric_limits<float>::max();

    for(const auto &sphere : spheres){
        glm::vec3 intersect_point;
        float intersect_dist;

        if(sphere.intersects(origin, ray, intersect_point, intersect_dist)){
            if(intersect_dist < max_dist){
                material = sphere.material;
                max_dist = intersect_dist;
                N = glm::normalize(intersect_point - sphere.center);
                point = intersect_point;
            }
        }
    }

    return max_dist < 1000.0f;
}

bool scene_intersect(const glm::vec3 &origin, const glm::vec3 &ray, const std::vector<Sphere> &spheres,
        glm::vec3 &point)
{
    Material material;
    glm::vec3 N;

    return scene_intersect(origin, ray, spheres, point, N, material);
}

glm::vec3 refract(const glm::vec3 &incident_ray, const glm::vec3 &normal_vector, float refraction_index)
{
    glm::vec3 I = glm::normalize(incident_ray);
    glm::vec3 N = glm::normalize(normal_vector);
    float cosTheta1 = -glm::clamp(glm::dot(I, N), -1.0f, 1.0f);
    float eta = 0.0f;

    // if cosTheta1 is negative, so we are inside of sphere
    if(cosTheta1 < 0){
        N = -N;
        eta = refraction_index;
    }else{
        eta = 1.0f / refraction_index;
    }
    return glm::normalize(glm::refract(I, N, eta));
}

glm::vec3 cast_ray(const glm::vec3 origin, const glm::vec3 &ray,
        const std::vector<Sphere> &spheres, const std::vector<Light> lights,
        int depth = 0)
{
    glm::vec3 color(0.2f, 0.7f, 0.8f);
    Material material;
    glm::vec3 N, point;

    if(depth <= 4){
        if(scene_intersect(origin, ray, spheres, point, N, material)){
            glm::vec3 reflect_vector = glm::reflect(ray, N);
            glm::vec3 refract_vector = refract(ray, N, material.refractive_index);
            glm::vec3 refract_orig = glm::dot(refract_vector, N) < 0.0f ? (point - N*glm::vec3(0.001)) :
                (point + N*glm::vec3(0.001));
            glm::vec3 reflect_orig = glm::dot(reflect_vector, N) < 0.0f ? (point - N*glm::vec3(0.001)) :
                (point + N*glm::vec3(0.001));
            glm::vec3 refract_color = cast_ray(refract_orig, refract_vector, spheres, lights, depth + 1);
            glm::vec3 reflect_color = cast_ray(reflect_orig, reflect_vector, spheres, lights, depth + 1);
            float diffuse_intensity = 0.0f;
            float specular_intensity = 0.0f;

            for(const auto &light : lights){
                glm::vec3 light_vector = glm::normalize(light.position - point);
                glm::vec3 view_vector = glm::normalize(view_pos - point);
                glm::vec3 half_vector = glm::normalize(light_vector + view_vector);

                glm::vec3 shadow_point;
                glm::vec3 shadow_orig = glm::dot(light_vector, N) < 0.0f ? (point - N*glm::vec3(0.001)) :
                    (point + N*glm::vec3(0.001));
                if(scene_intersect(shadow_orig, light_vector, spheres, shadow_point)){
                    float actual_dist = glm::length(glm::vec3(shadow_point - point));
                    float expected_dist = glm::length(glm::vec3(light.position - point));

                    if(actual_dist < expected_dist) continue;
                }

                diffuse_intensity += light.diffuse_intensity * glm::max(glm::dot(light_vector, N), 0.0f);
                specular_intensity += glm::pow(glm::max(glm::dot(half_vector, N), 0.0f), material.specular_exponent);
            }
            color = material.diffuse_color * diffuse_intensity * material.albedo[0] + 
                specular_intensity * material.albedo[1] + 
                reflect_color * material.albedo[2] +
                refract_color * material.albedo[3];
        }
       // else{
       //     if(glm::dot(ray, glm::vec3(0.0f, 0.0f, -1.0f)) >= 0.0f){ 
       //         std::vector<Sphere> tmp_spheres;
       //         glm::vec3 point, N;
       //         float R = (float)width/PI;

       //         tmp_spheres.push_back(Sphere(glm::vec3(camX, camY, camZ), R));
       //         scene_intersect(origin, ray, tmp_spheres, point);

       //         float phi = glm::acos(point.x / R); 
       //         float theta = glm::acos(point.y / R);
       //         float img_x = (((float)background_img_width / 2 )/ PI) * (PI - phi);
       //         float img_y = ((float)background_img_height / PI) * theta;

       //         int img_pos = int((float)background_img_width * img_y + img_x) * 3;
       //         float red = (float)background_img[img_pos] / 255.0f;
       //         float green = (float)background_img[img_pos + 1] / 255.0f;
       //         float blue = (float)background_img[img_pos + 2] / 255.0f;

       //         color = glm::vec3(red, green, blue);
       //         //color = glm::vec3(1.0, 0.0, 0.0);
       //     }
       // }
    }

    return color;
}

glm::vec3 tone_mapping(glm::vec3 color)
{
    return color / (glm::vec3(1.0f, 1.0f, 1.0f) + color);
}

glm::vec3 gamma_correction(glm::vec3 color)
{
    return glm::pow(color, glm::vec3(1.0f/2.2f));
}

void fill_buf(glm::vec3 buf[], int width, int height)
{
    Material ivory(1.0f, glm::vec4(0.6f,  0.3f, 0.1f, 0.0f), glm::vec3(0.4f, 0.4f, 0.3f), 50.0f);
    Material glass(1.5f, glm::vec4(0.0f,  0.5f, 0.1f, 0.8f), glm::vec3(0.6f, 0.7f, 0.8f), 125.0f);
    Material red_rubber(1.0f, glm::vec4(0.9f,  0.1f, 0.0f, 0.0f), glm::vec3(0.3f, 0.1f, 0.1f), 10.0f);
    Material mirror(1.0f, glm::vec4(0.0f, 10.0f, 0.8f, 0.0f), glm::vec3(1.0f, 1.0f, 1.0f), 1425.0f);

    std::vector<Sphere> spheres;
    spheres.push_back(Sphere(glm::vec3(-3.0f, 0.0f, -16.0f), 2.0f, ivory));
    spheres.push_back(Sphere(glm::vec3(-1.0f, -1.5f, -12.0f), 2.0f, glass));
    spheres.push_back(Sphere(glm::vec3( 1.5f, -0.5f, -18.0f), 3.0f, red_rubber));
    spheres.push_back(Sphere(glm::vec3( 7.0f,  5.0f, -18.0f), 4.0f, mirror));

    std::vector<Light>  lights;
    lights.push_back(Light(glm::vec3(-20.0f, 20.0f,  20.0f), 1.5f));
    lights.push_back(Light(glm::vec3(30.0f, 50.0f, -25.0f), 1.8f));
    lights.push_back(Light(glm::vec3(30.0f, 20.0f,  30.0f), 1.7f));

    int i,j,idx;

    for(j = 0, idx = 0; j < height; ++j){
        //for(i = 0; i < width; ++i, idx = (height - j - 1) * width + i){
        for(i = 0; i < width; ++i, idx = j * width + i){
            //buf[idx] = glm::vec3((float)j/height, (float)i/width, 0.0f);

            glm::vec4 p_view = glm::vec4(((float)i/width * 2.0f - 1.0f) * aspect,
                                         (float)j/height * 2.0f - 1.0f,
                                         zn, 1.0f);
            glm::vec3 p = glm::vec3(view_inv * p_view);
            glm::vec3 ray_init = glm::vec3(p.x, p.y, p.z) - view_pos; 

            glm::vec3 color = cast_ray(view_pos, ray_init, spheres, lights);
            //color = tone_mapping(color);
            //color = gamma_correction(color);

            buf[idx] = color;

            // Print Background
            //float red = (float)background_img[(j * width + i) * 3] / 255.0;
            //float green = (float)background_img[(j * width + i) * 3 + 1] / 255.0;
            //float blue = (float)background_img[(j * width + i) * 3 + 2] / 255.0;
            //buf[idx] = glm::vec3(red, green, blue);
        }
    }
}

int main(int argc, char **argv)
{
    glm::vec3 *buf = (glm::vec3 *)calloc(width*height, sizeof(float)*3);

    background_img = (unsigned char*)load_file("red.rgb");
    fill_buf(buf, width, height);

    create_window(width, height);

    glGenVertexArrays(1, &plane_vao);
    glBindVertexArray(plane_vao);
    glGenBuffers(1, &plane_buf);
    glBindBuffer(GL_ARRAY_BUFFER, plane_buf);
    glBufferData(GL_ARRAY_BUFFER, sizeof(vertices_plane), vertices_plane, GL_STATIC_DRAW);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 5*sizeof(float), 0);
    glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 5*sizeof(float), (void*)(3*sizeof(float)));
    glEnableVertexAttribArray(0);
    glEnableVertexAttribArray(1);

    load_shader("final.vs", "final.fs", &plane_vs, &plane_fs, &plane_ph);
    glUseProgram(plane_ph);

    glGenTextures(1, &tex);
    glBindTexture(GL_TEXTURE_2D, tex);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB32F, width, height,
                                0, GL_RGB, GL_FLOAT, buf);

    glActiveTexture(GL_TEXTURE0);
    glUniform1i(glGetUniformLocation(plane_ph, "tex"), 0);

    glEnable(GL_DEPTH_TEST);
    window_loop();

    return 0;
}
