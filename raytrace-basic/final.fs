// vim: set ft=glsl:
#version 330 core

in VS_OUT{
    vec3 coord;
    vec2 tex_coord;
}vs_out;

uniform sampler2D tex;
out vec4 FragColor;

void main(void){
    vec3 color = texture(tex, vs_out.tex_coord).rgb;
    FragColor = vec4(color, 1.0);
}
