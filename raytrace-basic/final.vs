// vim: set ft=glsl:
#version 330 core

layout (location = 0) in vec3 coord;
layout (location = 1) in vec2 tex_coord;

out VS_OUT{
    vec3 coord;
    vec2 tex_coord;
}vs_out;

void main(void){
    gl_Position = vec4(coord, 1.0);

    vs_out.coord = coord;
    vs_out.tex_coord = tex_coord;
}
