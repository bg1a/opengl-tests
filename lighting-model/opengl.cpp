#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <fcntl.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <sys/time.h>
#include <unistd.h>

#include <iostream>

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <GLES3/gl31.h>
#include <GL/gl.h>
#include <GL/glx.h>
#include <GL/glext.h>

extern void create_window(int width, int height);
extern void window_loop(void);

static const int win_width = 1920;
static const int win_height = 1200;

static const int img_width = 1920;
static const int img_height = 1200;
static const int img_bytes_per_pixel = 4;
static const int img_size = img_width * img_height * img_bytes_per_pixel;
const char *bin_file1 = "./test_img_bgrx.raw";
const char *bin_file2 = "./nature_monte2.rgba";

// OpenGL, constant data and global variables

// For triangle strip
//GLfloat vertices[] = {
//    //Vertex                                Texture
//   -1.0f,  1.0f,  1.0f,                     0.0f, 0.0f,      // Front-top-left
//    1.0f,  1.0f,  1.0f,                     1.0f, 0.0f,      // Front-top-right
//   -1.0f, -1.0f,  1.0f,                     0.0f, 1.0f,      // Front-bottom-left
//    1.0f, -1.0f,  1.0f,                     1.0f, 1.0f,      // Front-bottom-right
//    1.0f, -1.0f, -1.0f,                     1.0f, 0.0f,      // Back-bottom-right
//    1.0f,  1.0f,  1.0f,                     0.0f, 1.0f,      // Front-top-right
//    1.0f,  1.0f, -1.0f,                     0.0f, 0.0f,      // Back-top-right
//   -1.0f,  1.0f,  1.0f,                     1.0f, 1.0f,      // Front-top-left
//   -1.0f,  1.0f, -1.0f,                     1.0f, 0.0f,      // Back-top-left
//   -1.0f, -1.0f,  1.0f,                     0.0f, 1.0f,      // Front-bottom-left
//   -1.0f, -1.0f, -1.0f,                     0.0f, 0.0f,      // Back-bottom-left
//    1.0f, -1.0f, -1.0f,                     1.0f, 0.0f,      // Back-bottom-right
//   -1.0f,  1.0f, -1.0f,                     0.0f, 1.0f,      // Back-top-left
//    1.0f,  1.0f, -1.0f,                     1.0f, 1.0f,      // Back-top-right
//};

GLfloat vertices[] = {
    //Vertex             Normals              Texture
   -1.0f, -1.0f, -1.0f,  0.0f,  0.0f, -1.0f,  0.0f, 0.0f,                                                             
    1.0f, -1.0f, -1.0f,  0.0f,  0.0f, -1.0f,  1.0f, 0.0f,                                                             
    1.0f,  1.0f, -1.0f,  0.0f,  0.0f, -1.0f,  1.0f, 1.0f,                                                             
    1.0f,  1.0f, -1.0f,  0.0f,  0.0f, -1.0f,  1.0f, 1.0f,                                                             
   -1.0f,  1.0f, -1.0f,  0.0f,  0.0f, -1.0f,  0.0f, 1.0f,                                                             
   -1.0f, -1.0f, -1.0f,  0.0f,  0.0f, -1.0f,  0.0f, 0.0f,                                                             
                                            
   -1.0f, -1.0f,  1.0f,  0.0f,  0.0f,  1.0f,  0.0f, 0.0f,                                                            
    1.0f, -1.0f,  1.0f,  0.0f,  0.0f,  1.0f,  1.0f, 0.0f,                                                            
    1.0f,  1.0f,  1.0f,  0.0f,  0.0f,  1.0f,  1.0f, 1.0f,                                                            
    1.0f,  1.0f,  1.0f,  0.0f,  0.0f,  1.0f,  1.0f, 1.0f,                                                            
   -1.0f,  1.0f,  1.0f,  0.0f,  0.0f,  1.0f,  0.0f, 1.0f,                                                            
   -1.0f, -1.0f,  1.0f,  0.0f,  0.0f,  1.0f,  0.0f, 0.0f,                                                            

   -1.0f,  1.0f,  1.0f, -1.0f,  0.0f,  0.0f,  1.0f, 1.0f,                                                   
   -1.0f,  1.0f, -1.0f, -1.0f,  0.0f,  0.0f,  0.0f, 1.0f,                                                   
   -1.0f, -1.0f, -1.0f, -1.0f,  0.0f,  0.0f,  0.0f, 0.0f,                                                   
   -1.0f, -1.0f, -1.0f, -1.0f,  0.0f,  0.0f,  0.0f, 0.0f,                                                   
   -1.0f, -1.0f,  1.0f, -1.0f,  0.0f,  0.0f,  1.0f, 0.0f,                                                   
   -1.0f,  1.0f,  1.0f, -1.0f,  0.0f,  0.0f,  1.0f, 1.0f,                                                   
                                                         
    1.0f,  1.0f,  1.0f,  1.0f,  0.0f,  0.0f,  0.0f, 0.0f,                                                   
    1.0f,  1.0f, -1.0f,  1.0f,  0.0f,  0.0f,  1.0f, 0.0f,                                                   
    1.0f, -1.0f, -1.0f,  1.0f,  0.0f,  0.0f,  1.0f, 1.0f,                                                   
    1.0f, -1.0f, -1.0f,  1.0f,  0.0f,  0.0f,  1.0f, 1.0f,                                                   
    1.0f, -1.0f,  1.0f,  1.0f,  0.0f,  0.0f,  0.0f, 1.0f,                                                   
    1.0f,  1.0f,  1.0f,  1.0f,  0.0f,  0.0f,  0.0f, 0.0f,                                                   

   -1.0f, -1.0f, -1.0f,  0.0f, -1.0f,  0.0f,  0.0f, 0.0f,                                                  
    1.0f, -1.0f, -1.0f,  0.0f, -1.0f,  0.0f,  1.0f, 0.0f,                                                  
    1.0f, -1.0f,  1.0f,  0.0f, -1.0f,  0.0f,  1.0f, 1.0f,                                                  
    1.0f, -1.0f,  1.0f,  0.0f, -1.0f,  0.0f,  1.0f, 1.0f,                                                  
   -1.0f, -1.0f,  1.0f,  0.0f, -1.0f,  0.0f,  0.0f, 1.0f,                                                  
   -1.0f, -1.0f, -1.0f,  0.0f, -1.0f,  0.0f,  0.0f, 0.0f,                                                  
                                                         
    1.0f,  1.0f,  1.0f,  0.0f,  1.0f,  0.0f,  1.0f, 1.0f,                                                  
    1.0f,  1.0f, -1.0f,  0.0f,  1.0f,  0.0f,  1.0f, 0.0f,                                                  
   -1.0f,  1.0f, -1.0f,  0.0f,  1.0f,  0.0f,  0.0f, 0.0f,                                                  
    1.0f,  1.0f,  1.0f,  0.0f,  1.0f,  0.0f,  1.0f, 1.0f,                                                  
   -1.0f,  1.0f, -1.0f,  0.0f,  1.0f,  0.0f,  0.0f, 0.0f,                                                  
   -1.0f,  1.0f,  1.0f,  0.0f,  1.0f,  0.0f,  0.0f, 1.0f,                                                  
};


GLuint program, program_lc;
GLuint vao, vbo, cbo, tex1, tex2;
GLuint vao_lc, vbo_lc;

static const char *v_shader = 
"#version 330 core \n"
"layout (location=0) in vec3 vPos; \n"
"layout (location=1) in vec2 tPos; \n"
"layout (location=2) in vec3 in_Normal; \n"
"uniform mat4 model; \n"
"uniform mat4 view; \n"
"uniform mat4 proj; \n"
"out vec2 texcoord; \n"
"out vec3 Normal; \n"
"out vec3 vertex; \n"
"void main(void) \n"
"{ \n"
"  gl_Position = proj * view * model * vec4(vPos, 1.0f); \n"
"  texcoord = tPos; \n"
"  Normal = mat3(transpose(inverse(model))) * in_Normal; \n"
"  vertex = vec3(model * vec4(vPos, 1.0f)); \n"
"} \n";

static const char *frag_shader =
"#version 330 core \n"
"in vec2 texcoord; "
"in vec3 Normal; "
"in vec3 vertex; "
"uniform sampler2D tex1; "
"uniform sampler2D tex2; "
"uniform vec3 viewer; "
"out vec4 FragColor; "
"struct Material{ "
"    vec3 ambient; "
"    vec3 diffuse; "
"    vec3 spec; "
"    float shin; "
"}; "
"uniform Material m; "
"struct Light{ "
"    vec3 ambient; "
"    vec3 diffuse; "
"    vec3 spec; "
"    vec3 pos; "
"}; "
"const float cutoff_cos = cos(7.0 * (3.14 / 180.0));"
"const float out_cutoff_cos = cos(10.0 * (3.14 / 180));"
"uniform Light l; "
"void main(void) "
"{ "
"  vec4 tex1_color = texture(tex1, texcoord); "
"  vec4 tex2_color = texture(tex2, texcoord); "
"  tex1_color.rgba = tex1_color.bgra; "
//"  FragColor = mix(tex1_color, tex2_color, 0.0); "
"  float l_distance = length(l.pos - vertex);"
"  float attenuation = 1.0 / (1.0 + 0.003 * l_distance + 0.009 * pow(l_distance, 2)); "
"  vec3 ambient = l.ambient * m.ambient * attenuation; "
"  float d = max(dot(normalize(l.pos - vertex), Normal), 0.0f); "
"  vec3 diffuse = l.diffuse * m.diffuse * d * attenuation; "
"  vec3 r = reflect(normalize(vertex - l.pos), Normal); "
"  vec3 v = normalize(viewer - vertex); "
"  vec3 spec = l.spec * m.spec * pow(max(dot(r, v), 0.0f), m.shin) * attenuation; "
"  float theta_cos = dot(normalize(l.pos), normalize(l.pos - vertex));"
"  float I = clamp( (theta_cos - out_cutoff_cos) / (cutoff_cos - out_cutoff_cos), 0.0, 1.0);"
"  vec3 color = ambient + diffuse * I + spec * I; "
"  vec3 tex_color = vec3(mix(tex1_color, tex2_color, 0.0)); "
"  FragColor = vec4(color * tex_color, 1.0f); "
"} ";

static const char *v_shader_lc = 
"#version 330 core \n"
"layout (location = 0) in vec3 vPos; \n"
"uniform mat4 model; \n"
"uniform mat4 view; \n"
"uniform mat4 proj; \n"
"void main(void) \n"
"{ \n"
" gl_Position = proj * view * model * vec4(vPos, 1.0f); \n"
"} \n";

static const char *f_shader_lc = 
"#version 330 core \n"
"out vec4 FragColor; \n"
"void main(void) \n"
"{ \n"
"  FragColor = vec4(1.0f, 1.0f, 1.0f, 1.0f); \n"
"} \n";

unsigned char *image_data1 = NULL;
unsigned char *image_data2 = NULL;

static struct timeval init_tv;

static double tv2d(struct timeval *tv)
{
    return (double)tv->tv_sec + ((double) tv->tv_usec) / 1000000.0;
}

static float get_time(void){
    struct timeval tv = {0};

    gettimeofday(&tv, NULL);

    return tv2d(&tv) - tv2d(&init_tv);
}

void upload_img(const char *filename, unsigned char **image_data)
{
    int f = open(filename, O_RDONLY);
    struct stat buf = {0};

    if((f > 0) && !fstat(f, &buf) && (buf.st_size == img_size)){
        off_t fsize = buf.st_size;
        unsigned char *data = NULL;

        data = (unsigned char*)mmap(NULL, fsize, PROT_READ, MAP_SHARED, f, 0);
        if(data){
            *image_data = (unsigned char*)malloc(fsize);
            for(int r = (img_height - 1); r > 0; --r){
                size_t raw_size = img_width * img_bytes_per_pixel;
                unsigned char *src = data + r * raw_size;
                unsigned char *dst = *image_data + (img_height - r - 1) * raw_size;
                memcpy(dst, src, raw_size);
            }
            munmap(data, fsize);
        }
    }else{
        printf("Failed to read file!\n");
        printf("Make sure that file: %s exists and it contains RGBA data of size %d\n", filename, img_size);
    }
    close(f);
}

void draw_opengl()
{
    //glDisable(GL_CULL_FACE);
    //glDisable(GL_LIGHTING);
    glClear(GL_DEPTH_BUFFER_BIT | GL_COLOR_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);
    //glClearColor(0.2f, 0.3f, 0.3f, 1.0f);

    auto lPos_time = get_time();
    auto lPos_radius = 6.0f;
    auto lPos_x = lPos_radius * glm::sin(lPos_time);
    auto lPos_z = lPos_radius * glm::cos(lPos_time);
    glm::vec3 lPos(lPos_x, 0.0f, lPos_z);

    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, tex1);
    glActiveTexture(GL_TEXTURE1);
    glBindTexture(GL_TEXTURE_2D, tex2);

    auto model = glm::mat4(1.0f);
    //model = glm::rotate(model, get_time() * glm::radians(50.0f), glm::vec3(0.5f, 1.0, 0.0));
    model = glm::rotate(model, glm::radians(50.0f), glm::vec3(0.5f, 1.0, 0.0));


    auto radius = 14;
    //auto view = glm::mat4(1.0f);
    //view = glm::translate(view, glm::vec3(0.0, 0.0, -6.0f));
    //

    //auto cam_time = get_time();
    auto cam_time = 0;
    auto camX = glm::sin(cam_time) * radius;
    auto camZ = glm::cos(cam_time) * radius;
    auto view = glm::lookAt(glm::vec3(camX, 0, camZ), glm::vec3(0, 0, 0), glm::vec3(0, 1, 0));
    auto proj = glm::perspective(glm::radians(45.0f), (float)img_width/img_height, 0.1f, 100.0f);

    auto model_lc = glm::mat4(1.0f);
    model_lc = glm::translate(model_lc, lPos);
    model_lc = glm::scale(model_lc, glm::vec3(0.1f, 0.1f, 0.1f));
    
    glUseProgram(program_lc);
    glBindVertexArray(vao_lc);

    auto model_lc_loc = glGetUniformLocation(program_lc, "model");
    auto view_lc_loc = glGetUniformLocation(program_lc, "view");
    auto proj_lc_loc = glGetUniformLocation(program_lc, "proj");

    glUniformMatrix4fv(model_lc_loc, 1, GL_FALSE, glm::value_ptr(model_lc));
    glUniformMatrix4fv(view_lc_loc, 1, GL_FALSE, glm::value_ptr(view));
    glUniformMatrix4fv(proj_lc_loc, 1, GL_FALSE, glm::value_ptr(proj));

    //glDrawArrays(GL_TRIANGLE_STRIP, 0, 14);
    glDrawArrays(GL_TRIANGLES, 0, 36);


    glUseProgram(program);
    glBindVertexArray(vao);
    auto model_loc = glGetUniformLocation(program, "model");
    glUniformMatrix4fv(model_loc, 1, GL_FALSE, glm::value_ptr(model));

    auto view_loc = glGetUniformLocation(program, "view");
    glUniformMatrix4fv(view_loc, 1, GL_FALSE, glm::value_ptr(view));

    auto proj_loc = glGetUniformLocation(program, "proj");
    glUniformMatrix4fv(proj_loc, 1, GL_FALSE, glm::value_ptr(proj));

    auto lPos_loc = glGetUniformLocation(program, "l.pos");
    glUniform3fv(lPos_loc, 1, &lPos[0]);

    glm::vec3 light_ambient(0.7, 0.7, 0.7);
    auto lPos_ambient = glGetUniformLocation(program, "l.ambient");
    glUniform3fv(lPos_ambient, 1, &light_ambient[0]);

    glm::vec3 light_diffuse(0.5, 0.5, 0.5);
    auto lPos_diffuse = glGetUniformLocation(program, "l.diffuse");
    glUniform3fv(lPos_diffuse, 1, &light_diffuse[0]);

    glm::vec3 light_spec(1.0, 1.0, 1.0);
    auto lPos_spec = glGetUniformLocation(program, "l.spec");
    glUniform3fv(lPos_spec, 1, &light_spec[0]);

    glm::vec3 material(1.0, 1.0, 1.0);
    auto mPos_ambient = glGetUniformLocation(program, "m.ambient");
    glUniform3fv(mPos_ambient, 1, &material[0]);

    auto mPos_diffuse = glGetUniformLocation(program, "m.diffuse");
    glUniform3fv(mPos_diffuse, 1, &material[0]);

    auto mPos_spec = glGetUniformLocation(program, "m.spec");
    glUniform3fv(mPos_spec, 1, &material[0]);

    auto mPos_shin = glGetUniformLocation(program, "m.shin");
    glUniform1f(mPos_shin, 32.0);

    auto vPos_loc = glGetUniformLocation(program, "viewer");
    glm::vec3 vPos(camX, 0.0, camZ);
    glUniform3fv(vPos_loc, 1, &vPos[0]);
    //glDrawArrays(GL_TRIANGLE_STRIP, 0, 14);
    glDrawArrays(GL_TRIANGLES, 0, 36);



}

int main(int argc, char **argv)
{
    char infoLog[512] = {0};
#if defined(__linux__)
    setenv("DISPLAY", ":0", 0);
#endif
    create_window(win_width, win_height);

    gettimeofday(&init_tv, NULL);

    GLuint vs = glCreateShader(GL_VERTEX_SHADER);
    glShaderSource(vs, 1, (const GLchar **) &v_shader, NULL);
    glCompileShader(vs);
    glGetShaderInfoLog(vs, sizeof(infoLog), NULL, infoLog);
    std::cout << "VS shader status: " << infoLog << std::endl;
    
    GLuint fs = glCreateShader(GL_FRAGMENT_SHADER);
    glShaderSource(fs, 1, (const GLchar **) &frag_shader, NULL);
    glCompileShader(fs);
    glGetShaderInfoLog(fs, sizeof(infoLog), NULL, infoLog);
    std::cout << "FS shader status: " << infoLog << std::endl;
    
    program = glCreateProgram();
    glAttachShader(program, vs);
    glAttachShader(program, fs);
    glLinkProgram(program);

    
    GLuint vs_lc = glCreateShader(GL_VERTEX_SHADER);
    glShaderSource(vs_lc, 1, (const GLchar**) &v_shader_lc, NULL);
    glCompileShader(vs_lc);
    glGetShaderInfoLog(vs_lc, 512, NULL, infoLog);
    std::cout << "VSL shader status: " << infoLog << std::endl;

    GLuint fs_lc = glCreateShader(GL_FRAGMENT_SHADER);
    glShaderSource(fs_lc, 1, (const GLchar **) &f_shader_lc, NULL);
    glCompileShader(fs_lc);
    glGetShaderInfoLog(fs_lc, 512, NULL, infoLog);
    std::cout << "FSL shader status: " << infoLog << std::endl;

    program_lc = glCreateProgram();
    glAttachShader(program_lc, vs_lc);
    glAttachShader(program_lc, fs_lc);
    glLinkProgram(program_lc);


    glGenVertexArrays(1, &vao);
    glBindVertexArray(vao);

    glEnableVertexAttribArray(0);
    glEnableVertexAttribArray(1);
    glEnableVertexAttribArray(2);

    glGenBuffers(1, &vbo);
    glBindBuffer(GL_ARRAY_BUFFER, vbo);
    glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 8*sizeof(float), 0);
    glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 8*sizeof(float), (void*)(6*sizeof(float)));
    glVertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE, 8*sizeof(float), (void*)(3*sizeof(float)));

    upload_img(bin_file1, &image_data1);
    upload_img(bin_file2, &image_data2);

    glGenTextures(1, &tex1);
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, tex1);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_BORDER);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_BORDER);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, img_width, img_height, 
            0, GL_RGBA, GL_UNSIGNED_BYTE, image_data1); 

    glGenTextures(1, &tex2);
    glActiveTexture(GL_TEXTURE1);
    glBindTexture(GL_TEXTURE_2D, tex2);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_BORDER);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_BORDER);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, img_width, img_height,
            0, GL_RGBA, GL_UNSIGNED_BYTE, image_data2);

    glUniform1i(glGetUniformLocation(program, "tex1"), 0);
    glUniform1i(glGetUniformLocation(program, "tex2"), 1);

    //glm::vec4 vec(1.0f, 0.0f, 0.0f, 1.0f);
    //glm::mat4 trans = glm::translate(glm::mat4(1.0f), glm::vec3(1.0f, 1.0f, 0.0f));
    //glm::vec4 vout = trans * vec;

    //std::cout << "(" << vout.x << ", ";
    //std::cout << vout.y << ", "; 
    //std::cout << vout.z << ")" << std::endl; 

    //trans = glm::mat4(1.0f);
    //trans = glm::rotate(trans, glm::radians(90.0f), glm::vec3(0.0, 0.0, 1.0f));
    //trans = glm::scale(trans, glm::vec3(0.5f, 0.5f, 0.5f));

    glUseProgram(program_lc);
    glGenVertexArrays(1, &vao_lc);
    glBindVertexArray(vao_lc);
    glEnableVertexAttribArray(0);
    glBindBuffer(GL_ARRAY_BUFFER, vbo);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 8*sizeof(float), 0);

    glEnable(GL_DEPTH_TEST);
    glDepthFunc(GL_LEQUAL);
    glEnable(GL_STENCIL_TEST);
    glStencilMask(0x00);

    glEnable(GL_CULL_FACE);
    //glCullFace(GL_FRONT);

    window_loop();

    return 0;
}

