#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include <vector>

#include <GLES3/gl31.h>
#include <GL/gl.h>

#include <glm/glm.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtc/matrix_transform.hpp>

#include "rgbe.h"

extern void create_window(int width, int height);
extern void window_loop();

const int width = 800;
const int height = 600;
const float aspect = (float)width/(float)height;
const float PI = 3.14159265359f;
const float camX = 0.0f;
const float camY = 0.0f;
const float camZ = 3.0f;
glm::vec3 view_pos = {
    camX, camY, camZ, 
};

struct LightSource{
    glm::vec3 pos;
    glm::vec3 color;
}lsrc[] = {
    {
        {0.0f,  0.0f, 10.0f},
        {150.0f, 150.0f, 150.0f},
    },
};

#define ARRAY_SZ(_arr) \
    (sizeof(_arr) / sizeof(_arr[0]))

#define SHADER_SET_MAT4(_h, _name, _mat4) \
    glUniformMatrix4fv(glGetUniformLocation(_h, _name), 1, GL_FALSE, glm::value_ptr(_mat4))

#define SHADER_SET_VEC3(_h, _name, _vec3) \
    glUniform3fv(glGetUniformLocation(_h, _name), 1, glm::value_ptr(_vec3))

#define SHADER_SET_INT(_h, _name, _i) \
    glUniform1i(glGetUniformLocation(_h, _name), _i)
    
#define SHADER_SET_FLOAT(_h, _name, _f) \
    glUniform1f(glGetUniformLocation(_h, _name), _f)

#define VERTEX_OFF  0
#define VERTEX_SZ   (3*sizeof(GLfloat))
#define NORMAL_OFF  (VERTEX_OFF + VERTEX_SZ)
#define NORMAL_SZ   (3*sizeof(GLfloat))
#define TEX_OFF     (NORMAL_OFF + NORMAL_SZ)
#define TEX_SZ      (2*sizeof(GLfloat))
#define POINT_SZ    (TEX_OFF + TEX_SZ)

GLfloat vertices_cube[] = {
    //Vertex             Normals         Texture
   -1.0, -1.0, -1.0,  0.0,  0.0,  1.0,  0.0, 0.0,                                                             
    1.0, -1.0, -1.0,  0.0,  0.0,  1.0,  1.0, 0.0,                                                             
    1.0,  1.0, -1.0,  0.0,  0.0,  1.0,  1.0, 1.0,                                                             
    1.0,  1.0, -1.0,  0.0,  0.0,  1.0,  1.0, 1.0,                                                             
   -1.0,  1.0, -1.0,  0.0,  0.0,  1.0,  0.0, 1.0,                                                             
   -1.0, -1.0, -1.0,  0.0,  0.0,  1.0,  0.0, 0.0,                                                             
                                      
   -1.0, -1.0,  1.0,  0.0,  0.0, -1.0,  0.0, 0.0,                                                            
    1.0, -1.0,  1.0,  0.0,  0.0, -1.0,  1.0, 0.0,                                                            
    1.0,  1.0,  1.0,  0.0,  0.0, -1.0,  1.0, 1.0,                                                            
    1.0,  1.0,  1.0,  0.0,  0.0, -1.0,  1.0, 1.0,                                                            
   -1.0,  1.0,  1.0,  0.0,  0.0, -1.0,  0.0, 1.0,                                                            
   -1.0, -1.0,  1.0,  0.0,  0.0, -1.0,  0.0, 0.0,                                                            

   -1.0,  1.0,  1.0,  1.0,  0.0,  0.0,  1.0, 1.0,                                                   
   -1.0,  1.0, -1.0,  1.0,  0.0,  0.0,  0.0, 1.0,                                                   
   -1.0, -1.0, -1.0,  1.0,  0.0,  0.0,  0.0, 0.0,                                                   
   -1.0, -1.0, -1.0,  1.0,  0.0,  0.0,  0.0, 0.0,                                                   
   -1.0, -1.0,  1.0,  1.0,  0.0,  0.0,  1.0, 0.0,                                                   
   -1.0,  1.0,  1.0,  1.0,  0.0,  0.0,  1.0, 1.0,                                                   
                                                 
    1.0,  1.0,  1.0, -1.0,  0.0,  0.0,  0.0, 0.0,                                                   
    1.0,  1.0, -1.0, -1.0,  0.0,  0.0,  1.0, 0.0,                                                   
    1.0, -1.0, -1.0, -1.0,  0.0,  0.0,  1.0, 1.0,                                                   
    1.0, -1.0, -1.0, -1.0,  0.0,  0.0,  1.0, 1.0,                                                   
    1.0, -1.0,  1.0, -1.0,  0.0,  0.0,  0.0, 1.0,                                                   
    1.0,  1.0,  1.0, -1.0,  0.0,  0.0,  0.0, 0.0,                                                   

   -1.0, -1.0, -1.0,  0.0,  1.0,  0.0,  0.0, 0.0,                                                  
    1.0, -1.0, -1.0,  0.0,  1.0,  0.0,  1.0, 0.0,                                                  
    1.0, -1.0,  1.0,  0.0,  1.0,  0.0,  1.0, 1.0,                                                  
    1.0, -1.0,  1.0,  0.0,  1.0,  0.0,  1.0, 1.0,                                                  
   -1.0, -1.0,  1.0,  0.0,  1.0,  0.0,  0.0, 1.0,                                                  
   -1.0, -1.0, -1.0,  0.0,  1.0,  0.0,  0.0, 0.0,                                                  
                                                 
    1.0,  1.0,  1.0,  0.0, -1.0,  0.0,  1.0, 1.0,                                                  
    1.0,  1.0, -1.0,  0.0, -1.0,  0.0,  1.0, 0.0,                                                  
   -1.0,  1.0, -1.0,  0.0, -1.0,  0.0,  0.0, 0.0,                                                  
    1.0,  1.0,  1.0,  0.0, -1.0,  0.0,  1.0, 1.0,                                                  
   -1.0,  1.0, -1.0,  0.0, -1.0,  0.0,  0.0, 0.0,                                                  
   -1.0,  1.0,  1.0,  0.0, -1.0,  0.0,  0.0, 1.0,                                                  
};

GLfloat vertices_plane[] = {
    -1.0,   1.0,  0.0,      0.0, 1.0,
    -1.0,  -1.0,  0.0,      0.0, 0.0,
     1.0,   1.0,  0.0,      1.0, 1.0,
     1.0,  -1.0,  0.0,      1.0, 0.0,
};

std::vector<float> generate_sphere_vertices(int N, float R)
{
    const float lat_step = PI / (float)N;
    const float long_step = 2.0f * (PI / (float)N);
    std::vector<float> vertices;

    auto add_vertex = [&](int long_idx, int lat_idx, float u_idx, float v_idx){
        int t = long_idx % N;
        int m = lat_idx;

        float Px = R * sin(m * lat_step) * sin(t * long_step);
        float Py = R * cos(m * lat_step);
        float Pz = R * sin(m * lat_step) * cos(t * long_step);
        
        // Push vertix coordinates
        vertices.push_back(Px);
        vertices.push_back(Py);
        vertices.push_back(Pz);
        // Push normal coordinates
        vertices.push_back(Px);
        vertices.push_back(Py);
        vertices.push_back(Pz);
        // Push texture coordinates
        vertices.push_back(u_idx / (float)N);
        vertices.push_back(v_idx / (float)N);
    };
    auto add_point = [&](int long_idx, int lat_idx){
        add_vertex(long_idx, lat_idx, (float)long_idx, (float)lat_idx);
    };
    auto add_pole_point = [&](int long_idx, int lat_idx, float u, float v){
        add_vertex(long_idx, lat_idx, u, v);
    };

    for(int i = 0; i < N; i += 2){
        if(i != 0) add_point(i, 1);
        add_point(i, 1);
        add_point(i + 1, 1);
        add_pole_point(0, 0, i, 0);
        add_point(i + 2, 1);
        add_point(i + 2, 1);
    }
        
    for(int j = 1, i = 0; (j + 1) < N; ++j){
        add_point(0, j);
        add_point(0, j);
        add_point(0, j + 1);
        for(i = 0; i < N; ++i){
            add_point(i + 1, j);
            add_point(i + 1, j + 1);
        }
        add_point(i, j + 1);
    }

    for(int i = 0; i < N; i += 2){
        add_point(i, N - 1);
        add_point(i, N - 1);
        add_pole_point(0, N, i, N);
        add_point(i + 1, N - 1);
        add_point(i + 2, N - 1);
        if(i != (N - 1))add_point(i + 2, N - 1);
    }

    return vertices;
}


char *load_file(const char *filename)
{
    FILE *f = fopen(filename, "r");
    char *text = NULL;

    if(f){
        size_t size = 0;

        fseek(f, 0, SEEK_END);
        size = ftell(f);
        rewind(f);

        text = (char*)calloc(size + 1, sizeof(char));
        fread(text, sizeof(char), size, f);
        fclose(f); 
    }

    return text;
}

char *load_hdr(const char *filename, int *img_width, int *img_height)
{
    FILE *f = fopen(filename, "rb");
    char *img = NULL;
    int w = 0, h = 0;

    if(f){
        rgbe_header_info info = {0};
        RGBE_ReadHeader(f, &w, &h, &info);
        img = (char *)malloc(sizeof(float) * 3 * w * h);
        RGBE_ReadPixels_RLE(f, (float*)img, w, h);

        *img_width = w;
        *img_height = h;

        fclose(f);
    }

    return img;
}

void load_shader(const char *vertex_source, const char *fragment_source,
        GLuint *vertex, GLuint *fragment, GLuint *program)
{
    const char * test_str = "#version ";
    char infoLog[512] = {0};
    char *shader_source = NULL;

    *vertex = glCreateShader(GL_VERTEX_SHADER);
    if(!strcmp(vertex_source, test_str)){
        glShaderSource(*vertex, 1, &vertex_source, NULL);
        glCompileShader(*vertex);
        glGetShaderInfoLog(*vertex, sizeof(infoLog), NULL, infoLog);
        printf("Vertex shader compile log: %s\n", infoLog);
    }else{
        shader_source = load_file(vertex_source);
        glShaderSource(*vertex, 1, &shader_source, NULL);
        glCompileShader(*vertex);
        glGetShaderInfoLog(*vertex, sizeof(infoLog), NULL, infoLog);
        printf("%s, compile log: %s\n", vertex_source, infoLog);
        free(shader_source);
    }

    *fragment = glCreateShader(GL_FRAGMENT_SHADER);
    if(!strcmp(fragment_source, test_str)){
        glShaderSource(*fragment, 1, &fragment_source, NULL);
        glCompileShader(*fragment);
        glGetShaderInfoLog(*fragment, sizeof(infoLog), NULL, infoLog);
        printf("Fragment shader compile log: %s\n", infoLog);
    }else{
        shader_source = load_file(fragment_source);
        glShaderSource(*fragment, 1, &shader_source, NULL);
        glCompileShader(*fragment);
        glGetShaderInfoLog(*fragment, sizeof(infoLog), NULL, infoLog);
        printf("%s, compile log: %s\n", fragment_source, infoLog);
        free(shader_source);
    }

    *program = glCreateProgram();
    glAttachShader(*program, *vertex);
    glAttachShader(*program, *fragment);
    glLinkProgram(*program);
}

GLuint vao, buf, tex, pbr_vs, pbr_fs, pbr_ph;
GLuint cube_vao, cube_buf, plane_vao, plane_buf;
std::vector<float> vertices;
GLuint albedo_tex, ao_tex, metallic_tex, normal_tex, roughness_tex;
GLuint captureFBO, captureRBO;
GLuint hdrTexture, envCubemap;
GLuint background_vs, background_fs, background_ph;
GLuint er2cubemap_vs, er2cubemap_fs, er2cubemap_ph;
GLuint irradianceMap;
GLuint irr_vs, irr_fs, irr_ph;
GLuint prefilterMap;
GLuint prefilter_vs, prefilter_fs, prefilter_ph;
GLuint brdfLUTTexture;
GLuint brdf_vs, brdf_fs, brdf_ph;

void draw_opengl(char c){
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_CUBE_MAP, irradianceMap);
    glActiveTexture(GL_TEXTURE1);
    glBindTexture(GL_TEXTURE_CUBE_MAP, prefilterMap);
    glActiveTexture(GL_TEXTURE2);
    glBindTexture(GL_TEXTURE_2D, brdfLUTTexture);


    glBindVertexArray(vao);
    glDrawArrays(GL_TRIANGLE_STRIP, 0, 
            (vertices.size() * sizeof(vertices[0])) / POINT_SZ);
}

int main(int argc, char **argv)
{
    vertices = generate_sphere_vertices(64, 1.0);

    create_window(width, height);

    glEnable(GL_DEPTH_TEST);
    glDepthFunc(GL_LEQUAL);
    glEnable(GL_TEXTURE_CUBE_MAP_SEAMLESS);
    
    glGenVertexArrays(1, &vao);
    glBindVertexArray(vao);
    glGenBuffers(1, &buf);
    glBindBuffer(GL_ARRAY_BUFFER, buf);
    glBufferData(GL_ARRAY_BUFFER, vertices.size() * sizeof(vertices[0]),
            vertices.data(), GL_STATIC_DRAW);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, POINT_SZ, (void*)VERTEX_OFF);
    glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, POINT_SZ, (void*)NORMAL_OFF);
    glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, POINT_SZ, (void*)TEX_OFF);
    glEnableVertexAttribArray(0);
    glEnableVertexAttribArray(1);
    glEnableVertexAttribArray(2);

    // PBR shader
    load_shader("pbr.vs", "pbr.fs", &pbr_vs, &pbr_fs, &pbr_ph);
    glUseProgram(pbr_ph);

    glm::mat4 model(1.0);
    glm::mat4 view = glm::lookAt(glm::vec3(camX, camY, camZ),
                                 glm::vec3(0.0f, 0.0f, 0.0f), glm::vec3(0.0f, 1.0f, 0.0f));
    glm::mat4 proj = glm::perspective(glm::radians(45.0f), aspect, 0.1f, 100.0f);
    SHADER_SET_MAT4(pbr_ph, "model", model);
    SHADER_SET_MAT4(pbr_ph, "view", view);
    SHADER_SET_MAT4(pbr_ph, "proj", proj);

    for(int i = 0; i < ARRAY_SZ(lsrc); ++i){
        char lsrc_pos[30];
        char lsrc_color[30];

        snprintf(lsrc_pos, sizeof(lsrc_pos), "lsrc[%d].pos", i);
        snprintf(lsrc_color, sizeof(lsrc_color), "lsrc[%d].color", i);
        SHADER_SET_VEC3(pbr_ph, lsrc_pos, lsrc[i].pos);
        SHADER_SET_VEC3(pbr_ph, lsrc_color, lsrc[i].color);
    }
    SHADER_SET_VEC3(pbr_ph, "view_pos", view_pos);
    SHADER_SET_FLOAT(pbr_ph, "metallic", 0.28571428);
    SHADER_SET_FLOAT(pbr_ph, "roughness", 0.428571);
    SHADER_SET_INT(pbr_ph, "irradianceMap", 0);
    SHADER_SET_INT(pbr_ph, "prefilterMap", 1);
    SHADER_SET_INT(pbr_ph, "brdfLUT", 2);
    SHADER_SET_VEC3(pbr_ph, "albedo", glm::vec3(0.5f, 0.0f, 0.0f));
    SHADER_SET_FLOAT(pbr_ph, "ao", 1.0f);

    // Background shader
    load_shader("background.vs", "background.fs", &background_vs, &background_fs, &background_ph);
    glUseProgram(background_ph);
    SHADER_SET_INT(background_ph, "environmentMap", 0);
    SHADER_SET_MAT4(background_ph, "view", view);
    SHADER_SET_MAT4(background_ph, "projection", proj);

    glGenVertexArrays(1, &cube_vao);
    glBindVertexArray(cube_vao);
    glGenBuffers(1, &cube_buf);
    glBindBuffer(GL_ARRAY_BUFFER, cube_buf);
    glBufferData(GL_ARRAY_BUFFER, sizeof(vertices_cube), vertices_cube, GL_STATIC_DRAW);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 8*sizeof(GLfloat), 0);
    glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 8*sizeof(GLfloat), (void*)(3*sizeof(GLfloat)));
    glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, 8*sizeof(GLfloat), (void*)(6*sizeof(GLfloat)));
    glEnableVertexAttribArray(0);
    glEnableVertexAttribArray(1);
    glEnableVertexAttribArray(2);

    glGenVertexArrays(1, &plane_vao);
    glBindVertexArray(plane_vao);
    glGenBuffers(1, &plane_buf);
    glBindBuffer(GL_ARRAY_BUFFER, plane_buf);
    glBufferData(GL_ARRAY_BUFFER, sizeof(vertices_plane), vertices_plane, GL_STATIC_DRAW);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 5*sizeof(GLfloat), 0);
    glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 5*sizeof(GLfloat), (void*)(3*sizeof(GLfloat)));
    glEnableVertexAttribArray(0);
    glEnableVertexAttribArray(1);

    glGenFramebuffers(1, &captureFBO);
    glGenRenderbuffers(1, &captureRBO);
    glBindFramebuffer(GL_FRAMEBUFFER, captureFBO);
    glBindRenderbuffer(GL_RENDERBUFFER, captureRBO);
    glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT24, 512, 512);
    glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, captureRBO);

    int hdr_width = 0;
    int hdr_height = 0;
    char *hdr_img = load_hdr("newport_loft.hdr", &hdr_width, &hdr_height);
    glGenTextures(1, &hdrTexture);
    glBindTexture(GL_TEXTURE_2D, hdrTexture);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB16F, hdr_width, hdr_height, 0, GL_RGB, GL_FLOAT, hdr_img);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    free(hdr_img);
    
    glGenTextures(1, &envCubemap);
    glBindTexture(GL_TEXTURE_CUBE_MAP, envCubemap);
    for (unsigned int i = 0; i < 6; ++i)
    {
        glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X + i, 0, GL_RGB16F, 512, 512, 0, GL_RGB, GL_FLOAT, nullptr);
    }
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

    glm::mat4 captureProjection = glm::perspective(glm::radians(90.0f), 1.0f, 0.1f, 10.0f);
    glm::mat4 captureViews[] =
    {
        glm::lookAt(glm::vec3(0.0f, 0.0f, 0.0f), glm::vec3(1.0f,  0.0f,  0.0f), glm::vec3(0.0f, -1.0f,  0.0f)),
        glm::lookAt(glm::vec3(0.0f, 0.0f, 0.0f), glm::vec3(-1.0f,  0.0f,  0.0f), glm::vec3(0.0f, -1.0f,  0.0f)),
        glm::lookAt(glm::vec3(0.0f, 0.0f, 0.0f), glm::vec3(0.0f,  1.0f,  0.0f), glm::vec3(0.0f,  0.0f,  1.0f)),
        glm::lookAt(glm::vec3(0.0f, 0.0f, 0.0f), glm::vec3(0.0f, -1.0f,  0.0f), glm::vec3(0.0f,  0.0f, -1.0f)),
        glm::lookAt(glm::vec3(0.0f, 0.0f, 0.0f), glm::vec3(0.0f,  0.0f,  1.0f), glm::vec3(0.0f, -1.0f,  0.0f)),
        glm::lookAt(glm::vec3(0.0f, 0.0f, 0.0f), glm::vec3(0.0f,  0.0f, -1.0f), glm::vec3(0.0f, -1.0f,  0.0f))
    };

    load_shader("cubemap.vs", "er2cubemap.fs", &er2cubemap_vs, &er2cubemap_fs, &er2cubemap_ph);
    glUseProgram(er2cubemap_ph);
    SHADER_SET_INT(er2cubemap_ph, "equirectangularMap", 0);
    SHADER_SET_MAT4(er2cubemap_ph, "projection", captureProjection);
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, hdrTexture);
    glViewport(0, 0, 512, 512);
    glBindFramebuffer(GL_FRAMEBUFFER, captureFBO);
    for (unsigned int i = 0; i < 6; ++i)
    {
        SHADER_SET_MAT4(er2cubemap_ph, "view", captureViews[i]);
        glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, 
                GL_TEXTURE_CUBE_MAP_POSITIVE_X + i, envCubemap, 0);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        glBindVertexArray(cube_vao);
        glDrawArrays(GL_TRIANGLES, 0, 36);
    }
    glBindFramebuffer(GL_FRAMEBUFFER, 0);
    glBindTexture(GL_TEXTURE_CUBE_MAP, envCubemap);
    glGenerateMipmap(GL_TEXTURE_CUBE_MAP);

    // Create irradiance cubemap
    glGenTextures(1, &irradianceMap);
    glBindTexture(GL_TEXTURE_CUBE_MAP, irradianceMap);
    for (unsigned int i = 0; i < 6; ++i)
    {
        glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X + i, 0, GL_RGB16F, 32, 32, 0, GL_RGB, GL_FLOAT, NULL);
    }
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

    glBindFramebuffer(GL_FRAMEBUFFER, captureFBO);
    glBindRenderbuffer(GL_RENDERBUFFER, captureRBO);
    glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT24, 32, 32);

    // solve diffuse integral
    load_shader("cubemap.vs", "irr_conv.fs", &irr_vs, &irr_fs, &irr_ph);
    glUseProgram(irr_ph);
    SHADER_SET_INT(irr_ph, "environmentMap", 0);
    SHADER_SET_MAT4(irr_ph, "projection", captureProjection);
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_CUBE_MAP, envCubemap);
    glViewport(0, 0, 32, 32);
    glBindFramebuffer(GL_FRAMEBUFFER, captureFBO);
    for (unsigned int i = 0; i < 6; ++i)
    {
        SHADER_SET_MAT4(irr_ph, "view", captureViews[i]);
        glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, \
                GL_TEXTURE_CUBE_MAP_POSITIVE_X + i, irradianceMap, 0);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        glBindVertexArray(cube_vao);
        glDrawArrays(GL_TRIANGLES, 0, 36);
    }
    glBindFramebuffer(GL_FRAMEBUFFER, 0);

    // create pre-filter cubemap
    glGenTextures(1, &prefilterMap);
    glBindTexture(GL_TEXTURE_CUBE_MAP, prefilterMap);
    for (unsigned int i = 0; i < 6; ++i)
    {
        glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X + i, 0, GL_RGB16F, 128, 128, 0, GL_RGB, GL_FLOAT, NULL);
    }
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glGenerateMipmap(GL_TEXTURE_CUBE_MAP);

    // quasi monte-carlo simulation
    load_shader("cubemap.vs", "prefilter.fs", &prefilter_vs, &prefilter_fs, &prefilter_ph);
    glUseProgram(prefilter_ph);
    SHADER_SET_INT(prefilter_ph, "environmentMap", 0);
    SHADER_SET_MAT4(prefilter_ph, "projection", captureProjection);
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_CUBE_MAP, envCubemap);

    glBindFramebuffer(GL_FRAMEBUFFER, captureFBO);
    unsigned int maxMipLevels = 5;
    for (unsigned int mip = 0; mip < maxMipLevels; ++mip)
    {
        unsigned int mipWidth  = 128 * std::pow(0.5, mip);
        unsigned int mipHeight = 128 * std::pow(0.5, mip);
        glBindRenderbuffer(GL_RENDERBUFFER, captureRBO);
        glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT24, mipWidth, mipHeight);
        glViewport(0, 0, mipWidth, mipHeight);

        float roughness = (float)mip / (float)(maxMipLevels - 1);
        SHADER_SET_FLOAT(prefilter_ph, "roughness", roughness);
        for (unsigned int i = 0; i < 6; ++i)
        {
            SHADER_SET_MAT4(prefilter_ph, "view", captureViews[i]);
            glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_CUBE_MAP_POSITIVE_X + i, prefilterMap, mip);

            glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
            glBindVertexArray(cube_vao);
            glDrawArrays(GL_TRIANGLES, 0, 36);
        }
    }
    glBindFramebuffer(GL_FRAMEBUFFER, 0);

    // BRDF
    glGenTextures(1, &brdfLUTTexture);
    glBindTexture(GL_TEXTURE_2D, brdfLUTTexture);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RG16F, 512, 512, 0, GL_RG, GL_FLOAT, 0);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

    glBindFramebuffer(GL_FRAMEBUFFER, captureFBO);
    glBindRenderbuffer(GL_RENDERBUFFER, captureRBO);
    glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT24, 512, 512);
    glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, brdfLUTTexture, 0);

    load_shader("brdf.vs", "brdf.fs", &brdf_vs, &brdf_fs, &brdf_ph);
    glViewport(0, 0, 512, 512);
    glUseProgram(brdf_ph);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glBindVertexArray(plane_vao);
    glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);

    glBindFramebuffer(GL_FRAMEBUFFER, 0);

    glViewport(0, 0, width, height);

    window_loop();

    return 0;
}
