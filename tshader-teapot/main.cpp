#include "glad.h"
#define GLFW_INCLUDE_NONE
#include <GLFW/glfw3.h>

#include <glm/glm.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtc/matrix_transform.hpp>

#include "load.h"
#include "teapot.h"

const int width = 800;
const int height = 600;
const float camX = 0.0f;
const float camY = 0.0f;
const float camZ = 700.0f;

const shader_handle_t shaders[] = {
    {GL_VERTEX_SHADER, "main.vs"},
    {GL_TESS_CONTROL_SHADER, "main.tc"},
    {GL_TESS_EVALUATION_SHADER, "main.te"},
    {GL_FRAGMENT_SHADER, "main.fs"},
};

GLuint vao, buf, knots, ph;
GLFWwindow* window;
void draw_opengl(void)
{
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glClearColor(0.02, 0.0, 0.02, 1.0);

    glDrawElements(GL_PATCHES, sizeof(TeapotKnots)/sizeof(unsigned short), GL_UNSIGNED_SHORT, 0);
}

int main(int argc, char **argv)
{
    glfwInit();
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 6);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
    if(!(window = glfwCreateWindow(width, height, "Tesselation Shader, Teapot", NULL, NULL))){
        printf("Failed to create a window!\n");
        return -1;
    }
    glfwMakeContextCurrent(window);

    gladLoadGL();
    glfwSwapInterval(1);

    glGenVertexArrays(1, &vao);
    glBindVertexArray(vao);
    glGenBuffers(1, &buf);
    glBindBuffer(GL_ARRAY_BUFFER, buf);
    glBufferData(GL_ARRAY_BUFFER, sizeof(TeapotPositions), TeapotPositions, GL_STATIC_DRAW);

    glGenBuffers(1, &knots);                                                      
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, knots);                                 
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(TeapotKnots), TeapotKnots, GL_STATIC_DRAW);

    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3*sizeof(GLfloat), 0);
    glEnableVertexAttribArray(0);

    load_shaders(shaders, SHADER_ARR_SZ(shaders), &ph);
    glUseProgram(ph);

    glm::mat4 model(1.0);
    glUniformMatrix4fv(glGetUniformLocation(ph, "model"), 1, GL_FALSE, glm::value_ptr(model));
    glm::mat4 proj = glm::perspective(glm::radians(45.0f), (float)width/(float)height, 0.1f, 1000.0f);
    glm::mat4 view = glm::lookAt(glm::vec3(camX, camY, camZ),
                                 glm::vec3(0.0f, 0.0f, 0.0f), glm::vec3(0.0f, 1.0f, 0.0f));
    glUniformMatrix4fv(glGetUniformLocation(ph, "view"), 1, GL_FALSE, glm::value_ptr(view));
    glUniformMatrix4fv(glGetUniformLocation(ph, "proj"), 1, GL_FALSE, glm::value_ptr(proj));

    glPatchParameteri(GL_PATCH_VERTICES, 16);
    glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
    glEnable(GL_DEPTH_TEST);
    glEnable(GL_CULL_FACE);
    glCullFace(GL_BACK);

    while(!glfwWindowShouldClose(window)){
        int width, height;
        glfwGetFramebufferSize(window, &width, &height);
        glViewport(0, 0, width, height);

        draw_opengl();
        glfwSwapBuffers(window);
        glfwPollEvents();
    }
    glfwDestroyWindow(window);
    glfwTerminate();

    return 0;
}
