// vim: set ft=glsl:
#version 460 core

layout (location = 0) in vec3 coord;
out vec3 vPosition;

void main(){
    vPosition = coord;
}
